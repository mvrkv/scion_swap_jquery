ScionSwap::Application.routes.draw do
  devise_for :user, :only => :sessions, :constraints => {:format => 'text/html'}
  devise_for :user, :skip => :sessions, :controllers => { :confirmations => 'devise_overrides/confirmations' }
  devise_scope :user do
    post '/user/auth_token', :to => 'users#auth_token'
    root :to => 'devise/sessions#new'
    get 'user/change_password', :to => 'users#change_password', :as => :change_password   
    get 'user/unsubscribe_activation', :to => 'users#unsubscribe_activation', :as => :unsubscribe_activation
    get 'user/deactivate_reminder_mail/:confirmation_token', :to => 'users#deactivate_reminder_mail', :as => :deactivate_reminder_mail
    
  end
  
  resource :user do
    collection do
      put :update_password

    end
  end

  # Member Routes
  resources :users, :except => [:destroy] do 
    collection do
      get :admin, :as => :admin_dashboard
      get :dashboard
      get :activate
      get :edit
      get :current
      get :members
    end
    member do
      post :resend_confirmation
      post :add_credits
    end
  end
  scope :via => :get do
    match 'search/browse', :to => 'search#browse', :as => :browse
    match 'search(/:location(/:dealership_id))', :to => 'search#index', :as => :search   
    match 'terms', :to => 'static#show', :as => :terms
    match 'privacy', :to => 'static#show', :as => :privacy
    match 'faq', :to => 'static#show', :as => :faq
    match 'help', :to => 'static#help', :as => :help
  end

  resources :dealerships, :only => [:show, :edit, :update, :index] do
    resources :fleet_groups, :only => [:update] do
      resources :fleet_group_quantities, :only => [:index, :create, :destroy]
    end
    resources :dealership_holidays, :only => [:update, :create, :destroy]
    resources :reservations, :only => [:index]

    member do
      get :schedule
      get :inventory
      get :details
    end
  end

  resources :reservations, :except => [:destroy] do
    member do
      get :cancel
      put :cancel
      get :start
      put :start
      put :end
    end
  end
  
  resources :survey_questions, :controller => 'activation_survey_questions', :only => :index do
    resources :survey_answers, :controller => 'activation_survey_answers', :only => [:create, :index]
  end
  
  resources :activity_logs, :only => :index
end
