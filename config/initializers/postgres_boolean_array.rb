class String
  def from_postgres_array_with_boolean(base_type = :string)
    if !empty? && base_type == :boolean
      elements = match(/\{(.*)\}/m).captures.first.gsub(/\\"/, '$ESCAPED_DOUBLE_QUOTE$').split(/(?:,)(?=(?:[^"]|"[^"]*")*$)/m)
      elements.map do |e|
        res = e.gsub('$ESCAPED_DOUBLE_QUOTE$', '"').gsub("\\\\", "\\").gsub(/^"/, '').gsub(/"$/, '').gsub("''", "'").strip
        res == 'NULL' ? nil : !['f', false, 'false'].include?(res)
      end
    else
      self.from_postgres_array_without_boolean(base_type)
    end
  end
  alias_method_chain :from_postgres_array, :boolean
end

module ActiveRecord
  module ConnectionAdapters
    class PostgreSQLColumn < Column
      def simplified_type_with_time_and_time_zone(field_type)
        case field_type
        when /^time(stamp)? with(?:out)? time zone\[\]$/
          :time_array
        else
          simplified_type_without_time_and_time_zone(field_type)
        end
      end
      alias_method_chain :simplified_type, :time_and_time_zone
    end
  end
end
  