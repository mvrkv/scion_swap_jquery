ftp_config = YAML.load_file(Rails.root.join('config', 'ftp_data_exchange.yml'))
Rails.application.config.ftp_data_exchange = ftp_config[Rails.env].with_indifferent_access

analytics_config = YAML.load_file(Rails.root.join('config', 'analytics.yml'))
Rails.application.config.analytics_report = analytics_config[Rails.env].with_indifferent_access
