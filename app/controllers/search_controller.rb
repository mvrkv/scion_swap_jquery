class SearchController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource class: false
  respond_to :html, :json

  def index
    if params[:dealership_id]
      @dealership = Dealership.find(params[:dealership_id])
      redirect_to search_path(params[:location]) and return unless @dealership
    elsif params[:location]
      @dealership = Dealership.near(params[:location]).limit(1).first
    elsif member_signed_in?
      @dealership = current_member.member_dealership
    else
      redirect_to user_redirect_url(current_user) and return
    end
    @from_date = params[:from_date]
    @to_date = params[:to_date]
    if @to_date.nil? && @from_date.present?
      @to_date = @from_date + 1.day
    elsif @from_date.nil? && @to_date.present?
      @from_date = @to_date - 1.day
    end
    redirect_to browse_path and return if @dealership.blank? && request.format.html?
    respond_with @dealership
  end

  def browse
    @dealerships = Dealership.all
    respond_with @dealerships
  end
end
