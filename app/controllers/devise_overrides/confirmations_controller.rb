class DeviseOverrides::ConfirmationsController < Devise::ConfirmationsController
  before_filter :redirect_confirmed_to_activate, :only => :show
  
  def unsubscribe_activation
    raise params.inspect
  end
  
  protected
  def redirect_confirmed_to_activate
    return true if params[:confirmation_token].blank?
    resource = resource_class.find_by_confirmation_token("#{params[:confirmation_token]} ")
    if resource && resource.confirmed? && resource.tos_accepted_at.blank?
      sign_in_and_redirect resource
      false
    end
  end
end