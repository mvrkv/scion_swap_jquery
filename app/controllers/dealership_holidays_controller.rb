class DealershipHolidaysController < InheritedResources::Base
  belongs_to :dealership
  actions :create, :update, :destroy
  load_and_authorize_resource
  respond_to :json
  
  def create
    create! do |format|
      maybe_render_conflict(format)
    end
  end
  
  def update
    update! do |format|
      maybe_render_conflict(format)
    end
  end
  
  private
  def maybe_render_conflict(format)
    if resource.errors.keys == [:conflicts]
      format.json { render :json => {:errors => resource.errors}, :status => :conflict }
    end
  end
end
