class UsersController < InheritedResources::Base
  respond_to :html, :except => [:resend_confirmation, :add_credits]
  respond_to :json
  include Devise::Controllers::Helpers
  before_filter :authenticate_user!, :except => [:auth_token, :unsubscribe_activation, :deactivate_reminder_mail]
  load_and_authorize_resource
  has_scope :for_dealerships

  def dashboard
    params[:id] ||= current_user.id
    render :action => 'dashboard'
  end
  alias_method :admin, :dashboard
  
  def auth_token
    resource = User.find_for_database_authentication(email: params[:member][:email])
    respond_with resource do |format|
      if resource.valid_password?(params[:member][:password])
        format.json { render :json => resource, :only => [:id, :authentication_token] }
      else
        resource.errors.add(:password, "is incorrect")
      end
    end
  end
  
  def change_password
     @user = User.find(current_user)
 
  end
 
  def update_password
    #raise "came here".inspect
    @user = User.find(current_user)
    if @user.update_with_password(params[:user])
      # Sign in the user by passing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to root_path
    else
      #flash[:notice] = devise_error_messages
      render "change_password"
    end
  end
  
  def unsubscribe_activation
    #@user = User.find_by_confirmation_token(params[:confirmation_token])
  end
  
  def deactivate_reminder_mail
    #raise params.inspect
    @user = User.find_by_confirmation_token(params[:confirmation_token])
    if @user && @user.roles['member'] == 'pending' &&  @user.activation_mail_unsubscribe == false
      @user.update_attributes(:activation_mail_unsubscribe => true)
      @user_message = "You have sucessfully unsubscribed email notification."
    elsif @user && (@user.roles['member'] == 'active' || @user.roles['member'] == 'disabled' || @user.activation_mail_unsubscribe == true)
      #raise "here".inspect 
      @user_message = "You have already activated your account earlier.This link is no longer valid."
    else  
      @user_message = "Sorry this Link is no Longer Valid."
    end  
  end 
  
  def current
    @user = current_user
    render 'show'
  end

  def update
    resource_params.each do |rparam|
      if rparam['password'].blank? && rparam['password_confirmation'].blank?
        rparam.except!('password', 'password_confirmation')
      end
    end
    resource.updated_by = current_user
    update! do
      sign_in(resource, :bypass => true) if resource == current_user
      resource
    end
  end
  
  def resend_confirmation
    resource.resend_confirmation_token
    respond_with resource
  end
  
  def add_credits
    new_credit = resource.purchases.order(:purchased_date).reverse_order.first.credits.create(credit: params[:credits])
    unless new_credit.valid?
      resource.errors.add :credit, new_credit.errors.map{|field, err| err}
    end
    respond_with resource
  end

  protected
  def resource_params
    rparam = super
    rparam.each do |r|
      if current_user.is_active_member_support? && r.keys.include?('roles')
        if r['roles'].is_a?(String)
          r['roles'] = {r['roles'] => 'pending'}
        elsif !r['roles'].is_a?(Hash)
          r.except!('roles')
        end
      end
      if params['tos_accepted']
        r['tos_accepted_at'] = Time.zone.now
      end
    end
    rparam
  end
  
  def resource_with_current_user
    begin
      base_resource = resource_without_current_user
    rescue ActiveRecord::RecordNotFound
      if params[:action] == 'edit' && base_resource.nil? && user_signed_in?
        base_resource = set_resource_ivar(current_user)
      else
        raise
      end
    end
    base_resource
  end
  alias_method_chain :resource, :current_user

  def collection
    if member_signed_in?
      @users ||= end_of_association_chain.members
    else
      super
    end
  end
end
