class ApplicationController < ActionController::Base
  protect_from_forgery
  skip_before_filter :verify_authenticity_token, :if => :is_api_request
  before_filter :api_skip_trackable, :if => :is_api_request
  after_filter :flash_to_headers, :current_user_cookie

  def after_sign_in_path_for(resource)
    user_redirect_url(resource)
  end

  private
  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.html do
        redirect_to user_redirect_url(user_signed_in? && current_user), :status => :forbidden
      end
      format.json { render :nothing => true, :status => :forbidden }
    end
  end
  
  def user_redirect_url(user)
    return root_path unless user.present?
    if user.disabled?
      sign_out(user)
      flash.delete(:alert)
      flash[:error] = "You cannot sign in because your account has been disabled."
      root_path
    else
      user.is_active_member_support? || user.is_active_dealer_rep? ?
        admin_dashboard_users_path :
        user.is_active_member? ? dashboard_users_path : activate_users_path
    end
  end
  
  def flash_to_headers
    return if flash.empty?
    [:error, :warning, :success, :message].each do |flash_type|
      unless flash[flash_type].blank?
        response.headers["X-Message-#{flash_type.capitalize}"] = flash[flash_type]
      end
    end
    flash.discard
  end
  
  def current_user_cookie
    if user_signed_in?
      current_user.reload # Ensure we have the latest updated_at
      cookies['current-user'] = {id: current_user.id, updated_at: current_user.updated_at}.to_json
    else
      cookies.delete(:'current-user')
    end
  end

  def api_skip_trackable
    request.env['devise.skip_trackable'] = true
  end

  def is_api_request
    request.format == 'application/json'
  end

  User::ROLES.each do |role|
    define_method "#{role}_signed_in?" do
      user_signed_in? && current_user.send("is_#{role}?")
    end
    
    define_method "current_#{role}" do
      current_user && current_user.send("is_#{role}?") ? current_user : nil
    end
  end
end
