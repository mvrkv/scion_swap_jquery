class DealershipsController < InheritedResources::Base
  actions :show, :edit, :update, :index
  custom_actions :resource => [:schedule, :inventory]
  load_and_authorize_resource
  has_scope :single, :only => :index
  respond_to :json
  respond_to :html, :only => [:schedule, :inventory]
  
  def update
    update! do |format|
      if resource.errors.keys == [:conflicts]
        format.json { render :json => {:errors => resource.errors}, :status => :conflict }
      end
    end
  end

  private
  def resource_params
    rparam = super
    ['open_time', 'close_time'].each do |field|
      if params.any?{|k,v| k =~ /^#{field}\[\d+\]/ }
        rparam[0]["#{field}"] = []
        params.select{|k,v| k =~ /^#{field}\[\d+\]/ }.each{|k,v| rparam[0]["#{field}"][k.gsub(/(#{field}\[|\])/,'').to_i] = v }
      end
    end
    rparam
  end
end
