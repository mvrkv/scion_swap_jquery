class ActivityLogsController < ApplicationController
  respond_to :json
  
  def index
    if params[:user_id]
      target_user_id = params[:user_id]
      @activity_logs = ActivityLog.where{(user_id == target_user_id) | (reservation_id.in (Reservation.where{user_id == target_user_id}.select(:id)))}
    else
      @activity_logs = ActivityLog.where{id == nil}
    end
    respond_with @activity_logs
  end
end