class ActivationSurveyAnswersController < InheritedResources::Base
  before_filter :authenticate_user!
  actions :create, :index
  load_and_authorize_resource
end
