class ActivationSurveyQuestionsController < InheritedResources::Base
  before_filter :authenticate_user!
  respond_to :json
  actions :index
  load_and_authorize_resource
end
