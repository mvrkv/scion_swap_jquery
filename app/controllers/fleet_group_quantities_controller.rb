class FleetGroupQuantitiesController < InheritedResources::Base
  actions :index, :create, :update, :destroy
  load_and_authorize_resource
  respond_to :json

  def create
    next_update = resource.fleet_group.fleet_group_quantities.where{start_date > my{resource.start_date}}.first
    if next_update && next_update.quantity == resource.quantity
      params['id'] = next_update.id
      set_resource_ivar nil
      update! do
        respond_with resource, :status => :ok, :location => nil
        return
      end
    else
      create! do |format|
        if resource.errors.keys == [:inventory_shortage]
          format.json { render :json => {:errors => resource.errors}, :status => :conflict }
        end
      end
    end
  end

  private :update, :update!
end
