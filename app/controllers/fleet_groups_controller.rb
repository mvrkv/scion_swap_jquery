class FleetGroupsController < InheritedResources::Base
  before_filter :authenticate_user!
  load_and_authorize_resource
  respond_to :json
  respond_to :html, :only => [:show, :index, :edit]
  actions :show, :index, :edit, :update
end
