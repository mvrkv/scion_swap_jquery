class ReservationsController < InheritedResources::Base
  respond_to :json
  respond_to :html, :only => [:new, :edit, :index, :show, :start]
  before_filter :authenticate_user!
  load_and_authorize_resource
  has_scope :member_email, :only => :index
  has_scope :user_id, :only => :index do |controller, scope, value|
    value =~ /^\d+$/ ? scope.where(:user_id => value) : scope 
  end
  belongs_to :dealership, :optional => true
  
  def new
    new! do |format|
      unless @reservation.valid?
        format.json { render :json => @reservation.errors, :status => :unprocessable_entity }
      end
      @future_cancellations = future_cancellations(@reservation)
      @future_cancellations = nil if @future_cancellations.blank?
      format.html do
        flash[:notice] = "Reservation info not found. Please start your search over."
        redirect_to search_path
      end
    end
  end
  
  def create
    @reservation.updated_by = current_user
    create! do |format|
      unless @reservation.new_record?
        future_cancellations(@reservation).each(&:cancel)
      end
    end
  end
  
  def edit
    edit! do |format|
      format.html { render action: 'show' }
    end
  end
  
  def update
    if params['dryRun']
      resource.attributes = resource_params[0]
      if resource.valid?
        respond_to do |format|
          format.json
        end
      elsif resource.errors.keys.include?(:conflicts)
        respond_to do |format|
          format.json { render :json => {errors: resource.errors, available: resource.dealership.fleet_groups.available_between(resource.start_date, resource.end_date)}, :status => :conflict }
        end
      else
        respond_with resource
      end
    else
      resource.updated_by = current_user
      update!
    end
  end

  def cancel
    if request.put?
      resource.cancel
    end
  end

  def end
    if resource.dropoff.present?
      resource.errors.add(:end, "is invalid for complete reservations")
    else
      resource.end
    end
    respond_with resource
  end

  def start
    if request.put?
      if resource.pickup.present?
        resource.errors.add(:start, "is invalid for active or complete reservations")
      else
        resource.start
      end
    end
    respond_with resource
  end

  private
  def resource_params
    rparam = super
    if member_signed_in?
      rparam.each {|rp| rp.merge!(:user_id => current_member.id) unless rp.blank? }
    end
    if rparam.any?{|rp| rp.keys.include? :id } && cannot?(:waive_penalty, resource)
      rparam.each{|rp| rp.delete(:waive_cancellation_penalty) && rp.delete(:waive_late_penalty)}
    end
    rparam
  end

  def dealership
    @dealership ||= (Dealership.find_by_id(params[:dealership_id]) || resource.dealership)
  end
  
  def show  
     @reservation = Reservation.find(params[:id])
   end
  
  def future_cancellations(reservation)
    return if reservation.nil? || reservation.user.nil?
    future_res = reservation.user.reservations.pending.where{start_date >= my{reservation.start_date}}
    future_res = future_res.where{id != reservation.id} if reservation.id.present?
    future_res.select{|res| res.user.credits_available(res) <= reservation.length}
  end
end
