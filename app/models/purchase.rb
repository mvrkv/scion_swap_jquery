class Purchase < ActiveRecord::Base
  CREDITS_PER_NEW_PURCHASE = 10
  EXPIRATION_PERIOD = 1.year
  belongs_to :user
  belongs_to :dealership
  has_many :credits, :dependent => :destroy
  has_many :claims, :dependent => :destroy

  attr_accessor :dealer_code
  attr_accessible :vin, :dealer_code, :purchased_date, :user
  before_validation :assign_dealership
  before_create :default_credits
  validates :user, presence: true
  validates :purchased_date, presence: true
  validates :dealership, presence: true
  validates :vin, presence: true, uniqueness: true
  
  scope :expires_on, lambda{|date| where{`cast(purchased_date + INTERVAL '#{EXPIRATION_PERIOD} seconds' as date)` == date} }

  def assign_dealership
    self.dealership = Dealership.find_by_dealer_code(self.dealer_code) if self.dealership.blank? && self.dealer_code.present?
  end

  def credits_remaining
    return 0 if has_expired
    self.credits.sum(:credit) - self.claims.sum(:credits)
  end
  
  def expiry_date
    EXPIRATION_PERIOD.since(purchased_date)
  end
  
  def has_expired(date = Date.today)
    date >= expiry_date
  end

  private
  def default_credits
    if self.credits.blank?
      self.credits.build credit: CREDITS_PER_NEW_PURCHASE, reason: "Welcome to Scion Swap!"
    end
  end
end
