class DateFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if (options[:after].present? && record.send(options[:after]) && value <= record.send(options[:after])) ||
      (options[:before].present? && record.send(options[:before]) && value >= record.send(options[:before])) ||
      (options[:after_or_equal].present? && record.send(options[:after_or_equal]) && value < record.send(options[:after_or_equal])) ||
      (options[:before_or_equal].present? && record.send(options[:before_or_equal]) && value > record.send(options[:before]))
      record.errors.add attribute, :date_format
    end
  end
end



class ReserveOnHolidayValidator < ActiveModel::Validator
  def validate(record)
    record.dealership.dealership_holidays.collect(&:date).each do |holiday|
      record.errors.add :user, :cannot_start_reserve_holiday  if  record.start_date == holiday
      record.errors.add :user, :cannot_drop_reserve_holiday  if  record.end_date == holiday
    end
  end
end

class AvailabilityValidator < ActiveModel::Validator
  def validate(record)
    return if record.fleet_group.blank? || record.start_date.blank? || record.end_date.blank? || record.pickup.present? || record.cancelled_at.present? || (record.changes.keys & ['start_date', 'end_date', 'fleet_group', 'fleet_group_id']).length == 0
    if (record.start_date..record.end_date).any?{|date| record.fleet_group.quantity(date) <= record.fleet_group.reservations.where{(start_date <= date) & (end_date >= date)}.count}
      record.errors.add :conflicts, "Sorry, the #{record.fleet_group.make} #{record.fleet_group.model} is not available #{record.start_date} - #{record.end_date}"
    end
  end
end

class MemberSufficientBalanceValidator < ActiveModel::Validator
  def validate(record)
    return if record.pickup.present? && record.dropoff_changed? && record.dropoff_was.nil? && (record.changes.keys.map(&:to_sym) == [:dropoff] || (record.changes.keys.map(&:to_sym) == [:dropoff, :pickup] && record.pickup.to_i == record.pickup_was.to_i))
    record.errors.add :user, :insufficient_balance unless record.user.nil? || record.length.blank? || record.trac_days < record.length
  end
end

class WhenDatesCanChangeValidator < ActiveModel::Validator
  def validate(record)
    record.errors.add :start_date, :frozen if record.start_date_changed? && (record.pickup.present? || record.cancelled_at.present?)
    record.errors.add :start_date, :past if record.start_date_changed? && record.start_date < Date.today
    record.errors.add :end_date, :frozen if record.end_date_changed? && (record.dropoff.present? || record.cancelled_at.present?)
    record.errors.add :end_date, :extend_only if record.end_date_changed? && record.end_date < record.start_date_was
    record.errors.add :end_date, :past if record.end_date_changed? && record.end_date < Date.today
    record.errors.add :cancelled_at, :frozen if record.cancelled_at_changed? && record.pickup.present?
  end
end

class CreditsRequiredForOtherPendingReservationsValidator < ActiveModel::Validator
  def validate(record)
    return unless record.length.present?
    total_credits = record.length + (record.should_incur_late_penalty? ? record.late_return_penalty[:credits] : 0)
    other_pending_reservations = record.user.reservations.pending
    other_pending_reservations = other_pending_reservations.where{id != record.id} unless record.new_record?
    if other_pending_reservations.any?{|r| r.user.credits_available(r) - total_credits <= 0}
      record.errors.add :conflicts, :pending_orphan
    end
  end
end

class MemberLeadTimeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if record.updated_by.present? && (record.updated_by.is_dealer_rep? || record.updated_by.is_member_support?)
    if record.changes.keys.map(&:to_s).include?(attribute.to_s)
      value_was, value_is = record.changes[attribute]
      if value_was.respond_to?(:to_date) && value_was.to_date <= 2.days.since(Date.today)
        record.errors.add attribute, :lead_time
      elsif value_is.respond_to?(:to_date) && value_is.to_date <= 2.days.since(Date.today)
        record.errors.add attribute, :lead_time
      end
    end
  end
end

class Reservation < ActiveRecord::Base
  belongs_to :fleet_group                                            
  belongs_to :rental_fleet_group, class_name: 'FleetGroup'
  belongs_to :user, :touch => true
  has_many :claims, :dependent => :destroy
  has_many :activity_logs, :dependent => :destroy
  attr_accessible :dropoff, :end_date, :pickup, :rental_agreement_id, :start_date, :fleet_group_id, :user_id, :cancelled_at, :waive_cancellation_penalty, :waive_late_penalty, :rental_fleet_group_id
  attr_accessor :waive_cancellation_penalty, :waive_late_penalty, :updated_by, :conflicts

  validates :fleet_group, :presence => true, :availability => true
  validates :rental_fleet_group, presence: true, if: :pickup
  validates :start_date, :presence => true, :member_lead_time => true
  validates :end_date, :presence => true, :date_format => { :after_or_equal => :start_date, :allow_nil => true }
  validates :user, :presence => true
  validates :pickup, :inclusion => {:in => [nil], :if => :cancelled_at}
  validates :dropoff, :inclusion => {:in => [nil], :if => lambda{|r| r.pickup.blank? || r.cancelled_at.present? } }
  validates_with WhenDatesCanChangeValidator, on: :update
  validates_with MemberSufficientBalanceValidator
  validates_with CreditsRequiredForOtherPendingReservationsValidator
  validates_with ReserveOnHolidayValidator , on: :create

  scope :member_email, lambda {|email| joins(:user).where{users.email == email} }
  scope :pending, lambda { where{(start_date >= Date.today) & (cancelled_at == nil) & (pickup == nil)}.order(:start_date) }
  scope :with_inventory_requirement, lambda { select{Reservation.arel_table.columns.map(&:name).concat([Reservation.inventory_need_subquery])} }

  before_validation :normalize_start_and_end_dates
  before_create :set_reservation_number
  before_save :set_trac_cost

  delegate :dealership, :to => :fleet_group

  MAX_CANCELLATION_PENALTY_CREDITS = 2
  CANCELLATION_PENALTY_THRESHOLD = 2.days
  TRAC_COST_PER_DAY = 50.00
  INVENTORY_NEED_ALIAS = 'inventory_needs'
  ACTIVITY_LOG_FIELDS = %w(id fleet_group_id start_date end_date pickup dropoff cancelled_at rental_agreement_id)

  def length
    return nil if end_date.nil? || start_date.nil?
    (end_date - start_date).to_i + 1   # Number of days from start to end, add 1 to include both days in the count
  end
  
  def total_days
    return length unless dropoff.present? && start_date.present?
    (dropoff.to_date - start_date).to_i + 1
  end

  def inventory_requirement
    if @_inventory_requirement.nil?
      if self[INVENTORY_NEED_ALIAS]
        @_inventory_requirement = self[INVENTORY_NEED_ALIAS].to_i
      else
        need = Reservation.where{id == my{id}}.select{Reservation.inventory_need_subquery}
        @_inventory_requirement = need.first[INVENTORY_NEED_ALIAS].to_i if need.length > 0
      end
    end
    @_inventory_requirement
  end

  def cancel
    self.cancelled_at = Time.zone.now
    self.save
  end

  def end
    self.dropoff = Time.zone.now
    self.save
  end

  def start
    self.pickup = Time.zone.now
    self.save
  end

  def trac_days
    if dropoff.present? && self['trac_days'].present?
      self['trac_days']
    else
      reward_days = nil
      if cancelled_at.present?
        return 0 unless should_incur_cancellation_penalty?
        reward_days = cancellation_penalty
      else
        reward_days = self.length || 0
      end
      [reward_days - self.user.credits_available(self), 0].max
    end
  end

  def trac_cost
    if dropoff.present? || cancelled_at.present?
      self['trac_cost']
    else
      TRAC_COST_PER_DAY * trac_days
    end
  end
  
  def should_incur_cancellation_penalty?
    cancelled_at.present? && cancelled_at > (start_date - CANCELLATION_PENALTY_THRESHOLD).end_of_day && !waive_cancellation_penalty
  end
  
  def should_incur_late_penalty?
    ((dropoff.present? && dropoff > end_date.end_of_day) || (dropoff.nil? && pickup.present? && Date.today > end_date)) && !waive_late_penalty
  end
  
  def should_incur_penalty?
    should_incur_cancellation_penalty? || should_incur_late_penalty?
  end
  
  def cancellation_penalty
    should_incur_cancellation_penalty? ? [self.length, MAX_CANCELLATION_PENALTY_CREDITS].min : 0
  end
  
  def late_return_penalty
    return nil unless should_incur_late_penalty?
    dropoff_date = (dropoff || Time.zone.now).to_date
    base_penalty = (dropoff_date - end_date).to_i.abs
    available_credits = [self.user.credits_available(self) - self.length,0].max
    if base_penalty <= available_credits
      penalty = {credits: base_penalty}
    else
      penalty = {credits: available_credits, payment: (base_penalty - available_credits) * TRAC_COST_PER_DAY}
    end
    if user.reservations.pending.any?{|r| r.user.credits_available(r) - self.length - penalty[:credits] <= 0}
      penalty[:must_cancel_pending] = true
    end
    penalty
  end

  private
  def normalize_start_and_end_dates
    self.start_date = self.start_date.to_date if self.start_date.present?
    self.end_date = self.end_date.to_date if self.end_date.present?
  end
  
  def set_trac_cost
    return if self['trac_cost'].present?
    if dropoff.present? || should_incur_cancellation_penalty?
      self.trac_days = trac_days
      self.trac_cost = TRAC_COST_PER_DAY * trac_days
    end
  end
  
  def set_reservation_number
    self.number = rand(10**10).to_s.rjust(10,'0')
  end

  def self.inventory_need_subquery
    Reservation.from('reservations r1').
      where{(fleet_group_id == r1.fleet_group_id) & (start_date <= r1.end_date) & (end_date >= r1.start_date)}.
      select{count r1.id}.as(INVENTORY_NEED_ALIAS)
  end
end
