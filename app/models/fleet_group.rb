class FleetGroup < ActiveRecord::Base
  attr_accessor :fleet_group_quantity
  attr_accessible :make, :model, :fleet_group_quantity, :available_for_reservation
  belongs_to :dealership
  has_many :fleet_group_quantities, :dependent => :destroy
  has_many :reservations do
    def begin_today
      where(start_date: Date.today)
    end
    def end_today
      where(end_date: Date.today)
    end
    def active
      where{(pickup != nil) & (dropoff == nil)}
    end
    def late
      active.where{end_date < Date.today}
    end
    def today
      where{((start_date <= Date.today) & (end_date >= Date.today)) | ((pickup <= Date.today) & (dropoff == nil))}
    end
  end
  
  def self.available_between(from_date, to_date)
    self.joins(:fleet_group_quantities).joins("inner join generate_series('#{from_date}', '#{to_date}', '1 day'::interval) as res_date on (fleet_group_quantities.start_date=(SELECT MAX(start_date) from fleet_group_quantities fgq WHERE fgq.fleet_group_id=fleet_group_quantities.fleet_group_id AND fgq.start_date <= res_date))").
      group{fleet_groups.id}.
      having("MIN(fleet_group_quantities.quantity - (select count(*) from reservations where fleet_group_id=fleet_group_quantities.fleet_group_id AND res_date BETWEEN start_date AND end_date)) > 0")
  end

  before_validation :update_fleet_group_quantity, :if => :fleet_group_quantity
  after_validation :handle_fleet_group_quantity
  
  def upcoming_quantity_changes
    self.fleet_group_quantities.upcoming
  end

  def quantity(date=Date.today)
    fgq = self.fleet_group_quantities.where{start_date <= date}.order(:start_date).reverse_order.first
    fgq.nil? ? 0 : fgq.quantity
  end

  def availability_between(from_date, to_date)
    ab = self.fleet_group_quantities.
              joins("inner join generate_series('#{from_date}', '#{to_date}', '1 day'::interval) as res_date on (fleet_group_quantities.start_date=(SELECT MAX(start_date) from fleet_group_quantities fgq WHERE fgq.fleet_group_id=fleet_group_quantities.fleet_group_id AND fgq.start_date <= res_date))").
              select("res_date, fleet_group_quantities.quantity - (select count(*) from reservations where fleet_group_id=fleet_group_quantities.fleet_group_id AND res_date BETWEEN start_date AND end_date) as available_count").
              order("fleet_group_id, res_date").map{|row| [Date.parse(row['res_date']), row['available_count'].to_i]}
    return Hash[ab]
  end

  def humanize
    "#{self.make} #{self.model}"
  end

  private
  def update_fleet_group_quantity
    self.fleet_group_quantities.new(self.fleet_group_quantity)
  end

  def handle_fleet_group_quantity
    if self.errors.has_key?(:fleet_group_quantities)
      self.errors[:fleet_group_quantities].delete_if{|er| er == 'is invalid'}
      self.fleet_group_quantities.select{|fgq| fgq.errors }.each do |quantity|
        quantity.errors.each do |field, message|
          self.errors.add(:fleet_group_quantities, "#{field} #{message}")
        end
        self.fleet_group_quantities.delete(quantity) if quantity.new_record?
      end
    else
      self.fleet_group_quantity = nil
    end
  end
end
