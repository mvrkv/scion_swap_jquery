class ActivityLog < ActiveRecord::Base
  belongs_to :user
  belongs_to :reservation
  belongs_to :changed_by_user, class_name: 'User'
  serialize :resource_changes, ActiveRecord::Coders::Hstore
  attr_accessible :reservation, :user, :resource_changes, :changed_by_user
  default_scope order(:created_at).reverse_order
  
  validates :resource_changes, presence: true
  validates :changed_by_user, presence: true
  validates :user, :inclusion => {in: [nil], if: :reservation}
  validates :reservation, :exclusion => {in: [nil], unless: :user}
  before_save :normalize_changes
  after_find :denormalize_changes
  
  def old_values
    Hash[resource_changes.map{|field, change| [field, change[0]] }]
  end
  
  def new_values
    Hash[resource_changes.map{|field, change| [field, change[1]] }]
  end
  
  def changed_resource
    self.user || self.reservation
  end
  
  def humanize
    humanized = []
    resource_changes.each do |field, change|
      was, is = change
      if field =~ /_id$/ && changed_resource.respond_to?(field.sub(/_id$/, ''))
        change_field = field.sub(/_id$/, '').camelize.constantize
        if is.present? && (is_obj = change_field.find_by_id(is)) && is_obj.respond_to?(:humanize)
          is = is_obj.humanize
        end
        if was.present? && (was_obj = change_field.find_by_id(was)) && was_obj.respond_to?(:humanize)
          was = was_obj.humanize
        end
      end
      default = "changed #{field} from #{was} to #{is}"
      humanized.push I18n.t "#{field}.#{was}", scope: [:activity_log, changed_resource.class.name.downcase.to_sym], is: is, was: was, :default => [field.to_sym, default]
    end
    changed_by_label = "#{changed_by_user.first_name} #{changed_by_user.last_name}"
    if changed_by_user.is_active_dealer_rep?
      changed_by_label << ", dealer admin for #{changed_by_user.dealership.name},"
    elsif changed_by_user.is_active_member?
      changed_by_label << ", member,"
    elsif changed_by_user.is_active_member_support?
      changed_by_label << ", member support,"
    end
    changed_by_label.concat(' ').concat humanized.join(', ')
  end
  
  protected
  def normalize_changes
    resource_changes.each do |field, change| 
      if changed_resource.class.const_get(:ACTIVITY_LOG_FIELDS).include? field.to_s
        resource_changes[field] = "'#{field_value_for_storage(change[0])}', '#{field_value_for_storage(change[1])}'"
      else
        resource_changes.delete field
      end
    end
    true # Ensure return value does not cause rollback
  end
  
  def denormalize_changes
    resource_changes.each{|field, change| resource_changes[field] = change.scan(/'([^']*)'/).flatten }
    true # Ensure return value does not cause rollback
  end
  
  def field_value_for_storage(val)
    val.nil? ? 'nil' : val.to_s.gsub(/'/, "\'")
  end
end