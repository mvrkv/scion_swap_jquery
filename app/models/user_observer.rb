class UserObserver < ActiveRecord::Observer
  def after_create(user)
    if user.is_member? && user.roles['member'] == 'pending' 
      UserMailer.delay(:run_at => 1.weeks.from_now).activation_reminder(user)
      UserMailer.delay(:run_at => 2.weeks.from_now).activation_reminder(user)
    end
  end

  def after_update(user)
    if user.is_member?
      if user.confirmed? && user.tos_accepted_at.present? && ((user.confirmed_at_changed? && user.confirmed_at_was.nil?) || (user.tos_accepted_at_changed? && user.tos_accepted_at_was.nil?))
        UserMailer.delay.welcome(user)
      elsif user.confirmed? && user.tos_accepted_at.nil? && user.confirmation_token_changed? && user.confirmation_token.nil?
        user.update_attribute(:confirmation_token, "#{user.confirmation_token_was} ")
      elsif !(user.changes.keys & User::ACCOUNT_INFO_FIELDS).blank?
        UserMailer.delay.account_updated(user)
      end
    end
    log_changes = user.changes.select{|field, change| User::ACTIVITY_LOG_FIELDS.include? field }
    unless log_changes.blank?
      user.activity_logs.create!(user: user, changed_by_user: user.updated_by || user, resource_changes: log_changes)
    end
  end
end
