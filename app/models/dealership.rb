class OperatingHoursValidator < ActiveModel::Validator
  def validate(record)
    [:is_open, :open_time, :close_time].each do |field|
      record.errors.add field, 'can only have one element per day of the week' if record[field].present? && record[field].length > 7
    end
    (0..6).each do |wday|
      next unless record.is_open[wday]
      if record.open_time[wday].blank?
        record.errors.add :open_time, "is required if the dealership is open on #{Date::DAYNAMES[wday]}"
      end
      if record.close_time[wday].blank?
        record.errors.add :close_time, "is required if the dealership is open on #{Date::DAYNAMES[wday]}"
      end
      if record.open_time[wday].present? && record.close_time[wday].present? && Time.parse(record.open_time[wday]) >= Time.parse(record.close_time[wday])
        record.errors.add :open_time, 'must be before close time'
      end
    end
    if (record.is_open_changed? || record.open_time_changed? || record.close_time_changed?) && !(record.is_open_was.nil? || record.open_time_was.nil? || record.close_time_was.nil?)
      change_conflicts = 0
      (0..6).each do |wday|
        if (record.is_open_was[wday] && !record.is_open[wday]) || 
            (record.is_open[wday] && (record.is_open_was[wday] &&
              (record.open_time[wday] > record.open_time_was[wday] || record.close_time[wday] < record.close_time_was[wday])))
          change_conflicts += record.reservations.where{(cancelled_at == nil) & (((extract(`dow from start_date`) == my{wday}) & (pickup == nil)) | ((extract(`dow from end_date`) == my{wday}) & (dropoff == nil)))}.size
        end
      end
      record.errors.add :conflicts, change_conflicts.to_s if change_conflicts > 0 && record.override_conflicts.to_i != change_conflicts
    end
  end
end

class Dealership < ActiveRecord::Base
  attr_accessible :street, :city, :name, :state, :zip, :dealer_code, :latitude, :longitude, :is_open, :open_time, :close_time, :override_conflicts, :instructions, :phone
  attr_accessor :override_conflicts
  has_many :dealer_reps, :class_name => 'User', :foreign_key => 'dealership_id'
  has_many :fleet_groups
  has_many :available_fleet_groups, class_name: 'FleetGroup', conditions: {:available_for_reservation => true}
  has_many :purchases
  has_many :members, through: :purchases, class_name: 'User'
  has_many :dealership_holidays
  has_many :reservations, through: :fleet_groups
  scope :single, order(:id).limit(1)

  validates :dealer_code, presence: true
  validates :instructions, length: {maximum: 200}
  validates :name, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :street, presence: true
  validates :zip, presence: true
  validates :phone, format: {with: /^\d{10}$/}
  validates_with OperatingHoursValidator

  geocoded_by :address
  before_validation :normalize_phone
  after_find :denormalize_phone
  after_initialize :set_default_operating_hours
  after_validation :geocode, :if => lambda {|dealer| (dealer.city_changed? || dealer.state_changed? || dealer.zip_changed?) && (latitude.blank? || longitude.blank?)}
  
  def address
    [[self.street, self.city, self.state].compact.join(', '), self.zip].compact.join(' ')
  end

  private
  def set_default_operating_hours
    if self.is_open.blank?
      self.is_open = [false] * 7
    end
    self.open_time  = [nil] * 7 if self.open_time.blank?
    self.close_time = [nil] * 7 if self.close_time.blank?
  end
  
  def normalize_phone
    return if self.phone.blank?
    self.phone = self.phone.gsub(/[\(\)\s-]/, '')
  end
  
  def denormalize_phone
    return if self.phone.blank?
    self.phone = "(#{self.phone[0..2]}) #{self.phone[3..5]}-#{self.phone[6..-1]}"
  end
end
