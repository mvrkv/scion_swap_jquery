class ReservationObserver < ActiveRecord::Observer
  def after_create(reservation)
    if reservation.start_date >= Date.today
      ReservationMailer.delay.new_member_reservation(reservation)
      ReservationMailer.delay.reservation_mail_to_dealer(reservation)
      
    end
    
    reservation.activity_logs.create!(reservation: reservation, changed_by_user: reservation.updated_by, resource_changes: {id: [nil, reservation.id]}) unless reservation.updated_by.blank?
    true
  end

  def after_update(reservation)
    if reservation.dropoff_changed? && reservation.dropoff_was.nil?
      reservation_debit = reservation.length
      reservation_debit += reservation.late_return_penalty[:credits] if reservation.should_incur_late_penalty? && reservation.late_return_penalty[:credits] > 0
      charge_reservation_credits(reservation, reservation_debit)
    elsif reservation.cancelled_at_changed? && reservation.cancelled_at_was.nil? && reservation.should_incur_cancellation_penalty?
      charge_reservation_credits(reservation, reservation.cancellation_penalty)
    end
    
    log_changes = reservation.changes.select{|field, change| Reservation::ACTIVITY_LOG_FIELDS.include? field }
    unless log_changes.blank? || reservation.updated_by.blank?
      reservation.activity_logs.create!(reservation: reservation, changed_by_user: reservation.updated_by, resource_changes: log_changes)
    end
    true
  end

  private
  def charge_reservation_credits(reservation, credits_count)
    credits_charged = 0
    reservation.user.purchases.order(:purchased_date).each do |purchase|
      if purchase.credits_remaining > 0
        credits_to_charge = credits_count - reservation.claims.map(&:credits).sum
        credits_to_charge = [credits_to_charge, purchase.credits_remaining].min
        if credits_to_charge > 0
          reservation.claims.create!(credits: credits_to_charge, purchase: purchase)
          credits_charged += credits_to_charge
        end
        break if reservation.claims.map(&:credits).sum >= credits_count
      end
    end
  end
end
