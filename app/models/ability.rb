class Ability
  include CanCan::Ability

  def initialize(user)
    can :auth_token, User
    can :unsubscribe_activation, User
    can :deactivate_reminder_mail, User
    return if user.nil? || !user.is_a?(User)

    can :current, User
    can [:update], User, :id => user.id
    can :activate, User, :id => user.id if user.roles.any?{|role, status| status == 'pending' }
    add_member_abilities(user) if user.is_member?
    add_dealer_rep_abilities(user) if user.is_active_dealer_rep?
    add_member_support_abilities(user) if user.is_active_member_support?
    cannot :destroy, [Reservation, User]
    cannot [:create, :destroy], [Dealership, FleetGroup]
  end

  private
  def add_member_abilities(user)
    if user.is_active_member?
      can :read, FleetGroup
      can [:read, :browse], :search
      can :manage, User, :id => user.id
      cannot :create, User
      can :manage, Reservation, :user_id => user.id
      can [:index, :read], Dealership
    elsif user.roles['member'] != 'disabled'
      can :read, ActivationSurveyQuestion
      can :create, ActivationSurveyAnswer, :user_id => user.id
    end
    cannot :waive_penalty, Reservation
    cannot :add_credits, User
  end

  def add_dealer_rep_abilities(user)
    can :manage, FleetGroup, :dealership_id => user.dealership_id
    can :manage, FleetGroupQuantity, :fleet_group_id => user.dealership.fleet_group_ids
    can :manage, Dealership, :id => user.dealership_id
    can :index, :search
    can :manage, Reservation, :fleet_group => {:dealership_id => user.dealership_id}
    can :waive_penalty, Reservation, :fleet_group => {:dealership_id => user.dealership_id}
    can :manage, DealershipHoliday, :dealership_id => user.dealership_id
    can [:read, :members], User
    can [:update, :resend_confirmation], User, :purchases => {:dealership_id => user.dealership_id}
    can [:admin, :change_password,:update_password], User, :id => user.id
    cannot :add_credits, User
  end

  def add_member_support_abilities(user)
    can :manage, User.members
    can [:new, :resend_confirmation,:change_password,:update_password], User
    can [:create, :update, :read, :cancel], Reservation
    can [:read, :browse], :search
    can :waive_penalty, Reservation
    can [:read,:create,:update,:members], User do |target|
      !target.is_member?
    end
    can :manage, [Dealership, DealershipHoliday]
    can :index, ActivationSurveyAnswer
    can [:admin, :change_password,:update_password], User, :id => user.id
  end
end
