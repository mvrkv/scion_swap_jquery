class SwapRolesValidator < ActiveModel::Validator
  def validate(record)
    invalid_role_names = record.roles.keys.map(&:to_s) - User::ROLES
    if invalid_role_names.present?
      record.errors.add(:roles, :inclusion)
    else
      record.roles.each do |name, status|
        record.errors.add(:roles, :inclusion, :message => "is not a valid status for role #{name}") unless User::ROLE_STATUSES[name.to_s].include?(status.to_s)
      end
      record.errors.add(:roles, :inclusion, :message => 'member cannot be active because all rewards are expired') if record.is_active_member? && record.purchases.maximum(:purchased_date) < Date.today - 1.year
    end
  end
end

class User < ActiveRecord::Base
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :roles, :dealership, :dealership_id, :tos_accepted_at, :activation_survey_answers, :customer_ref_id,:activation_mail_unsubscribe
  attr_accessor :updated_by
  serialize :roles, ActiveRecord::Coders::Hstore
  devise :database_authenticatable, :token_authenticatable, :confirmable, :recoverable, :rememberable, :trackable, :timeoutable, :validatable
  has_many :purchases, :dependent => :destroy
  has_many :reservations
  belongs_to :dealership
  has_many :activation_survey_answers, :dependent => :destroy
  has_many :changes_made, class_name: 'ActivityLog', foreign_key: :changed_by_user_id
  has_many :activity_logs, foreign_key: :user_id

  validates :customer_ref_id, uniqueness: {allow_nil: true}
  validates :email, uniqueness: true
  validates :roles, swap_roles: true, presence: true
  validates :dealership, presence: {if: :is_dealer_rep? }
  validates :password, :password_confirmation, presence: true, on: :update_password 

  ACCOUNT_INFO_FIELDS = %w(first_name last_name email password)
  ACTIVITY_LOG_FIELDS = %w(email first_name last_name confirmed_at)
  ROLES = %w(member dealer_rep member_support)
  ROLE_STATUSES = {'member' => %w(pending active expired disabled), 'dealer_rep' => %w(pending active disabled), 'member_support' => %w(pending active disabled)}
  ROLES.each do |role|
    scope "#{role}s", where{defined(roles, "#{role}")}

    define_method "#{role}_status" do
      return nil if self.roles.nil?
      self.roles[role]
    end

    define_method "is_#{role}?" do
      return self.roles.keys.include? role
    end

    define_method "is_active_#{role}?" do
      self.send("#{role}_status").to_s == 'active'
    end
  end
  def is_pending_user?
    self.roles.all?{|role, status| status == 'pending' }
  end
  def disabled?
    roles.size > 0 && roles.all?{|role, status| status == 'disabled' }
  end

  def self.for_dealerships(dealerships)
    joins{purchases.outer}.where{(purchases.dealership_id.in dealerships) | (dealership_id.in dealerships)}
  end

  before_validation :set_default_role, :on => :create, :if => lambda {|u| u.roles.blank? }
  before_update :update_role_status, :if => lambda {|u| u.confirmed? && u.is_pending_user? }
  before_save :ensure_authentication_token

  def set_default_role
    self.roles = {} if self.roles.nil?
    self.roles['member'] = 'pending' if self.roles.blank? && self.customer_ref_id.present?
    true
  end

  def update_role_status
    if !confirmed? || first_name.blank? || last_name.blank? || email.blank? ||
      encrypted_password.blank? || tos_accepted_at.blank? || tos_accepted_at > Time.zone.now
      self.roles.keys.each{|role| self.roles[role] = 'pending' }
    elsif !self.disabled?
      self.roles.keys.select{|role| self.roles[role] == 'pending'}.each{|role| self.roles[role] = 'active' }
    end
  end
  
  def activation_survey_answers=(survey_answers)
    if survey_answers && survey_answers.is_a?(Array)
      answer_arr = survey_answers
      until answer_arr.empty? do
        answer = answer_arr.pop
        if (question_id = answer.delete(:question_id))
          answer[:activation_survey_question] = ActivationSurveyQuestion.find question_id
        end
        if answer.is_a?(Hash)
          if (survey_answer = self.activation_survey_answers.where(:activation_survey_question_id => answer[:activation_survey_question].id).first)
            survey_answer.update_attribute(:answer, answer[:answer])
          else
            activation_survey_answers.build(answer)
          end
        elsif !activation_survey_answers.exists?(answer)
          activation_survey_answers << answer
        end
      end
    else
      super(survey_answers)
    end
  end

  def member_dealership
    return nil unless self.is_member?
    purchase = self.purchases.first
    purchase && purchase.dealership
  end

  def as_json(options={})
    if (options.keys & ['only', :only, 'except', :except]).blank?
      options[:only] = ["id", "email", "first_name", "last_name", "roles", "tos_accepted_at", "customer_ref_id", "dealership_id" ]
    end
    super(options)
  end

  def password_required_with_member_role?
    return false if !self.roles.respond_to?(:keys) || self.roles.empty? || self.roles.all?{|role, status| status == 'pending' }
    password_required_without_member_role?
  end
  alias_method_chain :password_required?, :member_role

  def credits
    self.purchases.joins{credits}.sum(:credit).to_i
  end

  def credits_remaining
    self.purchases.sum(&:credits_remaining)
  end

  def credits_pending(before_reservation=nil)
    pending = self.reservations.pending
    pending = pending.where{(id != before_reservation.id) & (start_date < before_reservation.start_date)} if before_reservation.present? && before_reservation.is_a?(Reservation)
    pending.sum(&:length)
  end

  def credits_available(before_reservation=nil)
    [credits_remaining - credits_pending(before_reservation), 0].max
  end

  def credit_expirations
    credits_by_date = self.purchases.map(&:credits).flatten.group_by{|c| 1.year.since(c.purchase.purchased_date)}
    Hash[credits_by_date.map{|k,v| [k,v.map(&:credit).inject(:+)]}]
  end
  
  def transactions
    transactions = []
    self.purchases.map(&:credits).flatten.each{|cr| transactions.push({date: cr.purchase.purchased_date, summary: cr.reason, credits: cr.credit})}
    my_reservations = Reservation.where{user_id == my{self.id}}
    Claim.where{reservation_id.in (my_reservations.select(:id))}.each{|cl| transactions.push({date: cl.created_at.to_date, summary: 'Rental', credits: -cl.credits, reservation_id: cl.reservation_id})}
    transactions.sort_by!{|t| t[:date] }
    balance = 0
    transactions.each do |t| 
      t[:balance] = (balance += t[:credits])
      if (res = Reservation.find_by_id(t[:reservation_id]))
        t[:reservation_label] = "#{res.fleet_group.make} #{res.fleet_group.model} from #{res.dealership.name}"
      end
    end
  end
  
  handle_asynchronously :send_devise_notification
end
