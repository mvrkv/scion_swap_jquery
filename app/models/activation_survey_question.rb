class ActivationSurveyQuestion < ActiveRecord::Base
  has_many :activation_survey_answers
  attr_accessible :question, :available_answers
end
