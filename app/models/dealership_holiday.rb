class HolidayConflictValidator < ActiveModel::Validator
  def validate(record)
    if record.date && record.dealership.present?
      conflict_count = record.dealership.reservations.where{(cancelled_at == nil) & (((start_date == record.date) & (pickup == nil)) | ((end_date == record.date) & (pickup != nil) & (dropoff == nil)))}.size
      record.errors.add :conflicts, conflict_count.to_s if conflict_count > 0 && record.override_conflicts.to_i != conflict_count
    end
  end
end

class DealershipHoliday < ActiveRecord::Base
  belongs_to :dealership
  default_scope where{extract(`year from date`) >= my{Date.today.year}}
  attr_accessible :dealership_id, :date, :override_conflicts
  attr_accessor :override_conflicts

  validates :dealership, presence: true
  validates :date, presence: true, holiday_conflict: true
end
