class InventoryNeedsValidator < ActiveModel::Validator
  def validate(record)
    return if record.fleet_group.nil?
    needs = record.fleet_group.reservations.with_inventory_requirement.where{end_date >= record.start_date}
    if (next_quant = record.fleet_group.fleet_group_quantities.where{start_date > record.start_date}.first)
      needs = needs.where{start_date < next_quant.start_date}
    end
    inventory_shortage = (needs.map(&:inventory_requirement).max || 0) - record.quantity
    record.errors.add :inventory_shortage, inventory_shortage.to_s if inventory_shortage > 0 && record.override_inventory.to_i != inventory_shortage
  end
end

class QuantityChangedValidator < ActiveModel::Validator
  def validate(record)
    return if record.fleet_group.nil? || record.quantity.nil? || record.start_date.nil?
    record.errors.add :quantity, :quantity_changed if record.fleet_group.quantity(record.start_date) == record.quantity
  end
end

class FleetGroupQuantity < ActiveRecord::Base
  belongs_to :fleet_group
  attr_accessible :fleet_group, :quantity, :start_date, :fleet_group_id, :override_inventory
  attr_accessor :override_inventory
  default_scope order(:start_date)

  validates :start_date, presence: true, uniqueness: {scope: :fleet_group_id}
  validates :quantity, presence: true, quantity_changed: true, inventory_needs: true
  validates :fleet_group, presence: true
  
  scope :upcoming, where{start_date >= Date.today}
end
