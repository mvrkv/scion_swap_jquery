class Claim < ActiveRecord::Base
  belongs_to :reservation
  belongs_to :purchase
  attr_accessible :credits, :reservation_id, :purchase_id, :purchase
end
