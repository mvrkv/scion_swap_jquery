class Credit < ActiveRecord::Base
  belongs_to :purchase
  attr_accessible :credit, :purchase, :reason
  validates :credit, numericality: {only_integer: true, greater_than: 0}
  validates :purchase, presence: true
end
