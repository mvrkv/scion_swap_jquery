class ActivationSurveyAnswer < ActiveRecord::Base
  belongs_to :user
  belongs_to :activation_survey_question
  attr_accessible :user, :activation_survey_question, :answer

  validates :answer, presence: true, inclusion: {in: lambda {|ans| ans.activation_survey_question.available_answers}}
  validates :user_id, presence: true
  validates :activation_survey_question_id, presence: true, uniqueness: {scope: :user_id}
end
