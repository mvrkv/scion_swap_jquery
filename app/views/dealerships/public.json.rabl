extends "dealerships/attributes"
child(:available_fleet_groups => :fleet_groups) do
  attributes :id, :make, :model, :description, :image_url, :product_url

  node(:quantity) do |fleet_group|
    return nil unless fleet_group.is_a?(FleetGroup)
    @from_date.present? || @to_date.present? ? 
      fleet_group.availability_between(@from_date, @to_date).values.min : 
      fleet_group.quantity
    end
end