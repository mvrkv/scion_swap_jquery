child(@dealership.nearbys.select{|nearby| can? :read, nearby} => :nearbys) do |nearby|
  attributes :id, :name, :city, :state, :street, :zip, :latitude, :longitude
end