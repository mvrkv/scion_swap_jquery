object @dealership
extends "dealerships/attributes"
attributes :is_open, :open_time, :close_time
child(:fleet_groups) do
  attributes :id, :make, :model, :description, :image_url, :dealership_id, :available_for_reservation
  extends('fleet_groups/admin_schedule', :locals => {:dates => (Date.today.beginning_of_week..Date.today.end_of_week)})
  child(:upcoming_quantity_changes => :quantities) do
    extends('fleet_group_quantities/public')
  end
end
child(:dealership_holidays => :holidays) do
  attributes :id, :dealership_id, :date
end
