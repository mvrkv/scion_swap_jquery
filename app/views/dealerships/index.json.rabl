collection @dealerships

if current_user.is_active_member_support? || current_user.is_active_dealer_rep?
  extends "dealerships/admin"
else
  extends "dealerships/public"
end