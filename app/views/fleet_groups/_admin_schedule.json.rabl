dates = locals[:dates].kind_of?(Enumerable) ? (locals[:dates].min..locals[:dates].max) : [locals[:dates]]

node(:fleet_size) do
  sizes = dates.map do |date|
    [date, root_object.quantity(date)]
  end
  Hash[sizes]
end
node :availability do
  root_object.availability_between(dates.min, dates.max)
end
node(:reservations) do |d|
  partial('reservations/attributes', :object => d.reservations.today)
end
