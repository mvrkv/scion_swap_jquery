attributes :id, :start_date, :end_date, :pickup, :dropoff, :rental_agreement_id, :cancelled_at, :number
attributes :trac_cost => :payment
node(:credits) {|reservation| reservation.length - reservation.trac_days }
