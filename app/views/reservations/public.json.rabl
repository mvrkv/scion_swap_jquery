extends 'reservations/attributes'
node(:penalty, :if => lambda{|res| res.should_incur_penalty?}) do |res|
  r = {}
  r[:lateReturn] = res.late_return_penalty if res.should_incur_late_penalty?
  r[:cancellation] = res.cancellation_penalty if res.should_incur_cancellation_penalty?
  r
end
child :user do
  extends "users/public"
end
child :fleet_group do
  extends "fleet_groups/public"
end
node(:dealership) do |reservation|
  return nil unless reservation.is_a?(Reservation)
  partial "dealerships/attributes", object: reservation.fleet_group.dealership
end
