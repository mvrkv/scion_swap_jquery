object @reservation
extends "reservations/public"
child @future_cancellations => :future_cancellations do
  attributes :id, :start_date, :end_date
  child :fleet_group do
    extends "fleet_groups/public"
  end
end
