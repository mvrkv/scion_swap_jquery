object @user

extends "users/public"
if @current_user && @current_user == @user
  node(:upcoming_reservations) do |user|
    partial "reservations/public", object: user.reservations.pending
  end
  node(:transactions) {|user| user.transactions }
end
