attributes :id, :email, :first_name, :last_name, :tos_accepted_at, :customer_ref_id, :roles, :updated_at
child(:purchases => :credits) do
  attributes :purchased_date, :credits_remaining
  node(:credits) {|purchase| purchase.credits.sum(:credit)}
end
child(:dealership, :if => lambda{|user| user.is_active_dealer_rep?}) do
  extends 'dealerships/admin'
end
node(:member_dealership, :if => lambda{|user| user.is_member? && user.member_dealership.present?}) do |user|
  partial 'dealerships/public', object: user.member_dealership
end