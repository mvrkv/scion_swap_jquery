collection @activity_logs
attributes :id, :resource_changes, :created_at
attributes :humanize => :summary
child :user => :user do
  extends "users/public"
end
child :reservation => :reservation do
  extends "reservations/public"
end
child :changed_by_user => :changed_by_user do
  extends "users/public"
end
