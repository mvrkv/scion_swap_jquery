object @dealership

extends 'dealerships/public'
extends 'dealerships/nearby'
attributes :is_open, :open_time, :close_time
