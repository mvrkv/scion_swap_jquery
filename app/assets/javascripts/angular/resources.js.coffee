window.swap.toTitleCase = (str) ->
  str.replace(/_?\w[^\s_]*/g, (txt) ->
    txt = txt.replace(/^_./, (t) -> t.substr(1))
    txt.charAt(0).toUpperCase() + txt.substr(1)
  )
window.swap.toUnderscore = (str) ->
  str.charAt(0).toLowerCase() + str.substr(1).replace(/[A-Z]/g, (ch) -> '_' + ch.toLowerCase())
window.swap.params = (params) ->
  obj = {}
  $.map $.param(params).split("&"), (pair) ->
    [name, value] = $.map(pair.split("="), (i) -> decodeURIComponent(i))
    obj[name] = value
  obj

$.each(window.models, (model, resourceParams) ->
  modelName = window.swap.toTitleCase(model)
  serviceName = "#{modelName.charAt(0).toLowerCase()}#{modelName.substr(1)}sService"
  angular.module(serviceName, ['ngResource'])
    .factory modelName, ["$resource", ($resource) ->
        url = resourceParams.url
        if url?
          delete resourceParams.url
        else
          url = "/#{window.swap.toUnderscore(model)}s/:id"
        params = {id: "@id"}
        if resourceParams.params?
          params = angular.extend(params, resourceParams.params)
          delete resourceParams.params
        actions = angular.extend({update: {method: 'PUT'}}, resourceParams.actions)
        model_factory = $resource(url, params, actions)
        model_factory['_save'] = model_factory.save
        model_factory['save'] = () ->
          arguments[0] = this.strip_relations(arguments[0])
          this._save.apply(this, arguments)
        model_factory['_update'] = model_factory.update.bind(model_factory)
        model_factory['update'] = () ->
          resourceIdx = if !arguments[1]? || angular.isFunction(arguments[1]) then 0 else 1
          arguments[resourceIdx] = this.strip_relations(arguments[resourceIdx])
          this._update.apply(this,arguments)
        model_factory['strip_relations'] = (a1) ->
          if a1
            _obj = angular.copy a1
            if _obj['swap-keep']?
              keepers = _obj['swap-keep']
              delete _obj['swap-keep']
              if angular.isString(keepers)
                keepers = [keepers]
            $.each _obj, (attr_name, attr) ->
              return true if keepers? && keepers.some (attribute) -> attribute == attr_name
              if $.isArray(attr)
                if attr.length > 0 && typeof($.parseJSON(JSON.stringify(attr[0]))) == 'object'
                  delete _obj[attr_name]
              else if (attr_str=JSON.stringify(attr)) && (typeof(attr_json=$.parseJSON(attr_str)) == 'object') && attr_json != null
                if attr['id']?
                  _obj["#{attr_name}_id"] = attr['id']
                delete _obj[attr_name]
              null
          _obj
        model_factory
      ])
