@swap.config(['$routeProvider', ($routeProvider) ->
   $routeProvider
    .when('/users/dashboard', {templateUrl: '/assets/dashboards/member.html', controller: MemberDashboardCtrl})
    .when('/users/admin', {templateUrl: '/assets/dashboards/admin.html', controller: AdminDashboardCtrl})
])

@AdminDashboardCtrl = ($scope, $rootScope, $window, $location, $timeout, User, Reservation, Dealership) ->
  $scope.changeDealership = {show: false, options: {dialogClass: 'changeAdminDealership modal'}}
  getReservationCount = (filterFunction) ->
    return 0 unless $scope.dealership?
    unless filterFunction?
      filterFunction = () -> true
    filtered_res = $.map($scope.dealership.fleet_groups.map((fg) -> fg.reservations.filter((res) -> filterFunction(res))), (n) -> n)
    filtered_res.length
  setDealership = (dealer) ->
    $scope.dealership = if dealer instanceof Dealership then dealer else new Dealership(dealer)
    $scope.inventoryDays = for day,size of $scope.dealership.fleet_groups[0].fleet_size
      Date.parseWithoutTimezone(day)
    $scope.pickupsTotal = getReservationCount (res) -> res.start_date == $scope.todayFormatted && !res.cancelled_at?
    $scope.pickupsRemaining = getReservationCount (res) -> res.start_date == $scope.todayFormatted && !res.pickup? && !res.cancelled_at?
    $scope.returnsTotal = getReservationCount (res) -> res.end_date == $scope.todayFormatted
    $scope.returnsRemaining = getReservationCount (res) -> res.end_date == $scope.todayFormatted && res.pickup? && !res.dropoff?
    $scope.activeRentalsCount = getReservationCount (res) -> (if res.isActive? then res else new Reservation(res)).isActive()
    $scope.fleetCount = ( ->
      return 0 unless $scope.dealership?
      fleet_per_group = $.map($scope.dealership.fleet_groups.map((fg) -> fg.fleet_size[$scope.todayFormatted]), (n) -> n)
      total_fleet = 0
      $.each fleet_per_group, (_, grp) -> total_fleet += grp
      total_fleet
    )()
    $scope.rentalHours = $scope.dealership.rentalHours()

  waitForUser = ->
    $scope.user = $scope.currentUser
    if !$scope.user? || !$scope.user.id?
      $timeout(waitForUser, 100)
    else if $scope.user.isMemberSupport()
        $scope.getCurrentDealership setDealership
    else if $scope.user.isDealerRep()
      setDealership $scope.user.dealership
  waitForUser()
  
  $scope.todayFormatted = (new Date()).toShortDateString()
  $scope.inventoryCount = (fleetGroup, day) ->
    if @dealership? && day? && !@dealership.is_open[day.getDay()]
      '--'
    else if fleetGroup? && day?
      fleetGroup.availability[day.toShortDateString()]
    else if fleetGroup? && !day?
      Math.min.apply(null, (count for day, count of fleetGroup.availability))
    else if !fleetGroup? && day? && @dealership?
      Math.min.apply(null, $.map(@dealership.fleet_groups.map((fg) -> fg.availability[day.toShortDateString()]), (n) -> n))
    else
      null
  $scope.manageInventory = ->
    $location.path("/dealerships/#{@dealership.id}/inventory") if @dealership?
  $scope.manageCalendar = ->
    $location.path("/dealerships/#{@dealership.id}/schedule") if @dealership?
  $scope.goToTodayPickups = -> 
    $rootScope.holdData 'reservationStatus', 'pickups'
    @goToReservations()
  $scope.goToTodayReturns = -> 
    $rootScope.holdData 'reservationStatus', 'dropoffs'
    @goToReservations()
  $scope.goToTodayRentals = -> 
    $rootScope.holdData 'reservationStatus', 'active'
    @goToReservations()
  $scope.goToReservations = ->
    $location.path("/dealerships/#{@dealership.id}/reservations") if @dealership?
  $scope.manageDealership = ->
    $location.path("/dealerships/#{@dealership.id}/details") if @dealership?
  $scope.searchUser = (searchVal) ->
    $rootScope.holdData 'memberSearch', searchVal
    $location.path("/users/members")
  $scope.showChangeDealership = -> 
    if @changeDealership.dealerships?
      for dealership, index in @changeDealership.dealerships
        if dealership.id == @dealership.id
          @changeDealership.selected = index
          break
    else
      @changeDealership.dealerships = Dealership.query (data) ->
        for dealership, index in data
          if dealership.id == $scope.dealership.id
            $scope.changeDealership.selected = index
            break
    @changeDealership.show = true
    dealerPosition = new google.maps.LatLng(@dealership.latitude, @dealership.longitude)
    @changeDealership.mapOptions = {marker: new google.maps.Marker({map: @changeDealership.gmap, position: dealerPosition}), zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP, center: dealerPosition}
  $scope.selectDealership = (dealership) ->
    dealerPosition = new google.maps.LatLng(dealership.latitude, dealership.longitude)
    $scope.changeDealership.mapOptions.marker.setPosition(dealerPosition)
    $scope.changeDealership.gmap.setCenter(dealerPosition)
  $scope.changeDealership.onDealershipMapIdle = ->
    google.maps.event.clearListeners(@gmap, 'idle')
    google.maps.event.trigger(@gmap, 'resize')
    dealerPosition = new google.maps.LatLng($scope.dealership.latitude, $scope.dealership.longitude)
    @mapOptions.marker = new google.maps.Marker({map: @gmap, position: dealerPosition})
    @gmap.setCenter(dealerPosition)
  $scope.closeChangeDealership = (setCurrentDealership) -> 
    @changeDealership.show = false
    $scope.changeDealership.selected = parseInt($scope.changeDealership.selected)
    newDealership = $scope.changeDealership.dealerships[$scope.changeDealership.selected]
    if newDealership? && setCurrentDealership
      setDealership(newDealership)
      $rootScope.setCurrentDealership(newDealership)

  unless google? && google.maps? || $('script[src^="//maps.googleapis.com/maps"]').length > 0
    $('<script></script>', {
      'type': "text/javascript", 
      'src': "//maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyAmwYzIDpoiCbQ0fch7TFsnSOWamhdxwI8&sensor=false&callback=gMapsLoaded"
    }).appendTo($('body'))
    $window.gMapsLoaded = =>
      $scope.$digest()

@MemberDashboardCtrl = ($scope, $location, $rootScope, User, Dealership) ->
  $scope.transactionHistoryVisible = false
  Dealership.query (data) ->
    myDealership = (dealer for dealer in data when dealer.id is $scope.currentUser.member_dealership.id)[0]
    $scope.dealershipNames = $.map data, (dealership) -> {value: dealership.id, label: "#{dealership.city} - #{dealership.name}"}
    $scope.search ?= {}
    $scope.search.location = "#{myDealership.city} - #{myDealership.name}"
    $scope.search.locationId = $scope.currentUser.dealership_id
  $scope.hasRemainingCredit = -> $scope.currentUser.remainingCredits() > 0
  $scope.transactionCredit = (transaction) -> "#{Math.abs(transaction.credits)} rental days #{if transaction.credits > 0 then "awarded" else "redeemed"}"
  $scope.$on 'autoCompleteSelected', (evt, params) ->
    if params.item?
      $scope.search.location = params.item.label
      $scope.search.locationId = params.item.value
    else
      delete $scope.search.locationId
  
  $scope.awardDate = (credit) ->
    Date.parseWithoutTimezone(credit.purchased_date).toLocaleDateString()
  $scope.awardExpiration = (credit) ->
    expDate = Date.parseWithoutTimezone(credit.purchased_date)
    expDate.setFullYear(expDate.getFullYear() + 1)
    "#{if @currentUser.credits.length == 1 then 'your' else credit.credits_remaining} rewards expire #{expDate.toLocaleDateString()}"

  $scope.doSearch = ->
    $rootScope.holdData 'searchOptions', @search
    @loadSearchPath(@search.location.split('-')[0].trim(), @search.locationId)
    
  $scope.viewReservationDetails = ->
    $location.path("/reservations/#{@reservation.id}") if @reservation?
  $scope.modifyReservation = ->
    $location.path("/reservations/#{@reservation.id}/edit") if @reservation?
  $scope.cancelReservation = ->
    $location.path("/reservations/#{@reservation.id}/cancel") if @reservation?
    

@AdminDashboardCtrl.$inject = ['$scope', '$rootScope', '$window', '$location', '$timeout', 'User', 'Reservation', 'Dealership']
@MemberDashboardCtrl.$inject = ['$scope', '$location', '$rootScope', 'User', 'Dealership']
