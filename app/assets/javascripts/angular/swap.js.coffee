window.models = {
  user: {actions: {current: {method: 'GET', params: {id: 'current'}}}},
  dealership: {}, 
  reservation: {actions: {new: {method: 'GET', params: {id: 'new'}}}},
  surveyQuestion: {}
  dealershipHoliday: {url: '/dealerships/:dealership_id/dealership_holidays/:id', params: {dealership_id: '@dealership_id'}},
  fleetGroup: {url: '/dealerships/:dealership_id/fleet_groups/:id', params: {dealership_id: '@dealership_id'}},
  fleetGroupQuantity: {url: '/dealerships/:dealership_id/fleet_groups/:fleet_group_id/fleet_group_quantities/:id', params: {dealership_id: '@dealership_id', fleet_group_id: '@fleet_group_id', id: '@id'}},
  activityLog: {}
}

$.cookie.json = true

dependencies = ['ui.directives', 'ui.bootstrap.tpls', 'ui.bootstrap.modal', 'ui.bootstrap.pagination'].concat $.map(window.models, (actions, model) -> "#{model}sService")
@swap = angular.module("swap", dependencies)
        .config(['$locationProvider', '$httpProvider', ($locationProvider, $httpProvider) ->
          $locationProvider.html5Mode(true)
          $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
          $httpProvider.responseInterceptors.push ['$q', 'Flash', ($q, Flash) ->
            (promise) ->
              promise.then (response) ->
                flashMessages = {}
                flashHeaderKey = /^x-message-(.*)/i
                $.each response.headers(), (key, value) ->
                  if flashMatch = key.match(flashHeaderKey)
                    flashMessages[flashMatch[1]] = value
                Flash.setImmediate(flashMessages)
                response
              , (response) ->
                if response.status != 409
                  for error of response.data.errors
                    if (error != 'conflicts')
                      Flash.setImmediate error.charAt(0).toUpperCase() + error.slice(1).replace(/_/g," ") + ' ' + response.data.errors[error], true
                  if !response.data.errors? && response.status == 403
                    Flash.setImmediate "You do not have permission to perform this operation", true
                $q.reject(response)
          ]
        ]).run(['$location', '$rootScope', 'User', 'Dealership', ($location, $rootScope, User, Dealership) ->
          $rootScope.loadSearchPath = (location, dealership) ->
            if angular.isObject(dealership)
              dealership_id = dealership.id
              location = [dealership.city, dealership.state].join(',')
            else if (asInt = parseInt(dealership)) && angular.isNumber(asInt) && !isNaN(asInt)
              dealership_id = asInt
            url = '/search'
            if location?
              url = [url, location].join('/')
              if dealership_id?
                url = [url, dealership_id].join('/')
            $location.path(url)
          $rootScope.holdData = (dataName, dataObject) ->
            @[dataName] = () ->
              delete @[dataName]
              dataObject
          $rootScope.saveSearchOptions = (searchOptions) ->
            $.cookie 'search-options', searchOptions, { expires: 365 }
          $rootScope.searchOptions = ->
            so = {}
            if $.cookie('search-options')?
              so = $.cookie('search-options')
            so
          $rootScope.currentUser = null
          $rootScope.refreshCurrentUser = (loadFromLocalStorage = false) ->
            console.log "Hereeee"
            cUser = null
            if $.cookie('current-user')?
              currentUserId = parseInt $.cookie('current-user').id
              if Modernizr && Modernizr.localstorage
                storageKey = "user_#{currentUserId}"
                if localStorage[storageKey]? && loadFromLocalStorage
                  cUser = new User JSON.parse(localStorage[storageKey])
                if cUser == null || cUser.updated_at != $.cookie('current-user').updated_at
                  cUser = User.current (data) ->
                    localStorage[storageKey] = JSON.stringify data
              else
                cUser = User.current()
            
            console.log '---Current User---'
            console.log $.cookie('current-user')    
            $rootScope.currentUser = cUser
          $rootScope.$on '$routeChangeSuccess', ->
            if !$rootScope.currentUser?
              $rootScope.refreshCurrentUser(true)
            else if !$rootScope.currentUser.updated_at? || ($.cookie('current-user')? && $.cookie('current-user').updated_at? && $rootScope.currentUser.updated_at < $.cookie('current-user').updated_at)
              $rootScope.refreshCurrentUser()
          $rootScope.getCurrentDealership = (callback) ->
            if @currentUser?
              storageKey = "currentDealership_#{@currentUser.id}"
              useLocalStorage = Modernizr && Modernizr.localstorage
              dealershipId = parseInt((useLocalStorage && localStorage[storageKey]) || (!useLocalStorage && $.cookie(storageKey)))
              if angular.isNumber(dealershipId) && !isNaN(dealershipId)
                dealership = Dealership.get {id: dealershipId}, callback
              else
                dealership = Dealership.query {single: true}, (data) -> 
                  if useLocalStorage
                    localStorage[storageKey] = data[0].id
                  else
                    $.cookie(storageKey, data[0].id)
                  callback(data[0]) if callback?
            dealership
          $rootScope.setCurrentDealership = (dealership) ->
            storageKey = "currentDealership_#{@currentUser.id}"
            if Modernizr && Modernizr.localstorage
              localStorage[storageKey] = dealership.id
            else
              $.cookie(storageKey, dealership.id)
          $rootScope.isCurrentMember = ->
            @currentUser? && @currentUser.isMember()
          $rootScope.isCurrentDealerRep = ->
            @currentUser? && @currentUser.isDealerRep()
          $rootScope.isCurrentMemberSupport = ->
            @currentUser? && @currentUser.isMemberSupport()
        ]).run(['Dealership', 'FleetGroup', 'Reservation', 'User', (Dealership, FleetGroup, Reservation, User) ->
          Dealership.prototype.describeHours = (day) ->
            hours = {}
            day = $.map [day], (n) -> n
            $.each day, (idx, dayItem) =>
              dayName = Date.DAYS_OF_WEEK[dayItem]
              if this.is_open[dayItem]
                date = new Date()
                timeParts = this.open_time[dayItem].split(':')
                date.setHours(timeParts[0])
                date.setMinutes(timeParts[1])
                open = date.toShortTimeString()
                timeParts = this.close_time[dayItem].split(':')
                date.setHours(timeParts[0])
                date.setMinutes(timeParts[1])
                close = date.toShortTimeString()
                desc = "#{open}-#{close}"
              else
                desc = "the dealership is closed"
              dayAdded = false
              for dayKey, dayDesc of hours
                if dayDesc == desc
                  hours["#{dayKey},#{dayName}"] = dayDesc
                  delete hours[dayKey]
                  dayAdded = true
              hours[dayName] = desc unless dayAdded
            hours
          Dealership.prototype.rentalHours = ->
            return @_hoursGrouped if @_hoursGrouped?
            @_hoursGrouped = []
            timeToLabel = (time) -> 
              timeParts = time.match(/(\d+):(\d+)(:\d+)?/)
              hour = parseInt timeParts[1]
              minute = parseInt timeParts[2]
              meridian = 'AM'
              if hour == 0
                hour = '12'
              else if hour >= 12
                meridian = 'PM'
                hour -= 12 if hour > 12
              "#{hour}#{if minute > 0 then ":#{minute}" else ''}#{meridian}"
            for dayIndex in [0..6]
              dayLabel = if @is_open[dayIndex] then "#{timeToLabel(@open_time[dayIndex])} - #{timeToLabel(@close_time[dayIndex])}" else 'Closed'
              matched = false
              for hourGroup in @_hoursGrouped
                if hourGroup.label == dayLabel
                  hourGroup.days.push dayIndex
                  matched = true
              @_hoursGrouped.push({days: [dayIndex], label: dayLabel}) unless matched
            for group in @_hoursGrouped
              groupDays = []
              idx = 0
              while idx < group.days.length
                dayLabel = Date.DAYS_OF_WEEK[group.days[idx]]
                firstIndex = idx
                while group.days[idx] == group.days[idx + 1] - 1
                  idx += 1
                if idx > firstIndex
                  dayLabel = dayLabel.concat(' - ').concat(Date.DAYS_OF_WEEK[group.days[idx]])
                groupDays.push dayLabel
                ++idx
              group.days = groupDays.join(',')
            @_hoursGrouped
          Dealership.prototype.searchUrl = ->
            "/search/#{@locationName()}/#{@id}"
          Dealership.prototype.locationName = ->
            [@city, @state].join(',')
          Dealership.prototype.toString = ->
            "#{@name} - #{@street}, #{@city}"

          FleetGroup.prototype.quantity = (day) ->
            day = new Date() unless day?
            unless angular.isDate(day)
              day = Date.parseWithoutTimezone(day)
            if @fleet_size[day.toShortDateString()]?
              @fleet_size[day.toShortDateString()]
            else if (nextUpdate = @quantities.filter((q) -> q.start_date > day.toShortDateString()).reverse()[0])
              nextUpdate.quantity
            else
              @fleet_size[$.map(@fleet_size, (_,v) -> v)[6]]
              
          Reservation.prototype.status = ->
            today = (new Date()).toShortDateString()
            if @isCancelled()
              "Cancelled"
            else if @pickup? && !@dropoff && @end_date < today
              "Late"
            else if @pickup? && !@dropoff?
              "Active"
            else if @pickup? && @dropoff?
              "Complete"
            else
              "Scheduled"
          Reservation.prototype.isCancelled = -> @cancelled_at?
          Reservation.prototype.isActive = ->
            ['Active', 'Late'].indexOf(@status()) >= 0
          
          User.prototype.availableCredits = ->
            unless @_availableCredits? || !@credits?
              @_availableCredits = 0
              @_availableCredits += credit.credits for credit in @credits
            @_availableCredits
          User.prototype.remainingCredits = ->
            unless @_remainingCredits? || !@credits?
              @_remainingCredits = 0
              @_remainingCredits += credit.credits_remaining for credit in @credits
            @_remainingCredits
          User.prototype.usedCredits = ->
            @availableCredits() - @remainingCredits()
          User.prototype.isMember = ->
            @roles? && ((@roles.indexOf? && @roles.indexOf('member') >= 0) || (angular.isObject(@roles) && @roles['member']?))
          User.prototype.isDealerRep = ->
            @roles? && ((@roles.indexOf? && @roles.indexOf('dealer_rep') >= 0) || (angular.isObject(@roles) && @roles['dealer_rep']?))
          User.prototype.isMemberSupport = ->
            @roles? && ((@roles.indexOf? && @roles.indexOf('member_support') >= 0) || (angular.isObject(@roles) && @roles['member_support']?))
        ])
