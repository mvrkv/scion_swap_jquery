@swap.config(['$routeProvider', ($routeProvider) ->
   $routeProvider
    .when('/dealerships/:id/schedule', {templateUrl: '/assets/dealerships/working_hours.html', controller: DealershipScheduleCtrl})
    .when('/dealerships/:id/inventory', {templateUrl: '/assets/dealerships/inventory_schedule.html', controller: DealershipInventoryCtrl})
    .when('/dealerships/:id/details', {templateUrl: '/assets/dealerships/details.html', controller: DealershipDetailsCtrl})
])

@DealershipScheduleCtrl = ($scope, $routeParams, Dealership, DealershipHoliday) ->
  $scope.holiday = edit: null
  $scope.scheduleConflict =
    show: false
    title: "Schedule Conflict"
    modal:
      keyboard: false
      dialogClass: "schedule-conflicts modal"

    schedule: {}

  $scope.dealership = Dealership.get(
    id: $routeParams.id
  , (data) ->
    $scope.cleanDealership = angular.copy(data)
  )
  $scope.resetHours = ->
    $scope.hoursErrors = null
    $scope.scheduleForm.$invalid = false
    $scope.dealership = angular.copy($scope.cleanDealership)
    $scope.updateSuccess = false
    $scope.cancelSuccess = true

  $scope.getHoursErrors = ->
    err = undefined
    err = []
    err = err.concat(@hoursErrors)  if @hoursErrors?
    err.push "Please fill in all required fields (highlighted in red)"  if (@scheduleForm.$error.required?) and @scheduleForm.$error.required.length > 0
    err

  $scope.changes = ->
    changes = undefined
    day = undefined
    _i = undefined
    _len = undefined
    _ref = undefined
    changes = []
    _ref = [0, 1, 2, 3, 4, 5, 6, 7]
    _i = 0
    _len = _ref.length

    while _i < _len
      day = _ref[_i]
      changes.push day  if ($scope.dealership.is_open[day] isnt $scope.cleanDealership.is_open[day]) or ($scope.dealership.is_open[day] and ($scope.dealership.open_time[day] isnt $scope.cleanDealership.open_time[day] or $scope.dealership.close_time[day] isnt $scope.cleanDealership.close_time[day]))
      _i++
    was: $.map(@cleanDealership.describeHours(changes), (sched, days) ->
      "" + days + " " + sched
    )
    now: $.map(@dealership.describeHours(changes), (sched, days) ->
      "" + days + " " + sched
    )

  $scope.updateOperatingHours = ->
    dship = undefined
    $scope.updateSuccess = false
    $scope.cancelSuccess = false
    $scope.hoursErrors = null
    @dealership["swap-keep"] = ["open_time", "close_time"]
    dship = angular.copy(@dealership)
    if $.inArray(dship.open_time, null)
      $.each dship.open_time, (idx, open) ->
        dship["open_time[" + idx + "]"] = open  if open?

      delete dship.open_time
    if $.inArray(dship.close_time, null)
      $.each dship.close_time, (idx, close) ->
        dship["close_time[" + idx + "]"] = close  if close?

      delete dship.close_time
    Dealership.update dship, ((data) ->
      $scope.refreshCurrentUser()
      $scope.setCurrentDealership data
      $scope.cleanDealership = angular.copy($scope.dealership)
      $scope.updateSuccess = true
      $scope.cancelSuccess = false
      $scope.scheduleForm.$dirty = false
      $scope.scheduleForm.$pristine = true
      $scope.scheduleForm.$invalid = false
      $scope.scheduleConflict.show = false
      $scope.clearUpdateSuccess = $scope.$watch("scheduleForm.$dirty", (newVal) ->
        if newVal
          $scope.updateSuccess = null
          $scope.clearUpdateSuccess()
      )
    ), (response) ->
      if response.data.errors.conflicts?
        $scope.scheduleConflict.schedule.conflicts = response.data.errors.conflicts[0]
        delete response.data.errors.conflicts

        $scope.scheduleForm.$invalid = false
        $scope.scheduleConflict.schedule.changes = $scope.changes()
        $scope.scheduleConflict.confirm = ->
          $scope.dealership.override_conflicts = $scope.scheduleConflict.schedule.conflicts
          $scope.updateOperatingHours.apply $scope
          delete $scope.dealership.override_conflicts

        $scope.scheduleConflict.cancel = $scope.resetHours
        alert $.isEmptyObject(response.data.errors)
        $scope.scheduleConflict.show = $.isEmptyObject(response.data.errors)
      $scope.hoursErrors = $.map(response.data.errors, (errors, field) ->
        error = undefined
        fieldLabel = undefined
        _i = undefined
        _len = undefined
        _results = undefined
        fieldLabel = field.charAt(0).toUpperCase() + field.substr(1).replace("_", " ")
        _results = []
        _i = 0
        _len = errors.length

        while _i < _len
          error = errors[_i]
          _results.push "" + fieldLabel + " " + error
          _i++
        $scope.scheduleForm.$invalid = true
        _results
      )


  $scope.dateString = (date) ->
    Date.parseWithoutTimezone(date).toLongString()

  $scope.addHoliday = ->
    $scope.holidayForm.$invalid = false
    @holiday.edit =
      dealership_id: @dealership.id
      date: null

  $scope.editHoliday = (holiday) ->
    @holiday.edit = angular.copy(holiday)

  $scope.resetHoliday = ->
    @holiday = edit: null

  $scope.getHolidayErrors = (error) ->
    err = undefined
    errorType = undefined
    field = undefined
    fieldName = undefined
    fields = undefined
    _i = undefined
    _len = undefined
    $scope.holidayForm.$invalid = true
    err = []
    for errorType of error
      fields = error[errorType]
      _i = 0
      _len = fields.length

      while _i < _len
        field = fields[_i]
        fieldName = field.$name.replace("-", " ")
        fieldName = fieldName.charAt(0).toUpperCase() + fieldName.substr(1).toLowerCase()
        err.push "" + fieldName + " is " + (errorType.replace(/([A-Z])/g, " $1").toLowerCase())
        _i++
    err

  $scope.removeHoliday = (holiday) ->
    DealershipHoliday["delete"]
      id: holiday.id
      dealership_id: holiday.dealership_id
    , (->
      $scope.dealership.holidays.splice $scope.dealership.holidays.indexOf(holiday), 1
    ), (response) ->
      alert "Error removing holiday. Try again later"


  $scope.saveHoliday = (holiday) ->
    failure = undefined
    success = undefined
    success = (data) ->
      existing = undefined
      idx = undefined
      existing = $scope.dealership.holidays.filter((h) ->
        h.id is data.id
      )
      if (existing?) and existing.length > 0
        idx = $scope.dealership.holidays.indexOf(existing[0])
        $scope.dealership.holidays[idx] = data
      else
        $scope.dealership.holidays.push data
      $scope.holiday.edit = null
      $scope.scheduleConflict.show = false

    failure = (response) ->
      if response.data.errors.conflicts?
        $scope.scheduleConflict.schedule.conflicts = response.data.errors.conflicts[0]
        delete response.data.errors.conflicts

        $scope.scheduleConflict.title = "Holiday Schedule Conflict"
        $scope.scheduleConflict.confirm = ->
          $scope.holiday.edit.override_conflicts = $scope.scheduleConflict.schedule.conflicts
          $scope.saveHoliday $scope.holiday.edit
          delete $scope.holiday.edit.override_conflicts

        $scope.scheduleConflict.cancel = ->
          $scope.holiday.edit = null

        $scope.scheduleConflict.show = $.isEmptyObject(response.data.errors)
      alert "Failed to save holiday. Please try again"  unless $.isEmptyObject(response.data.errors)

    if holiday.id?
      DealershipHoliday.update holiday, success, failure
    else
      DealershipHoliday.save holiday, success, failure


@DealershipInventoryCtrl = ($scope, $routeParams, Dealership, FleetGroup, FleetGroupQuantity) ->
  $scope.inventoryDateOptions = {'ui-date-format': 'yy-mm-dd'}
  $scope.inventoryConflict = {show: false, title: 'Inventory Warning', modal: {keyboard: false, dialogClass: 'schedule-conflicts modal'}, inventory: {}}
  $scope.dealership = Dealership.get {id: $routeParams.id}, (data) ->
    data.fleet_groups = $.map data.fleet_groups, (fg) -> new FleetGroup(fg)
    window.dealership = data.fleet_groups
  $scope.quantityDescription = (fleet_group, quantity,idx) ->
    quantDate = Date.parseWithoutTimezone(quantity.start_date)
    desc = "#{quantDate.toDayDateString()} - "
    quantIdx = fleet_group.quantities.indexOf(quantity)
    yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    if quantIdx == 0 && quantDate >= yesterday
      oldQuantity = fleet_group.quantity(yesterday)
    else if quantIdx > 0
     oldQuantity = fleet_group.quantities[quantIdx - 1].quantity 
    if oldQuantity?
      desc += "#{if oldQuantity < quantity.quantity then 'add' else 'remove'} #{Math.abs(quantity.quantity - oldQuantity)} cars for "
    desc += "a total of #{quantity.quantity}"
  $scope.updateInventory = (group) ->
    group.edit = {quantity: group.quantity(), date_immediate: 'select', start_date: ''}
  $scope.scheduleUpdate = (group) ->
    if group.edit?
      group.edit.errors = null
      quantityParam = {dealership_id: group.dealership_id, fleet_group_id: group.id, quantity: group.edit.quantity, start_date: (if group.edit.date_immediate == 'immediately' then new Date().toShortDateString() else group.edit.start_date), override_inventory: group.override_inventory}
      FleetGroupQuantity.save quantityParam, (data, response) ->
        index = -1
        for quant, idx in group.quantities
          index = idx if quant.id == data.id
        if index >= 0
          group.quantities[index] = data
        else
          group.quantities.push data
        group.quantities = group.quantities.sort (a,b) -> new Date(a.start_date) - new Date(b.start_date)
        $scope.inventoryConflict.show = false
        $scope.refreshCurrentUser()
        $scope.cancelUpdate(group)
      , (response) ->
        window.response = response
        if response.data.errors.inventory_shortage?
          $scope.inventoryConflict.inventory.conflicts = response.data.errors.inventory_shortage[0]
          delete response.data.errors.inventory_shortage
          $scope.inventoryConflict.inventoryGroup = group
          $scope.inventoryConflict.confirm = ->
            group.override_inventory = $scope.inventoryConflict.inventory.conflicts
            $scope.scheduleUpdate.apply($scope, [group])
            delete group.override_inventory
          $scope.inventoryConflict.show = $.isEmptyObject(response.data.errors)
        if response.data.errors? && !$.isEmptyObject(response.data.errors)
          group.edit.errors = []
          group.edit.errors.push "#{field.charAt(0).toUpperCase()}#{field.substr(1).replace(/_/g, ' ')} #{error}" for error in errors for field, errors of response.data.errors
  $scope.removeQuantity = (group, update) ->
    FleetGroupQuantity.delete {dealership_id: group.dealership_id, fleet_group_id: group.id, id: update.id}, () -> 
      group.quantities.splice(group.quantities.indexOf(update), 1)
      $scope.refreshCurrentUser()
    , () -> alert('Failed to delete inventory update')
  $scope.cancelUpdate = (group) ->
    delete group.edit

@DealershipDetailsCtrl = ($scope, $routeParams, $location, Dealership) ->
  $scope.dealership = Dealership.get {id: $routeParams.id}
  $scope.cancel = -> $location.path('/users/admin')
  $scope.updateDealership = ->
    Dealership.update $scope.dealership, (data) -> 
      $scope.refreshCurrentUser()
      $scope.setCurrentDealership data
      $location.path('/users/admin')

@DealershipScheduleCtrl.$inject = ['$scope', '$routeParams', 'Dealership', 'DealershipHoliday']
@DealershipInventoryCtrl.$inject = ['$scope', '$routeParams', 'Dealership', 'FleetGroup', 'FleetGroupQuantity']
@DealershipDetailsCtrl.$inject = ['$scope', '$routeParams', '$location', 'Dealership']

