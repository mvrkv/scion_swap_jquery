@swap.service('Flash', ['$rootScope', ($rootScope) ->
  @queue = []
  @currentMessages = null
  
  $rootScope.$on '$routeChangeSuccess', () =>
    @currentMessages = $.map @queue.shift() || {}, (key, value) ->
      {type: value, message: key}
  
  hashFromMessage = (messages) ->
    if not messages? || $.isEmptyObject(messages)
      null
    else if angular.isString(messages)
      {message: messages}
    else if messages? && angular.isObject(messages) && Object.keys(messages).length > 0
      messages
  
  {
    get: =>
      @currentMessages
    set: (messages) =>
      flash = hashFromMessage(messages)
      if flash?
        @queue.push flash
    clear: =>
      @currentMessages = null
      @queue = []
    setImmediate: (messages, replace) =>
      @currentMessages ?= []
      flash = hashFromMessage(messages)
      if flash?
        if replace? && replace
          @currentMessages = [flash]
        else
          @currentMessages = @currentMessages.concat(flash)
  }
])