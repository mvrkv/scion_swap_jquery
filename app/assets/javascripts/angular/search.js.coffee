@swap.config(['$routeProvider', ($routeProvider) ->
   $routeProvider
    .when('/search/browse', {templateUrl: '/assets/search/browse.html', controller: BrowseCtrl})
    .when('/search/:location_name/:dealershipId', {templateUrl: '/assets/search/index.html', controller: SearchCtrl})
    .when('/search/:location_name', {templateUrl: '/assets/search/index.html', controller: SearchCtrl})
    .when('/search', {templateUrl: '/assets/search/index.html', controller: SearchCtrl})
])

@SearchCtrl = ($scope, $rootScope, $window, $http, $routeParams, $location, Flash, Dealership, Reservation) ->
  
  $scope.mapReady = false  
  $scope.gMapsWaiting = false
  $scope.mapOptions = {zoom: 15}
  $scope.markers = []
  $scope.nearby = {show: false, marker: null}
  $scope.dealer = {}
  $scope.searchComplete = false
  if $rootScope.searchOptions?
    $scope.search = angular.copy($rootScope.searchOptions())
  $scope.removeMapWatch = $scope.$watch 'googleMapsStatus()', (googleValue) ->
    if googleValue
      $scope.setMapOptions()
      $scope.removeMapWatch()
  
  $scope.setMapOptions = ->
    if @googleMapsStatus() && $scope.dealership?
      @mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP
      @dealershipLocation = new google.maps.LatLng($scope.dealership.latitude, $scope.dealership.longitude)
      if @dealer.gmap?
        @dealer.gmap.setCenter(@dealershipLocation)
        anyVisibleMarkers = false
        for marker in @markers
          visible = marker.position.equals(@dealershipLocation)
          anyVisibleMarkers = anyVisibleMarkers || visible
          marker.setVisible(visible)
        unless anyVisibleMarkers
          @markers.push(new google.maps.Marker({map: @dealer.gmap, position: @dealershipLocation}))
      else
        @mapOptions.center = @dealershipLocation
      @mapReady = true

  $scope.onMapIdle = ->
    @markers.push(new google.maps.Marker({map: @dealer.gmap, position: @dealershipLocation})) if @dealer.gmap
    google.maps.event.clearListeners @dealer.gmap, 'idle'

  $scope.googleMapsStatus = ->
    !$scope.gMapsWaiting && $window.google? && $window.google.maps?

  $scope.browse = ->
    $scope.nearby.show = true
    $scope.nearby.mapOptions = angular.copy($scope.mapOptions)
    $scope.nearby.mapOptions.center = new google.maps.LatLng($scope.dealership.latitude, $scope.dealership.longitude)
    $scope.nearby.selected = 0
  $scope.onNearbyMapIdle = ->
    google.maps.event.clearListeners($scope.nearby.gmap, 'idle')
    center = $scope.nearby.gmap.getCenter()
    google.maps.event.trigger($scope.nearby.gmap, 'resize')
    if $scope.nearby.selected?
      selectedNearby = $scope.dealership.nearbys[$scope.nearby.selected]
      center = new google.maps.LatLng(selectedNearby.latitude, selectedNearby.longitude)
    $scope.nearby.gmap.setCenter(center)
    $scope.nearby.marker = new google.maps.Marker({map: $scope.nearby.gmap, position: center})
  $scope.selectNearby = (nearby) ->
    nearbyPosition = new google.maps.LatLng(nearby.latitude, nearby.longitude)
    $scope.nearby.marker.setPosition(nearbyPosition)
    $scope.nearby.gmap.setCenter(nearbyPosition)
  $scope.closeNearbys = (loadNearby) ->
    $scope.nearby.show = false
    $scope.nearby.selected = parseInt($scope.nearby.selected)
    newDealership = $scope.dealership.nearbys[$scope.nearby.selected]
    if newDealership? && loadNearby
      $rootScope.saveSearchOptions $scope.search
      $scope.loadSearchPath(null, newDealership)
  $scope.nearby.options = {backdropFade: true, dialogClass: 'nearbys-list modal'}
  
  $scope.$on 'SearchDone', (event, args) =>
    $rootScope.saveSearchOptions $scope.search
    if !args.success && args.error?
      $scope.loadSearchPath('browse')
    else if args.success && args.data
      $scope.dealership = args.data
      if !$scope.mapsReady
        $scope.setMapOptions()
    $scope.searchComplete = true
    window.rootScope = $rootScope
    window.search = $scope.search

  $scope.reserve = (fleet_group, event) ->
    $scope.submittingReservation = true
    $scope.reservationErrors = {}
    resParms = {reservation: 
      {
        fleet_group_id: fleet_group.id, 
        start_date: $scope.search.fromDate,
        end_date: $scope.search.toDate
        user_id: $scope.search.user_id
      }
    }
    Reservation.new $window.swap.params(resParms), (data) ->
      $rootScope.holdData('targetReservation', data)
      $location.path('/reservations/new')
    , ((response) ->
      $scope.submittingReservation = false
      if response.status == 403
        errors = ['You do not have permission to create a reservation at this dealership']
      else
        errors = $.map response.data, (errors, field) ->
          @swap.toTitleCase(field.replace(/_(\w)/, (m, grp) -> ' ' + grp.toUpperCase())) + " " + (error for error in errors).join(',')
      $scope.reservationErrors[this.fleet_group.id] = errors
    ).bind({fleet_group: fleet_group, event: event})
  
  if !($scope.isCurrentMember() || ($scope.search? && $scope.search.user_id?))
    $location.path('/users/admin')
  else if $window.search_result
    dealership = new Dealership($window.search_result)
    delete $window.search_result
    if $location.path() == dealership.searchUrl()
      $scope.dealership = dealership
    else
      $rootScope.saveSearchOptions $scope.search
      $scope.loadSearchPath(dealership.locationName(), dealership)
      return
  else
    if ((dealershipId = parseInt($routeParams.dealershipId)) && $routeParams.location_name &&  angular.isNumber(dealershipId) && !isNaN(dealershipId)) || ($scope.currentUser && $scope.currentUser.member_dealership && (dealershipId = $scope.currentUser.member_dealership.id))
      if $routeParams.location_name == undefined
        console.log "enters in to if "
        console.log $scope.currentUser.member_dealership.location_name
        $routeParams.location_name = $scope.currentUser.member_dealership.location_name  
      url = "/search"
      url = [url, $.param({location: $routeParams.location_name}).replace(/[-]/, '+')].join('?')
      $http.get(url).success (data, status) ->
        $scope.dealership = new Dealership(data)
        
    else if angular.isString($routeParams.location_name)
      $scope.dealership = window.swap.toTitleCase($routeParams.location_name.replace(/[-]/, ' '))
    else
      $scope.dealership = null

  unless google? && google.maps? || $('script[src^="//maps.googleapis.com/maps"]').length > 0
    $scope.gMapsWaiting = true
    $('<script></script>', {
      'type': "text/javascript", 
      'src': "//maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyAmwYzIDpoiCbQ0fch7TFsnSOWamhdxwI8&sensor=false&callback=gMapsLoaded"
    }).appendTo($('body'))
    $window.gMapsLoaded = => 
      $scope.gMapsWaiting = false
      $scope.$digest()

@BrowseCtrl = ($scope, $rootScope, $location, Flash, Dealership) ->
  if $rootScope.searchOptions? && (searchOptions=$rootScope.searchOptions())
    $scope.search = angular.extend({}, searchOptions)
  
  $scope.goToCity = (city) ->
    $rootScope.saveSearchOptions $scope.search
    $scope.loadSearchPath(city)
    $rootScope.searchOptions = angular.copy($rootScope.saveSearchOptions)
  
  $scope.$on 'SearchDone', (event, args) ->
    if args.success && args.data?
      $rootScope.saveSearchOptions $scope.search
      $scope.loadSearchPath(null, args.data)
      
  $scope.search = angular.copy($rootScope.saveSearchOptions())

@SearchCtrl.$inject = ['$scope', '$rootScope', '$window', '$http', '$routeParams', '$location', 'Flash', 'Dealership', 'Reservation']
@BrowseCtrl.$inject = ['$scope', '$rootScope', '$location', 'Flash','Dealership']
