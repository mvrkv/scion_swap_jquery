@swap.config(['$routeProvider', ($routeProvider) ->
  $routeProvider
    .when('/terms', {templateUrl: '/assets/static/terms.html', controller: StaticCtrl})
    .when('/privacy', {templateUrl: '/assets/static/privacy.html', controller: StaticCtrl})
    .when('/faq', {templateUrl: '/assets/static/faq.html', controller: StaticCtrl})
])

@StaticCtrl = ($scope, $location, $anchorScroll) ->
  # handle anchor links in terms/privacy
  setTimeout ->
    hashLink = $('a[name='+$location.hash()+']')
    if hashLink.length
      $(window).scrollTop hashLink.offset().top - $('#header .navbar').outerHeight() - 10
  , 100

@StaticCtrl.$inject = ['$scope', '$location', '$anchorScroll']