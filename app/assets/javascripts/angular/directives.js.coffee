@swap.directive 'searchBar', ['$http', '$rootScope', 'User', 'Flash', ($http, $rootScope, User, Flash) ->
  {
    replace: true,
    restrict: 'A',
    scope: {
      dealership: '=',
      search: '='
    },
    templateUrl: '/assets/directives/search_bar.html',
    link: (scope) ->
      scope.search ?= {}
      scope.fromDateOptions = {'ui-date-format': 'yy-mm-dd', 'defaultDate': 2, 'minDate': 2}
      scope.toDateOptions = {'ui-date-format': 'yy-mm-dd', 'defaultDate': 2, 'minDate': 2}
      scope.$watch 'dealership', (newVal, oldVal) ->
        if newVal? && !$.isEmptyObject(newVal)
          if angular.isObject(newVal)
            scope.search.location = "#{newVal.city}, #{newVal.state}"
            scope.$emit('SearchDone', {success: true, data: newVal})
          else if angular.isString(newVal)
            scope.search.location = newVal
            scope.doSearch()
        else if scope.search.location? && scope.search.location.length > 0 && oldVal?
          error = "No dealerships were found near #{scope.search.location}"
          scope.$emit('SearchDone', {success: false, error: error})
      scope.currentUser = $rootScope.currentUser
      scope.doSearch = ->
        
        url = "/search"
        url = "#{url}?#{$.param(scope.search).replace(/[_-]/, '+')}" if scope.search?
        
        $http.get(url).success (data, status) ->
          if angular.isArray(data)
            dealership = data[0]
          else if angular.isObject(data) && data.id
            dealership = data
          else
            dealership = null
          if dealership
            scope.$emit('SearchDone', {success: true, data: dealership})
          else
            scope.$emit('SearchDone', {error: 'ScionSwap is not yet available at that location', data: dealership})
      if not scope.dealership? && Modernizr.geolocation
        navigator.geolocation.getCurrentPosition (location) ->
          
          scope.search.location = location.address.city if not scope.search.location? && location? && location.address? && location.address.city?
  }
]

@swap.directive 'scheduleConflict', ['$http', 'User', 'Flash', ($http, User, Flash) ->
  {
    replace: true,
    restrict: 'A',
    scope: {
      conflictOptions: '='
    },
    templateUrl: '/assets/directives/inventory_conflict.html',
    link: (scope, elem, attrs) ->
      scope.currentInventory = ->
        scope.conflictOptions.inventoryGroup && scope.conflictOptions.inventoryGroup.edit && scope.conflictOptions.inventoryGroup.quantity(scope.conflictOptions.inventoryGroup.edit.start_date || new Date())
      scope.scheduledInventory = ->
        scope.conflictOptions.inventoryGroup && scope.conflictOptions.inventoryGroup.edit && scope.conflictOptions.inventoryGroup.edit.quantity
      scope.inventoryNeed = ->
        @scheduledInventory() + parseInt(scope.conflictOptions && scope.conflictOptions.inventory && scope.conflictOptions.inventory.conflicts || 0)
      scope.makeModel = ->
        scope.conflictOptions.inventoryGroup && "#{scope.conflictOptions.inventoryGroup.make} #{scope.conflictOptions.inventoryGroup.model}"
      scope.closeConflicts = ->
        @conflictOptions.show = false
        @conflictOptions.title = null
        @conflictOptions.confirm = null
        @conflictOptions.cancel = null
        @conflictOptions.schedule = {}
        @conflictOptions.inventory = {}
      scope.confirm = ->
        if @conflictOptions.confirm?
          @conflictOptions.confirm()
      scope.cancelConflicts = ->
        if @cancel?
          @cancel()
        @closeConflicts()
      scope.$watch 'conflictOptions.show', (newVal, oldVal) ->
        if !newVal && oldVal
          scope.closeConflicts()
  }
]

@swap.directive 'autoComplete', ['$timeout', ($timeout) ->
  link: (scope, elem, attrs) ->
    scope.autocomplete = $(elem).autocomplete { 
      source: scope[attrs.uiItems] || [],
      minLength: 0,
      change: (evt, ui) ->
        scope.$emit('autoCompleteSelected', source: elem, item: if ui? && ui.item? then ui.item else null)
      select: (evt, ui) -> 
        $timeout( -> 
          scope.$emit('autoCompleteSelected', source: elem, item: ui.item)
        , 0)
    }
    scope.$watch attrs.uiItems, (newVal) -> $(elem).autocomplete 'option', 'source', newVal
]

@swap.directive 'adminRentalLocationAndDates', ['$rootScope', '$location', 'Reservation', ($rootScope, $location, Reservation) ->
  restrict: 'A',
  replace: true,
  scope: {
    member: '='
    modalOptions: '=',
    dealership: '='
  },
  templateUrl: '/assets/directives/admin_rental_location_and_dates.html',
  link: (scope, elem, attrs) ->
    scope.modalOptions ?= {}
    unless scope.modalOptions.dialogClass?
      scope.modalOptions.dialogClass = 'admin-rental modal'
    scope.modalOptions.show = false
    scope.dateInputOptions = {'defaultDate': 1, 'minDate': 0}
    scope.makeReservation = ->
      if scope.adminRental.$valid
        $rootScope.holdData('searchOptions', {user_id: @member.id, fromDate: @start_date, toDate: @end_date})
        $location.path("/search/#{scope.dealership.city}/#{scope.dealership.id}")
        scope.modalOptions.show = false
    scope.cancel = -> 
      @modalOptions.show = false
]