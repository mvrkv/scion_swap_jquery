@swap.controller 'FlashCtrl', ['$scope', 'Flash', ($scope, flash) ->
  $scope.flashMessages = ->  
    flash.get()
  $scope.clearMessages = ->
    flash.clear()
]