@swap.filter('capitalize', () ->
  (str) -> if str.capitalize? then str.capitalize() else ''
)
