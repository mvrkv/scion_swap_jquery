@swap.config(['$routeProvider', ($routeProvider) ->
   $routeProvider
    .when('/reservations/new', {templateUrl: '/assets/reservations/new.html', controller: ReservationNewCtrl})
    .when('/reservations/:reservation_id', {templateUrl: '/assets/reservations/show.html', controller: ReservationShowCtrl})
    .when('/dealerships/:dealership_id/reservations', {templateUrl: '/assets/reservations/admin_index.html', controller: ReservationAdminIndexCtrl})
    .when('/reservations/:reservation_id/cancel', {templateUrl: '/assets/reservations/cancel.html', controller: ReservationCancelCtrl})
    .when('/reservations/:reservation_id/start', {templateUrl: '/assets/reservations/start.html', controller: ReservationStartCtrl})
    .when('/reservations/:reservation_id/edit', {templateUrl: '/assets/reservations/edit.html', controller: ReservationEditCtrl})
])

@ReservationNewCtrl = ($scope, $rootScope, $location, User, Flash, Reservation, Dealership) ->
  $scope.reservation = $rootScope.targetReservation()
  unless $scope.reservation?
    Flash.set "Failed to load reservation. Please start your search over"
    $location.path('/search')
  $scope.dealership = new Dealership($scope.reservation.dealership)
  $scope.reservation_make_model = "#{$scope.reservation.fleet_group.make} #{$scope.reservation.fleet_group.model}"
  $scope.reservationUser = new User($scope.reservation.user)
  dayInTime = 1000*60*60*24   # 1000 ms / sec  *  60 sec / min  *  60 min / hr  *  24 hr / day
  $scope.reservationCredits = ((new Date($scope.reservation.end_date).getTime() - new Date($scope.reservation.start_date).getTime()) / dayInTime) + 1
  if $scope.reservation.future_cancellations?
    for reservation in $scope.reservation.future_cancellations
      reservation.start_date = Date.parseWithoutTimezone reservation.start_date
      reservation.end_date = Date.parseWithoutTimezone reservation.end_date
  
  $scope.acknowledgments = {cancellations: false, payment: false}
  
  $scope.submitReservation = () ->
    @reservationErrors = []
    if @reservation.payment > 0.00 && !$scope.acknowledgments.payment
      @reservationErrors.push "Before completing your reservation, you must acknowledge per-day rate by checking the box above."
    if @reservation.future_cancellations && @reservation.future_cancellations.length > 0 && !$scope.acknowledgments.cancellations
      @reservationErrors.push "Before completing your reservation, you must acknowledge that #{@reservation.future_cancellations.length} future reservations will be cancelled"
    if @reservationErrors.length == 0
      Reservation.save @reservation, (data) ->
        Flash.set({notice: 'Your reservation has been booked'})
        if $scope.isCurrentMember()
          $location.path("/users/dashboard")
        else if $scope.isCurrentMemberSupport() || $scope.isCurrentDealerRep()
          $location.path('/users/admin')
        else
          $location.path('/')
  $scope.returnToSearch = () ->
    $rootScope.holdData 'searchOptions', {fromDate: $scope.reservation.start_date, toDate: $scope.reservation.end_date}
    @loadSearchPath(null, @dealership)

@ReservationAdminIndexCtrl = ($scope, $rootScope, $location, $routeParams, Reservation) ->
  $scope.reservationsPerPage = 10
  $scope.loadingComplete = false
  $scope.statusOptions = [{label: 'View All', value: 'all'}, {label: 'Pick-ups to process', value: 'pickups'}, {label: 'Drop-offs to process', value: 'dropoffs'}, {label: 'Active Reservations', value: 'active'}]
  if $rootScope.reservationStatus? && (targetStatus = $rootScope.reservationStatus())
    $scope.reservationStatus = status for status in $scope.statusOptions when status.value == targetStatus
  $scope.reservationStatus = $scope.statusOptions[0] unless $scope.reservationStatus?
  $scope.reservations = Reservation.query {dealership_id: $routeParams.dealership_id}, (data) ->
    $scope.loadingComplete = true
    $scope.reservationPages = ($scope.reservations.length / $scope.reservationsPerPage) + 1
    $scope.reservationPage = 1
  $scope.reservationEndDate = (reservation) ->
    reservation.dropoff || Date.parseWithoutTimezone(reservation.end_date)
  $scope.viewReservation = (reservation) ->
    $rootScope.holdData 'targetReservation', reservation
    $location.path("/reservations/#{reservation.id}")
  $scope.editReservation = (reservation) ->
    $rootScope.holdData 'targetReservation', reservation
    $location.path("/reservations/#{@reservation.id}/edit")
  $scope.endReservation = (reservation) ->
    $rootScope.holdData 'targetReservation', reservation
    $rootScope.holdData 'closeReservation', true
    $location.path "/reservations/#{@reservation.id}"
  $scope.paginationEnabled = ->
    true if @reservationPages > 1
     
  $scope.reservationPredicate = 'start_date'
  $scope.reservationSort = (sort_field) ->
    
    if @reservationPredicate == sort_field
      @reverse = !@reverse
    else
      @reverse = false
    @reservationPredicate = sort_field
  $scope.sortIcon = (sort_field) ->
    
    {"sort-target": @reservationPredicate == sort_field, "sort-reverse": @reverse}
  $scope.isOnPage = (index) -> Math.floor(index / @reservationsPerPage) == @reservationPage - 1
  $scope.selectedReservations = () ->
    today = (new Date()).toShortDateString()
    filtered = []
    if $scope.reservationStatus.value == 'all'
      filtered = $scope.reservations
    else if $scope.reservationStatus.value == 'pickups'
      filtered = $scope.reservations.filter (res) -> res.status() == 'Scheduled' && res.start_date == today
    else if $scope.reservationStatus.value == 'dropoffs'
      filtered = $scope.reservations.filter (res) -> res.isActive() && res.end_date == today
    else
      filtered = $scope.reservations.filter (res) -> res.isActive()
    if $scope.pickupFilter? && $scope.pickupFilter != ''
      filtered = filtered.filter (res) -> Date.parseWithoutTimezone(res.start_date).toShortDateString() == $scope.pickupFilter.toShortDateString()
    if $scope.dropoffFilter? && $scope.dropoffFilter != ''
      filtered = filtered.filter (res) -> Date.parseWithoutTimezone(res.end_date).toShortDateString() == $scope.dropoffFilter.toShortDateString()
    if $scope.memberFilter? && $scope.memberFilter != '' 
      filtered = filtered.filter (res) ->
        regex = new RegExp($scope.memberFilter, "i")
        "#{res.user.first_name} #{res.user.last_name}".match(regex) || res.user.email.match(regex) || res.number.match("^#{$scope.memberFilter}")
    $scope.reservationPages = Math.ceil(filtered.length / 10)
    $scope.reservationPage = 1 if $scope.reservationPage > $scope.reservationPages
    pageStart = ($scope.reservationPage - 1) * $scope.reservationsPerPage
    filtered
  $scope.sortingReservations = () ->
    
    if $scope.memberFilter? && $scope.memberFilter != '' 
      filtered = filtered.filter (res) ->
        regex = new RegExp($scope.memberFilter, "i")
        "#{res.user.first_name} #{res.user.last_name}".match(regex) || res.user.email.match(regex) || res.number.match("^#{$scope.memberFilter}")
    $scope.reservationPages = Math.ceil(filtered.length / 10)
    $scope.reservationPage = 1 if $scope.reservationPage > $scope.reservationPages
    pageStart = ($scope.reservationPage - 1) * $scope.reservationsPerPage
    filtered
  currentUserHasEditPermission = (reservation) ->
    $scope.isCurrentMemberSupport() || ($scope.isCurrentDealerRep() && $scope.currentUser.dealership.id == reservation.fleet_group.dealership.id)
  $scope.canEnd = (reservation) ->
    reservation? && reservation.pickup? && !reservation.dropoff? && currentUserHasEditPermission(reservation)
  $scope.canEdit = (reservation) ->
    reservation? && !reservation.dropoff? && !reservation.isCancelled() && currentUserHasEditPermission(reservation)

@ReservationShowCtrl = ($scope, $rootScope, $window, $routeParams, $location, Reservation, User) ->
  if $rootScope.targetReservation? && (target=$rootScope.targetReservation())
    $scope.reservation = target
  else if $window.reservation?
    $scope.reservation = new Reservation($window.reservation)
    delete $window.reservation
  else
    $scope.reservation = Reservation.get { id: $routeParams.reservation_id }
  $scope.actions = {}
  $scope.creditBalance = (credit) ->
    penaltyCredits = @reservation.penalty? && @reservation.penalty.lateReturn? && @reservation.penalty.lateReturn.credits || 0
    Math.max(0, credit.credits_remaining - @reservation.credits - penaltyCredits)
  $scope.creditExpiration = (credit) ->
    purchase = Date.parseWithoutTimezone(credit.purchased_date)
    purchase.setFullYear(purchase.getFullYear() + 1)
    purchase
  $scope.reservationDueBackWithPayment = -> 
    
    (@isCurrentMemberSupport() || @isCurrentDealerRep()) && 
    @reservation.isActive() && 
    @reservation.payment > 0 && 
    (Date.parseWithoutTimezone(@reservation.end_date) <= new Date() || @actions.closingReservation)
  $scope.paymentDueClass = {'highlight-payment': $scope.reservationDueBackWithPayment()}
  $scope.canStart = -> (@isCurrentDealerRep() || @isCurrentMemberSupport()) && !@reservation.isCancelled() && !@reservation.pickup? && @reservation.start_date <= (new Date()).toShortDateString()
  $scope.canChange = -> @reservation.status() == 'Scheduled'
  $scope.canExtend = -> @reservation.isActive()
  $scope.canCancel = -> @reservation.status() == 'Scheduled'
  $scope.canEnd = -> @reservation.isActive() && (@isCurrentDealerRep() || @isCurrentMemberSupport())
  $scope.startReservation = ->
    $rootScope.holdData 'targetReservation', @reservation
    $location.path("/reservations/#{@reservation.id}/start")
  $scope.changeReservation = ->
    $rootScope.holdData 'targetReservation', @reservation
    $location.path("/reservations/#{@reservation.id}/edit")
  $scope.cancelReservation = ->
    $rootScope.holdData 'targetReservation', @reservation
    $location.path("/reservations/#{@reservation.id}/cancel")
  $scope.closeReservation = ->
    reservationEdit = angular.copy @reservation
    reservationEdit.dropoff = new Date()
    $scope.reservation.user = new User($scope.reservation.user)
    Reservation.update {dryRun: true}, reservationEdit, (data) ->
      $scope.penalty = data.penalty
      $scope.actions.closingReservation = true
  $scope.mustCancelPendingBeforeClose = ->
    @reservation.penalty? && @reservation.penalty.lateReturn? && @reservation.penalty.lateReturn.must_cancel_pending
  $scope.cancelClose = -> @actions.closingReservation = false
  $scope.endReservation = -> 
    @reservation.dropoff = new Date()
    Reservation.update @reservation, -> $location.path("/dealerships/#{$scope.reservation.dealership.id}/reservations")
  if $rootScope.closeReservation? && $rootScope.closeReservation() && $scope.reservation.isActive()
    $scope.closeReservation()

@ReservationCancelCtrl = ($scope, $rootScope, $window, $routeParams, $location, Reservation) ->
  $scope.actions = {}
  tryCancel = ->
    reservationEdit = angular.copy $scope.reservation
    reservationEdit.cancelled_at = new Date()
    Reservation.update {dryRun: true}, reservationEdit, (data) ->
      $scope.penalty = data.penalty
    , (response) ->
      $scope.reservation.cancelled_at = null
  if $rootScope.targetReservation? && (target=$rootScope.targetReservation())
    $scope.reservation = target
    tryCancel()
  else if $window.reservation?
    $scope.reservation = new Reservation($window.reservation)
    delete $window.reservation
    tryCancel()
  else
    $scope.reservation = Reservation.get { id: $routeParams.reservation_id }, (data) ->
      $scope.reservationEdit = data
      tryCancel()

  $scope.creditBalance = (credit) ->
    Math.max(0, credit.credits_remaining - @reservation.credits)
  $scope.creditExpiration = (credit) ->
    purchase = Date.parseWithoutTimezone(credit.purchased_date)
    purchase.setFullYear(purchase.getFullYear() + 1)
    purchase
  $scope.doCancelReservation = ->
    @reservation.cancelled_at = new Date()
    Reservation.update @reservation
    $location.path("/reservations/#{@reservation.id}")
  $scope.cancelCancel = ->
    $rootScope.holdData 'targetReservation', @reservation
    $location.path("/reservations/#{@reservation.id}")

@ReservationStartCtrl = ($scope, $rootScope, $window, $routeParams, $location, Reservation, Dealership) ->
  $scope.errors = {reservation: null}                                                                
  getDealershipRentalFleets = (dealershipId) ->
    Dealership.get {id: dealershipId}, (data) ->    
      $scope.rentalFleets = data.fleet_groups
  $scope.rentalFleetOrdering = (fleet_group) -> if fleet_group.make == '--Other--' then 'zzz' else "#{fleet_group.make} #{fleet_group.model}"
  if $rootScope.targetReservation? && (target=$rootScope.targetReservation())
    $scope.reservation = target      
    getDealershipRentalFleets(target.dealership.id)
  else if $window.reservation?
    $scope.reservation = new Reservation($window.reservation)
    getDealershipRentalFleets($window.reservation.dealership.id)
    delete $window.reservation                                   
  else
    $scope.reservation = Reservation.get { id: $routeParams.reservation_id }, (data) -> getDealershipRentalFleets(data.dealership.id)
  $scope.startNow = ->
    @errors.reservation = null
    Reservation.update {id: @reservation.id, pickup: new Date(), rental_fleet_group_id: @reservation.rental_fleet_group_id, rental_agreement_id: @reservation.rental_agreement_id}, (data) ->
      $location.path("/reservations/#{data.id}")
  $scope.cancel = ->
    $location.path("/reservations/#{@reservation.id}")

@ReservationEditCtrl = ($scope, $rootScope, $window, $routeParams, $location, Reservation, Flash) ->
  if $rootScope.targetReservation? && (target=$rootScope.targetReservation())
    $scope.reservation = target
    $scope.reservationEdit = angular.copy $scope.reservation
  else if $window.reservation?
    $scope.reservation = new Reservation($window.reservation)
    $scope.reservationEdit = angular.copy $scope.reservation
    delete $window.reservation
  else
    $scope.reservation = Reservation.get { id: $routeParams.reservation_id }, (data) ->
      $scope.reservationEdit = data
  if $scope.reservation.isCancelled() 
    Flash.set "A cancelled or completed reservation cannot be edited"
    $location.path("/reservations/#{$scope.reservation.id}")
  $scope.changesConfirmed = $scope.changesSubmitted = false
  $scope.differentAvailableModels = null
  $scope.creditBalance = (credit) ->
    Math.max(0, credit.credits_remaining - @reservation.credits)
  $scope.creditExpiration = (credit) ->
    purchase = Date.parseWithoutTimezone(credit.purchased_date)
    purchase.setFullYear(purchase.getFullYear() + 1)
    purchase
  $scope.requirePayment = -> @changesConfirmed && @reservation.payment > 0
  $scope.paymentDueClass = {'highlight-payment': $scope.changesConfirmed && $scope.reservation.payment > 0}
  $scope.$watch 'changesConfirmed', (newVal) -> $scope.paymentDueClass = {'highlight-payment': $scope.changesConfirmed && $scope.reservation.payment > 0}
  $scope.selectFleetGroup = (fleet_group) ->
    $scope.reservationEdit.fleet_group = $scope.reservation.fleet_group = fleet_group
    $scope.differentAvailableModels = null
  $scope.checkAvailability = ->
    Flash.clear()
    $scope.changesSubmitted = true
    Reservation.update {dryRun: true}, @reservationEdit, (data) ->
      $scope.changesConfirmed = true
      $scope.reservation = data
      if data.payment > 0
        Flash.setImmediate({type: 'notice', message: 'Please note: This reservation requires a payment by the customer'})
      else
        Flash.setImmediate({type: 'notice', message: 'Nice! The selected model is available during the times selected.'})
    , (response) ->
      $scope.changesSubmitted = false
      if response.status == 409
        Flash.setImmediate response.data.errors[error].toString(), true for error of response.data.errors
      if !$scope.reservation.isActive()
        if response.data.available? && response.data.available.length > 0
          $scope.differentAvailableModels = response.data.available
        else
          Flash.setImmediate "There are no other models available for these dates"
  $scope.updateReservation = ->
    Reservation.update @reservation, (data) ->
      $rootScope.holdData('targetReservation', data)
      $location.path("/reservations/#{data.id}")
  $scope.cancelChanges = ->
    $location.path("/reservations/#{@reservationEdit.id}")
    false

@ReservationNewCtrl.$inject = ['$scope', '$rootScope', '$location', 'User', 'Flash', 'Reservation', 'Dealership']
@ReservationAdminIndexCtrl.$inject = ['$scope', '$rootScope', '$location', '$routeParams', 'Reservation']
@ReservationShowCtrl.$inject = ['$scope', '$rootScope', '$window', '$routeParams', '$location', 'Reservation', 'User']
@ReservationCancelCtrl.$inject = ['$scope', '$rootScope', '$window', '$routeParams', '$location', 'Reservation']
@ReservationStartCtrl.$inject = ['$scope', '$rootScope', '$window', '$routeParams', '$location', 'Reservation', 'Dealership']
@ReservationEditCtrl.$inject = ['$scope', '$rootScope', '$window', '$routeParams', '$location', 'Reservation', 'Flash']
