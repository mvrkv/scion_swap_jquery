@swap.config(['$routeProvider', ($routeProvider) ->
   $routeProvider
    .when('/users/activate', {templateUrl: '/assets/users/activation.html', controller: UserActivationCtrl})
    .when('/users/members', {templateUrl: '/assets/users/members.html', controller: UserIndexCtrl})
    .when('/users/new', {templateUrl: '/assets/users/new.html', controller: UserNewCtrl})
    .when('/users/:user_id', {templateUrl: '/assets/users/show.html', controller: UserOverviewCtrl})
    .when('/users/:user_id/edit', {templateUrl: '/assets/users/edit.html', controller: UserEditCtrl})
])

@UserActivationCtrl = ($scope, $window, $location, User, SurveyQuestion) ->
  $scope.triedSubmit = false
  addSurveyAnswersToUser = () ->
    if $scope.user? && $scope.surveyQuestions? && $scope.surveyQuestions.length > 0
      $scope.user.activation_survey_answers = $scope.surveyQuestions.map (q) -> {question_id: q.id, answer: null}
  
  if $scope.isCurrentMember()
    if $window.survey_questions
      $scope.surveyQuestions = $window.survey_questions
    else
      $scope.surveyQuestions = SurveyQuestion.query () -> 
        addSurveyAnswersToUser()
  if $window.user
    $scope.user = $window.user
    $scope.user.activation_survey_answers = []
  else
    $scope.user = $scope.currentUser
  addSurveyAnswersToUser()

  $scope.isFieldError = (field, validation) ->
    field.$invalid && (!validation? || field.$error[validation]) && (field.$dirty || $scope.triedSubmit)
  $scope.myErrors = () ->
    errs = []
    if $scope.activationForm.firstname.$error.required
      errs.push "First name is requred"
    if $scope.activationForm.lastname.$error.required
      errs.push "Last name is required"
    if $scope.activationForm.email.$error.required
      errs.push "Email is required"
    else if $scope.activationForm.email.$error.email
      errs.push "Email format is invalid"
    if $scope.activationForm.password.$error.required
      errs.push "Password is required"
    if $scope.activationForm.password_confirmation.$error.required
      errs.push "Password Confirmation is required"
    else if $scope.activationForm.password_confirmation.$error.validator
      errs.push "Passwords do not match"
    if $scope.activationForm.password.$error.minlength || $scope.activationForm.password_confirmation.$error.minlength
      errs.push "Password must be at least 6 characters long"
    if $scope.activationForm.tos.$error.required
      errs.push "You must accept the Terms of Use"
    if $scope.activationForm.surveyForm && $scope.activationForm.surveyForm.$error.required
      errs.push "You must answer each survey question above"
    if $scope.activationForm.ageRestriction.$error.required
      errs.push "You must acknowlege the minimum age restriction"
    errs
      
  $scope.enterSubmitButton = () ->
    $scope.triedSubmit = true

  $scope.createUser = ->
    $scope.user['swap-keep'] = ['activation_survey_answers']
    User.update $scope.user, () ->
      $location.path('/users/dashboard')

@UserIndexCtrl = ($scope, $rootScope, $location, User) ->
  if $rootScope.memberSearch? && (memberSearch=$rootScope.memberSearch())
    $scope.memberFilter = memberSearch
  $scope.usersPerPage = 10
  $scope.users = User.query {}, (data) ->
    $scope.loadingComplete = true
    $scope.userPages = Math.ceil(data.length / $scope.usersPerPage)
    $scope.userPage = 1
  $scope.isOnPage = (index) -> Math.floor(index / @usersPerPage) == @userPage - 1
  $scope.selectedUsers = () ->
    filtered = $scope.users
    if $scope.memberFilter? && $scope.memberFilter != ''
      filtered = filtered.filter (user) ->
        regex = new RegExp($scope.memberFilter, "i")
        "#{user.first_name} #{user.last_name}".match(regex) || user.email.match(regex)
    if $scope.dealerFilter? && $scope.dealerFilter != ''
      filtered = filtered.filter (user) ->
        regex = new RegExp($scope.dealerFilter, "i")
        if user.dealership?
          dealership = user.dealership
        else if user.member_dealership?
          dealership = user.member_dealership
        (!dealership? || dealership.name.match(regex) || dealership.city.match(regex) || dealership.state.match(regex) || dealership.zip.match(regex))
    filtered
  $scope.sortingUsers = () ->
    filtered = $scope.users
    if $scope.memberFilter? && $scope.memberFilter != ''
      filtered = filtered.filter (user) ->
        regex = new RegExp($scope.memberFilter, "i")
        "#{user.first_name} #{user.last_name}".match(regex) || user.email.match(regex)
    if $scope.dealerFilter? && $scope.dealerFilter != ''
      filtered = filtered.filter (user) ->
        regex = new RegExp($scope.dealerFilter, "i")
        if user.dealership?
          dealership = user.dealership
        else if user.member_dealership?
          dealership = user.member_dealership
        (!dealership? || dealership.name.match(regex) || dealership.city.match(regex) || dealership.state.match(regex) || dealership.zip.match(regex))
    
    
    $scope.userPages = Math.ceil(filtered.length / $scope.usersPerPage)
    $scope.loadingComplete = true
    $scope.userPage = 1
    $scope.usersCount = filtered.length  
    filtered
  $scope.creditExpiration = (credit) ->
    purchase = Date.parseWithoutTimezone(credit.purchased_date)
    purchase.setFullYear(purchase.getFullYear() + 1)
    purchase
  $scope.creditExpirationInt = (credit) ->
    purchase = Date.parseWithoutTimezone(credit.purchased_date).getTime()
  $scope.userSort = (sort_field) ->
    if @userPredicate == sort_field
      @reverse = !@reverse
    else
      @reverse = false
    @userPredicate = sort_field
  $scope.sortIcon = (sort_field) ->
    {"sort-target": @userPredicate == sort_field, "sort-reverse": @reverse}
  $scope.viewUser = (user) ->
    $location.path("/users/#{user.id}")
  $scope.editUser = (user) ->
    $location.path("/users/#{@user.id}/edit")
  $scope.newReservation = (user) ->
    $scope.reservationUser = user
    $scope.adminRentalOptions.show = true
    $scope.adminRentalDealership = @getCurrentDealership()

@UserNewCtrl = ($scope, $rootScope, $routeParams, $location, User, Dealership) ->
  $scope.user = new User()
  $scope.allDealerships = -> @_allDealerships ?= Dealership.query()
  $scope.createAdminAccount = ->
    User.save @user, (data) -> $location.path("/users/#{data.id}")
  $scope.cancelAccount = -> $location.path('/users/members')

@UserOverviewCtrl = ($scope, $rootScope, $routeParams, $location, User, Reservation, Flash, ActivityLog) ->
  accountHistoryPageSize = 5
  $scope.addRewardDays = {show: false, additionalRewards: 1}
  $scope.user = User.get {id: $routeParams.user_id}, (data) ->
    $scope.reservations = Reservation.query {user_id: data.id}
    $scope.accountHistory = ActivityLog.query {user_id: data.id}, (data) ->
      $scope.accountHistoryPages = Math.ceil data.length / accountHistoryPageSize
      $scope.accountHistoryPage = 1
  $scope.reservationSort = (sort_field) ->
    if @reservationPredicate == sort_field
      @reverse = !@reverse
    else
      @reverse = false
    @reservationPredicate = sort_field
  $scope.sortIcon = (sort_field) ->
    {"sort-target": @reservationPredicate == sort_field, "sort-reverse": @reverse}
  $scope.creditExpiration = (credit) ->
    purchase = Date.parseWithoutTimezone(credit.purchased_date)
    purchase.setFullYear(purchase.getFullYear() + 1)
    purchase
  $scope.formatDate = (dateString) ->
    ms = Date.parse(dateString)
    return '' if isNaN(ms)
    date = new Date(ms)
    return date.toShortDateString() + " " + date.toShortTimeString()
  $scope.createReservation = ->
    if $scope.isCurrentMember()
      $location.path('/search')
    else
      $scope.reservationUser = @user
      $scope.adminRentalOptions.show = true
      $scope.adminRentalDealership = @getCurrentDealership()
  $scope.userDisabled = ->
    return false unless @user.roles?
    isDisabled = true
    isDisabled = false for role, status of @user.roles when status isnt 'disabled'
    isDisabled
  $scope.userNotConfirmed = ->
    @user.roles? && $.inArray('pending', $.map(@user.roles, (status, role) -> status)) >= 0
  $scope.resendConfirmationEmail = ->
    $.post("/users/#{@user.id}/resend_confirmation").done ->
      Flash.setImmediate {type: 'notice', message: 'Confirmation instructions successfully sent'}, true
      $scope.$apply()
    .fail (resp) ->
      if resp.status == 422
        errors = JSON.parse resp.responseText
        errorText = "#{field} #{errs.join(', ')}" for field, errs of errors.errors
      else if resp.status == 403
        errorText = "You do not have permission to resend this user's confirmation"
      else
        errorText = 'An error occurred. Please try again later'
      Flash.setImmediate errorText.capitalize(), true
      $scope.$apply()
  $scope.isOnPage = (index) -> Math.floor(index / accountHistoryPageSize) == @accountHistoryPage - 1
  $scope.showAddRewards = -> 
    @addRewardDays.show = true
    @addRewardDays.additionalRewards = 1
    @addRewardDays.currentRemaining = @user.remainingCredits()
  $scope.cancelAddRewards = -> @addRewardDays.show = false
  $scope.addRewardDays = ->
    Flash.clear()
    $.post("/users/#{@user.id}/add_credits", {credits: @addRewardDays.additionalRewards}).done((data) ->
      $scope.user = new User(data)
      $scope.cancelAddRewards()
      $scope.$apply()
    ).fail((response) -> 
      if response.status == 403
        Flash.setImmediate 'You do not have permission to perform that action'
      else if response.status == 422
        err = JSON.parse response.responseText
        err = "#{field} #{error for error in errors}" for field, errors of err.errors
        Flash.setImmediate err.capitalize(), true
      else
        Flash.setImmediate "An error occurred. Please try again later", true
      $scope.$apply() )

@UserEditCtrl = ($scope, $routeParams, $location, User) ->
  $scope.user = User.get {id: $routeParams.user_id}
  $scope.updateMemberAccount = ->
    if @user.disabled?
      delete @user.disabled
      $.each @user.roles, (role, _) -> $scope.user.roles[role] = 'disabled'
      @user['swap-keep'] = 'roles'
    User.update @user, -> $location.path("/users/#{$scope.user.id}")
  $scope.cancelUpdate = ->
    $location.path("/users/#{@user.id}")

@TermsModalCtrl = ($scope) ->
  $scope.open = ->
    $scope.shouldBeOpen = true

  $scope.close = ->
    $scope.closeMsg = "I was closed at: " + new Date()
    $scope.shouldBeOpen = false

  $scope.opts =
    backdropFade: true
    dialogFade: true

@UserActivationCtrl.$inject = ['$scope', '$window', '$location', 'User', 'SurveyQuestion']
@UserIndexCtrl.$inject = ['$scope', '$rootScope', '$location', 'User']
@UserNewCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'User', 'Dealership']
@UserOverviewCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'User', 'Reservation', 'Flash', 'ActivityLog']
@UserEditCtrl.$inject = ['$scope', '$routeParams', '$location', 'User']
@TermsModalCtrl.$inject = ['$scope']