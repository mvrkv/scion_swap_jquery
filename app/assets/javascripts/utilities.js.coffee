Date.DAYS_OF_WEEK = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
Date.prototype.getDayName = -> Date.DAYS_OF_WEEK[@getDay()]
Date.prototype.getDayAbbr = -> Date.DAYS_OF_WEEK[@getDay()].substr(0,3)
Date.prototype.getMonthName = -> ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][@getMonth()]
Date.prototype.toShortTimeString = ->
  hours = @getHours() % 12
  hours = 12 if hours <= 0
  minutes = @getMinutes()
  minutes = '0' + minutes if minutes < 10
  meridian = 'AM'
  meridian = 'PM' if @getHours() >= 12
  "#{hours}:#{minutes}#{meridian}"
Date.prototype.toLongString = -> "#{@getDayName()} #{@getMonthName()} #{@getDate()}, #{@getFullYear()}"
Date.prototype.toShortDateString = -> @getFullYear() + '-' + ('0' + (@getMonth() + 1)).slice(-2) + '-' + ('0' + @getDate()).slice(-2)
Date.prototype.toDayDateString = -> "#{@getDayAbbr()} #{@getMonth() + 1}/#{('0' + @getDate()).slice(-2)}"
Date.parseWithoutTimezone = (dateString) ->
  [year, month, date] = dateString.split('-')
  day = new Date()
  day.setFullYear(year)
  day.setMonth(parseInt(month, 10) - 1)
  day.setDate(date)
  day
String.prototype.capitalize = -> if @length > 0 then @[0].toUpperCase() + @slice(1) else ''
