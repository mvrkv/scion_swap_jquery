class UserMailer < ActionMailer::Base
  default :from => '"Wheelz" <pitcrew@wheelz.com>'
  layout 'mailer'

  def welcome(user)
    @user = user
    mail(:to => user.email)
  end
  
  def activation_reminder(user)
    return unless (user.roles['member'] == 'pending' &&  !user.activation_mail_unsubscribe)
    @user = user
    @is_second_reminder = @user.created_at < 10.days.ago
    mail(:to => user.email) 
  end

  def account_updated(user)
    @user = user
    mail(:to => user.email)
  end
  
  def monthly_activity(user)
    @user = user
    mail(:to => user.email)
  end
  
  def expiration_warning(user, purchases)
    return unless user.is_active_member?
    @user = user
    @credits_expiring = purchases.sum{|p| p.credits_remaining}
    @expiration_date = purchases.map(&:expiry_date).min
    mail(:to => user.email)
  end
end
