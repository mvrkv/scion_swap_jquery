class AnalyticsMailer < ActionMailer::Base
  default :from => '"Wheelz" <pitcrew@wheelz.com>'
  layout 'mailer'

  def analytics_report(recipients, subject, report_content, body='')
    attachments['report.csv'] = report_content
    @body = body
    mail(:to => recipients, :subject => subject)
  end
end