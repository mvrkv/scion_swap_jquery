class ReservationMailer < ActionMailer::Base
  default :from => '"Toyota" <pitcrew@scion-swap.com>'
  layout 'mailer'

  def new_member_reservation(reservation)
    @reservation = reservation
    @credits_after_reservation = [reservation.user.credits_available(reservation) - reservation.length, 0].max
    mail(:to => reservation.user.email)
    
  end
  
  def reservation_mail_to_dealer(reservation)
     @reservation = reservation
     emails = User.find_all_by_dealership_id(reservation.dealership.id).map(&:email)    
     mail(:to => emails, :subject => "New Reservation booked")    
  end
end
