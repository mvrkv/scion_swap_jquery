class RemoveEndDateFromFleetGroupQuantities < ActiveRecord::Migration
  def change
    remove_column :fleet_group_quantities, :end_date
    change_column :fleet_group_quantities, :start_date, :date, :null => false
  end
end
