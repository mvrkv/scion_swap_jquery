class CreateDealershipHolidays < ActiveRecord::Migration
  def change
    create_table :dealership_holidays do |t|
      t.references :dealership, :null => false
      t.date :date, :null => false
    end
  end
end
