class AddActivationReminderColumnUserTable < ActiveRecord::Migration
  def up
    add_column :users , :activation_mail_unsubscribe, :boolean ,:default => false 
  end

  def down
  end
end
