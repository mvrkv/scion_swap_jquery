class CreateDealerships < ActiveRecord::Migration
  def change
    create_table :dealerships do |t|
      t.string :name
      t.string :city
      t.string :state

      t.timestamps
    end

    add_column :users, :dealership_id, :integer
  end
end
