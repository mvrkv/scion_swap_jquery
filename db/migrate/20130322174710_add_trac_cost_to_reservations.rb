class AddTracCostToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :trac_cost, :decimal
    add_column :reservations, :trac_days, :integer
  end
end
