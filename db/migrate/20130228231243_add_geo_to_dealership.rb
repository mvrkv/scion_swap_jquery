class AddGeoToDealership < ActiveRecord::Migration
  def change
    add_column :dealerships, :street, :string 
    add_column :dealerships, :zip, :string 
    add_column :dealerships, :latitude, :float 
    add_column :dealerships, :longitude, :float 
  end
end
