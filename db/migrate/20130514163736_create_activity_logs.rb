class CreateActivityLogs < ActiveRecord::Migration
  def change
    create_table :activity_logs do |t|
      t.references :user, :reservation
      t.integer :changed_by_user_id
      t.hstore :resource_changes

      t.timestamps
    end
  end
end
