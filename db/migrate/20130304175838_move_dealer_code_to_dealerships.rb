class MoveDealerCodeToDealerships < ActiveRecord::Migration
  def change
    remove_column :purchases, :dealer_code
    add_column :purchases, :dealership_id, :integer

    add_column :dealerships, :dealer_code, :string
  end
end
