class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.references :fleet_group
      t.date :start_date
      t.date :end_date
      t.datetime :pickup
      t.datetime :dropoff
      t.string :rental_agreement_id
      t.references :user

      t.timestamps
    end
    add_index :reservations, :fleet_group_id
    add_index :reservations, :user_id
  end
end
