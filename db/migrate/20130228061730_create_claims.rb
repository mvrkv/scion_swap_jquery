class CreateClaims < ActiveRecord::Migration
  def change
    create_table :claims do |t|
      t.references :reservation
      t.references :purchase
      t.integer :credits
      
      t.timestamps
    end
  end
end
