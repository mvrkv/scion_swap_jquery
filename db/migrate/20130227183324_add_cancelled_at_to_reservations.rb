class AddCancelledAtToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :cancelled_at, :datetime
  end
end
