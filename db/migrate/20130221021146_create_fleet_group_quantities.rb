class CreateFleetGroupQuantities < ActiveRecord::Migration
  def change
    create_table :fleet_group_quantities do |t|
      t.date :start_date
      t.date :end_date
      t.integer :quantity
      t.references :fleet_group

      t.timestamps
    end
    add_index :fleet_group_quantities, :fleet_group_id
  end
end
