class CreateFleetGroups < ActiveRecord::Migration
  def change
    create_table :fleet_groups do |t|
      t.string :make
      t.string :model
      t.references :dealership

      t.timestamps
    end
  end
end
