class AddRentalFleetGroupIdToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :rental_fleet_group_id, :integer
  end
end
