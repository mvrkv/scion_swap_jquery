class AddPhoneToDealerships < ActiveRecord::Migration
  def change
    add_column :dealerships, :phone, :string
  end
end
