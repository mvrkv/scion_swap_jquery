class CreateActivationSurvey < ActiveRecord::Migration
  def change
    create_table :activation_survey_questions do |t|
      t.string :question
      t.string_array :available_answers
    end

    create_table :activation_survey_answers do |t|
      t.references :user
      t.references :activation_survey_question
      t.string :answer
    end
  end
end
