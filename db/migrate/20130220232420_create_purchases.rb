class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.string :vin
      t.string :dealer_code
      t.references :user
      t.date :purchased_date
    end
  end
end
