class AddDescriptionAndImageToFleetGroup < ActiveRecord::Migration
  def change
    add_column :fleet_groups, :description, :text
    add_column :fleet_groups, :image_url, :string
  end
end
