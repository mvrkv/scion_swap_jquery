class AddOperatingHoursToDealership < ActiveRecord::Migration
  def change
    add_column :dealerships, :is_open, :boolean_array
    add_column :dealerships, :open_time, :time_array
    add_column :dealerships, :close_time, :time_array
  end
end
