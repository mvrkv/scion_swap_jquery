class CreateHstoreExtension < ActiveRecord::Migration
  def up
    execute 'CREATE EXTENSION if not exists hstore'
  end

  def down
    execute 'DROP EXTENSION if exists hstore'
  end
end
