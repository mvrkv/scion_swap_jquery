class AddProductUrlToFleetGroup < ActiveRecord::Migration
  def change
    add_column :fleet_groups, :product_url, :string
  end
end
