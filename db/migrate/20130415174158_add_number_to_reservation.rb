class AddNumberToReservation < ActiveRecord::Migration
  def change
    add_column :reservations, :number, :string
  end
end
