class AddInstructionsToDealership < ActiveRecord::Migration
  def change
    add_column :dealerships, :instructions, :string, :limit => 255
  end
end
