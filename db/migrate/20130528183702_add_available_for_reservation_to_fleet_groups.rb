class AddAvailableForReservationToFleetGroups < ActiveRecord::Migration
  def change
    add_column :fleet_groups, :available_for_reservation, :boolean
  end
end
