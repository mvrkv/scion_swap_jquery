class MakeFleetGroupQuantityDatesNullable < ActiveRecord::Migration
  def change
    change_column :fleet_group_quantities, :start_date, :date, :null => true
    change_column :fleet_group_quantities, :end_date, :date, :null => true
  end
end
