class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.references :purchase
      t.integer :credit
      t.string :reason

      t.timestamps
    end
  end
end
