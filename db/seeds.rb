# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Dealership.find_or_create_by_name(:name => "West Kendall Toyota", :city => "Miami", :street => "13800 SW 136th St", :zip => "33186", :state => "FL", :dealer_code => "09216", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Kendall Toyota", :city => "Miami", :street => "10943 S Dixie Hwy", :zip => "33156", :state => "FL", :dealer_code => "09204", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Toyota of Hollywood", :city => "Hollywood", :street => "1841 Florida 7", :zip => "33021", :state => "FL", :dealer_code => "09107", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Headquarter Toyota", :city => "Hialeah", :street => "5895 NW 167th St", :zip => "33015", :state => "FL", :dealer_code => "09163", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Lipton Toyota", :city => "Fort Lauderdale", :street => "1700 W Oakland Park Blvd", :zip => "33311", :state => "FL", :dealer_code => "09158", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "AutoNation Toyota Weston", :city => "Davie", :street => "4050 Weston Rd", :zip => "33331", :state => "FL", :dealer_code => "09206", :phone => '9999999999')

Dealership.find_or_create_by_name(:name => "Fred Haas Toyota World", :city => "Spring", :street => "20400 Interstate 45", :zip => "77373", :state => "TX", :dealer_code => "42095", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Don McGill Toyota of Katy", :city => "Katy", :street => "21555 Katy Freeway", :zip => "77450", :state => "TX", :dealer_code => "42284", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Don McGill Toyota", :city => "Houston", :street => "11800 Katy Freeway", :zip => "77079", :state => "TX", :dealer_code => "42284", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Joe Myers Toyota", :city => "Houston", :street => "Jersey Village", :zip => "77065", :state => "TX", :dealer_code => "42270", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Mike Calvert Toyota", :city => "Houston", :street => "2333 South Loop West Freeway", :zip => "77054", :state => "TX", :dealer_code => "42138", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Tejas Toyota", :city => "Humble", :street => "19011 Highway 59 North", :zip => "77338", :state => "TX", :dealer_code => "42119", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Sterling McCall Toyota", :city => "Houston", :street => "9400 Southwest Freeway", :zip => "77074", :state => "TX", :dealer_code => "42073", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Fred Haas Toyota Country", :city => "Houston", :street => "22435 Texas 249", :zip => "77070", :state => "TX", :dealer_code => "42282", :phone => '9999999999')

Dealership.find_or_create_by_name(:name => "Millenium Toyota", :city => "Hempstead", :street => "257 North Franklin Street", :zip => "11550", :state => "NY", :dealer_code => "31154", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Smithtown Toyota", :city => "Smithtown", :street => "360 Jericho Turnpike", :zip => "11787", :state => "NY", :dealer_code => "31105", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Atlantic Toyota", :city => "Amityville", :street => "200 Sunrise Highway", :zip => "11701", :state => "NY", :dealer_code => "31166", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Sunrise Toyota", :city => "Oakdale", :street => "3984 Sunrise Highway", :zip => "11769", :state => "NY", :dealer_code => "31178", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Westbury Toyota", :city => "Westbury", :street => "1121 Old Country Road", :zip => "11590", :state => "NY", :dealer_code => "31181", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Competition Toyota", :city => "Middle Island", :street => "910 Middle Country Road", :zip => "11953", :state => "NY", :dealer_code => "31130", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Riverhead Toyota", :city => "Riverhead", :street => "1655 Old Country Road", :zip => "11901", :state => "NY", :dealer_code => "31139", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Penn Toyota", :city => "Greenvale", :street => "2400 Northern Boulevard", :zip => "11548", :state => "NY", :dealer_code => "31076", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Huntington Toyota", :city => "Huntington Station", :street => "1030 East Jericho Turnpike", :zip => "11746", :state => "NY", :dealer_code => "31179", :phone => '9999999999')
Dealership.find_or_create_by_name(:name => "Advantage Toyota", :city => "Valley Stream", :street => "400 West Sunrise Highway", :zip => "11581", :state => "NY", :dealer_code => "31173", :phone => '9999999999')

Dealership.all.each do |d|
  fleet_group = d.fleet_groups.find_or_create_by_make_and_model(:make => "Toyota", :model => "Camry", available_for_reservation: true)
  fleet_group.update_attribute(:dealership, d)
  fleet_group.update_attribute(:image_url, "/assets/camry.png")
  fleet_group.update_attribute(:product_url, "http://www.toyota.com/camry")
  fleet_group.update_attribute(:description, "Camry has long been one of America's favorite cars. It provides a spacious and refined interior, 10 standard airbags, estimated 25 city/35 highway MPG, and sport tuned handling.")
  FleetGroupQuantity.create(:start_date => Date.today, :quantity => 5, :fleet_group => fleet_group)

  fleet_group = d.fleet_groups.find_or_create_by_make_and_model(:make => "Toyota", :model => "Prius", available_for_reservation: true)
  fleet_group.update_attribute(:dealership, d)
  fleet_group.update_attribute(:image_url, "/assets/prius.png")
  fleet_group.update_attribute(:product_url, "http://www.toyota.com/prius")
  fleet_group.update_attribute(:description, "With its seamlessly integrated advanced technology, top-class fuel efficiency, generous dimensions of the rear seat, and the size and versatility of the cargo area, Prius provides occupants with the space and cargo capacity of a typical midsized sedan.")
  FleetGroupQuantity.create(:start_date => Date.today, :quantity => 5, :fleet_group => fleet_group)

  fleet_group = d.fleet_groups.find_or_create_by_make_and_model(:make => "Toyota", :model => "Tacoma", available_for_reservation: true)
  fleet_group.update_attribute(:dealership, d)
  fleet_group.update_attribute(:image_url, "/assets/tacoma.png")
  fleet_group.update_attribute(:product_url, "http://www.toyota.com/tacoma")
  fleet_group.update_attribute(:description, "Tacoma combines ample interior amenities with work-truck versatility, to help you move your stuff.")
  FleetGroupQuantity.create(:start_date => Date.today, :quantity => 5, :fleet_group => fleet_group)

  fleet_group = d.fleet_groups.find_or_create_by_make_and_model(:make => "Toyota", :model => "Sienna", available_for_reservation: true)
  fleet_group.update_attribute(:dealership, d)
  fleet_group.update_attribute(:image_url, "/assets/sienna.png")
  fleet_group.update_attribute(:product_url, "http://www.toyota.com/sienna")
  fleet_group.update_attribute(:description, "The versatile and accommodating Sienna gives minivan drivers an unbeatable combination. With a roomy and well-appointed interior, versatile cargo and seating for 7 to 8 passengers, Sienna offers just about everything a family needs.")
  FleetGroupQuantity.create(:start_date => Date.today, :quantity => 5, :fleet_group => fleet_group)

  dealer_rep = User.find_or_create_by_email(:first_name => "John", :last_name => "Dealer", :email => "dealer+#{d.id}@wheelz.com", :password => "123456", :roles => {"dealer_rep"=>"active"})
  dealer_rep.update_attribute(:dealership, d)
  member_support_rep = User.find_or_create_by_email(:first_name => "John", :last_name => "Member Support", :email => "member_support+#{d.id}@wheelz.com", :password => "123456", :roles => {"member_support"=>"active"})
  member_support_rep.update_attribute(:dealership, d)
end
