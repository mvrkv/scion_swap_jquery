# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131010070445) do

  add_extension "hstore"

  create_table "activation_survey_answers", :force => true do |t|
    t.integer "user_id"
    t.integer "activation_survey_question_id"
    t.string  "answer"
  end

  create_table "activation_survey_questions", :force => true do |t|
    t.string "question"
    t.string "available_answers", :array => true
  end

  create_table "activity_logs", :force => true do |t|
    t.integer  "user_id"
    t.integer  "reservation_id"
    t.integer  "changed_by_user_id"
    t.hstore   "resource_changes"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "claims", :force => true do |t|
    t.integer  "reservation_id"
    t.integer  "purchase_id"
    t.integer  "credits"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "credits", :force => true do |t|
    t.integer  "purchase_id"
    t.integer  "credit"
    t.string   "reason"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "dealership_holidays", :force => true do |t|
    t.integer "dealership_id", :null => false
    t.date    "date",          :null => false
  end

  create_table "dealerships", :force => true do |t|
    t.string   "name"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "street"
    t.string   "zip"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "dealer_code"
    t.boolean  "is_open",                      :array => true
    t.time     "open_time",                    :array => true
    t.time     "close_time",                   :array => true
    t.string   "instructions"
    t.string   "phone"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "fleet_group_quantities", :force => true do |t|
    t.date     "start_date",     :null => false
    t.integer  "quantity"
    t.integer  "fleet_group_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "fleet_group_quantities", ["fleet_group_id"], :name => "index_fleet_group_quantities_on_fleet_group_id"

  create_table "fleet_groups", :force => true do |t|
    t.string   "make"
    t.string   "model"
    t.integer  "dealership_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.text     "description"
    t.string   "image_url"
    t.string   "product_url"
    t.boolean  "available_for_reservation"
  end

  create_table "purchases", :force => true do |t|
    t.string  "vin"
    t.integer "user_id"
    t.date    "purchased_date"
    t.integer "dealership_id"
  end

  create_table "reservations", :force => true do |t|
    t.integer  "fleet_group_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "pickup"
    t.datetime "dropoff"
    t.string   "rental_agreement_id"
    t.integer  "user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.datetime "cancelled_at"
    t.decimal  "trac_cost"
    t.integer  "trac_days"
    t.string   "number"
    t.integer  "rental_fleet_group_id"
  end

  add_index "reservations", ["fleet_group_id"], :name => "index_reservations_on_fleet_group_id"
  add_index "reservations", ["user_id"], :name => "index_reservations_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "encrypted_password",          :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "first_name"
    t.string   "last_name"
    t.hstore   "roles"
    t.datetime "confirmation_read_at"
    t.datetime "tos_accepted_at"
    t.string   "customer_ref_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "dealership_id"
    t.string   "authentication_token"
    t.boolean  "activation_mail_unsubscribe", :default => false
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
