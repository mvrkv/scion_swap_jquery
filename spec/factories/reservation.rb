FactoryGirl.define do
  factory :reservation, aliases: [:future_reservation, :pending_reservation] do 
    start_date { Date.today + 5  }
    end_date   { Date.today + 10 }

    association :user, factory: :member
    fleet_group

    factory :past_reservation do
      start_date { Date.today - 7 }
      end_date   { Date.today - 5 }
      pickup     { Date.today - 7 }

    end

    factory :cancelled_reservation do
      start_date   { Date.today - 10 }
      end_date     { Date.today - 8 }
      cancelled_at { Date.today - 13 }
    end

    factory :active_reservation do
      start_date   { Date.today - 1 }
      end_date     { Date.today + 1 }
      pickup       { Date.today - 1 }
      rental_agreement_id { SecureRandom.random_number(1000) }
    end
  end
end
