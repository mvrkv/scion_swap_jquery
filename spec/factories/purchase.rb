FactoryGirl.define do
  factory :purchase do
    vin { rand(10 ** 8).to_s.rjust(8, '0') }
    sequence(:dealer_code) { |n| "PurchaseDealerCode #{n}" }
    purchased_date { Time.now.to_date }
    association :user, factory: :member

    ignore do
      entitlements 10
    end

    before(:create) do |purchase, evaluator|
      if Dealership.where(:dealer_code => purchase.dealer_code).blank?
        FactoryGirl.create(:dealership, dealer_code: purchase.dealer_code, reps_count: 0)
      end
    end

    after(:create) do |purchase, evaluator|
      FactoryGirl.create(:credit, purchase: purchase, credit: evaluator.entitlements)
    end
  end
end
