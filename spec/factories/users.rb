FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    sequence(:email) {|n|  "user#{n}@example.com" }
    password 'iL1k32Sw@pMySci0N'

    ignore do
      entitlements 10
      reservation_count 0
    end

    trait :customer_ref_id do
      customer_ref_id { rand(10 ** 8).to_s.rjust(8, '0') }
    end

    trait :with_vehicle_purchase do
      after(:create) do |user, evaluator|
        FactoryGirl.create(:purchase, user: user, entitlements: evaluator.entitlements)
      end
    end

    trait :with_reservations do
      after(:create) do |user, evaluator|
        FactoryGirl.create_list(:reservation, evaluator.reservation_count.to_i, user: user) unless evaluator.reservation_count.to_i < 1
      end
    end

    trait :confirmed do
      confirmed_at { Time.zone.now }
      roles member: 'active'
    end

    factory :member, traits: [:customer_ref_id, :with_vehicle_purchase, :confirmed]

    factory :unconfirmed_member, traits: [:customer_ref_id, :with_vehicle_purchase] do
      roles member: 'pending'
    end

    factory :dealer_rep do
      roles dealer_rep: 'active'

      ignore do
        dealership nil
      end
      after(:create) do |dealer_rep, evaluator|
        if evaluator.dealership.present?
          dealer_rep.dealership = evaluator.dealership
        elsif Dealership.count > 0
          dealer_rep.dealership = Dealership.first
        else
          dealer_rep.dealership = FactoryGirl.create(:dealership, reps_count: 0)
        end
        dealer_rep.save
      end
    end

    factory :member_support do
      roles member_support: 'active'
    end
  end
end
