FactoryGirl.define do
  factory :fleet_group_quantity do
    start_date '2013-01-01'
    quantity 5
    association :fleet_group, quantity: -1
  end
end
