FactoryGirl.define do
  factory :fleet_group do
    dealership
    make 'Toyota'
    model 'Camry'
    product_url 'http://www.toyota.com/camry'
    ignore do
      quantity 5
    end

    after(:create) do |fleet_group, evaluator|
      if evaluator.quantity.to_i >= 0
        f = FactoryGirl.create :fleet_group_quantity, quantity: evaluator.quantity, fleet_group: fleet_group
      end
    end
  end
end
