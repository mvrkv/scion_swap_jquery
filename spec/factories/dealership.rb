FactoryGirl.define do
  factory :dealership do
    sequence(:name, 'a') {|n|  "Dealer #{n}" }
    sequence(:dealer_code) { |n| "DealerCode #{n}" }
    street '151 10th St'
    city 'San Francisco'
    state 'CA'
    zip '94103'
    latitude 37.77493
    longitude -122.419415

    ignore do
      reps_count 1
    end

    after(:create) do |dealership, evaluator|
      unless dealership.dealer_reps.count > 0
        FactoryGirl.create_list :dealer_rep, evaluator.reps_count, dealership: dealership
      end
    end

    trait :daly_city do
      name "City Toyota - Scion"
      street "255 San Pedro Rd"
      city "Daly City"
      state "CA"
      zip "94014"
      latitude 37.67603
      longitude -122.46934
    end

    trait :burlingame do
      name "Putnam Toyota"
      street "50 California Dr"
      city "Burlingame"
      state "CA"
      zip "94010"
      latitude 37.57791
      longitude -122.34042
    end

    trait :san_francisco do
      name "San Francisco Toyota & Scion"
      street "1701 Van Ness Ave"
      city "San Francisco"
      state "CA"
      zip "94109"
      latitude 37.79142
      longitude -122.42300
    end

    trait :san_bruno do
      name "Melody Toyota"
      street "750 El Camino Real"
      city "San Bruno"
      state "CA"
      zip "94066"
      latitude 37.62804
      longitude -122.41586
    end

    trait :houston do
      name "Autonation Scion Gulf Freeway"
      street '12111 Gulf Freeway'
      city 'Houston'
      state 'TX'
      zip '77034'
      latitude 29.850173
      longitude -95.326996
    end

    trait :miami do
      name "Scion Of South Florida"
      street '9775 Northwest 12th Street'
      city 'Doral'
      state 'FL'
      zip '33172'
      latitude 25.858606
      longitude -80.306625
    end
  end
end
