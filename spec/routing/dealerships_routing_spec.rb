require "spec_helper"

describe DealershipsController do
  describe "Routing" do
    it "should NOT route to #index" do
      expect(get("/dealerships.json")).to_not route_to("dealerships#index", :format => "json")
    end

    it "should route to #show" do
      expect(get("/dealerships/1.json")).to route_to("dealerships#show", :id => "1", :format => "json")
    end

    it "should NOT route to #create" do
      expect(post("/dealerships.json")).to_not route_to("dealerships#create", :format => "json")
    end

    it "should route to #update" do
      pending("this should be looked at because the route is there but the action is not")
      expect(put("/dealerships/1.json")).to route_to("dealerships#update", :id => "1", :format => "json")
    end

    it "should NOT route to #destroy" do
      expect(delete("/dealerships/1.json")).to_not route_to("dealerships#destroy", :id => "1", :format => "json")
    end
  end
end
