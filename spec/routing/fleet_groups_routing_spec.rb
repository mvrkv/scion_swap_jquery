require "spec_helper"

describe FleetGroupsController do
  describe "Routing" do
    it "should route to #index" do
      expect(get("/dealerships/1/fleet_groups.json")).to route_to("fleet_groups#index", :dealership_id => "1", :format => "json")
    end

    it "should route to #show" do
      expect(get("/dealerships/1/fleet_groups/2.json")).to route_to("fleet_groups#show", :dealership_id => "1", :id => "2", :format => "json")
    end

    it "should NOT route to #create" do
      expect(post("/dealerships/1/fleet_groups.json")).to_not route_to("fleet_groups#create", :format => "json")
    end

    it "should route to #update" do
      expect(put("/dealerships/1/fleet_groups/2.json")).to route_to("fleet_groups#update", :dealership_id => "1", :id => "2", :format => "json")
    end

    it "should NOT route to #destroy" do
      expect(delete("/dealerships/1/fleet_groups/2.json")).to_not route_to("fleet_groups#destroy", :dealership_id => "1", :id => "2", :format => "json")
    end
  end
end
