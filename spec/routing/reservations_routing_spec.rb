require "spec_helper"

describe ReservationsController do
  describe "Routing" do
    it "should route to #index" do
      expect(get("/reservations.json")).to route_to("reservations#index", :format => "json")
    end

    it "should route to #show" do
      expect(get("/reservations/1.json")).to route_to("reservations#show", :id => "1", :format => "json")
    end

    it "should route to #create" do
      expect(post("/reservations.json")).to route_to("reservations#create", :format => "json")
    end

    it "should route to #update" do
      expect(put("/reservations/1.json")).to route_to("reservations#update", :id => "1", :format => "json")
    end

    it "should route to #destroy" do
      pending
      expect(delete("/reservations/1.json")).to route_to("reservations#destroy", :id => "1", :format => "json")
    end

    it "should route to #start" do
      expect(put("/reservations/1/start.json")).to route_to("reservations#start", :id => "1", :format => "json")
    end

    it "should route to #end" do
      expect(put("/reservations/1/end.json")).to route_to("reservations#end", :id => "1", :format => "json")
    end

    it "should route to #cancel" do
      expect(put("/reservations/1/cancel.json")).to route_to("reservations#cancel", :id => "1", :format => "json")
    end
  end
end
