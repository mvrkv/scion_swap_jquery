require "spec_helper"

describe UsersController do
  describe "Routing" do
    it "should route to #index" do
      expect(get("/users.json")).to route_to("users#index", :format => "json")
    end

    it "should route to #show" do
      expect(get("/users/1.json")).to route_to("users#show", :id => "1", :format => "json")
    end

    it "should route to #create" do
      expect(post("/users.json")).to route_to("users#create", :format => "json")
    end

    it "should route to #update" do
      expect(put("/users/1.json")).to route_to("users#update", :id => "1", :format => "json")
    end

    it "should route to #destroy" do
      pending
      expect(delete("/users/1.json")).to route_to("users#destroy", :id => "1", :format => "json")
    end
  end
end
