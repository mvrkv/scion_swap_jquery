require 'spec_helper'
 
describe UserMailer do
  describe "#welcome" do
    let(:user) { mock_model(User, :name => 'rspec', :email => 'rspec@wheelz.com') }
    let(:mail) { UserMailer.welcome(user) }
 
    #ensure that the subject is correct
    it 'renders the subject' do
      expect(mail.subject).to eq('Welcome to Scion Swap!')
    end
 
    #ensure that the receiver is correct
    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end
 
    #ensure that the sender is correct
    it 'renders the sender email' do
      expect(mail.from).to eq(['member_support@swap.com'])
    end
 
    #ensure that the @name variable appears in the email body
    it 'assigns @name' do
      pending
      mail.body.encoded.should match(user.name)
    end
 
    #ensure that the @confirmation_url variable appears in the email body
    it 'assigns @confirmation_url' do
      pending
      mail.body.encoded.should match("http://application_url/#{user.id}/confirmation")
    end
  end

  describe "#account_updated" do
    let(:user) { mock_model(User, :name => 'rspec', :email => 'rspec@wheelz.com') }
    let(:mail) { UserMailer.account_updated(user) }
 
    #ensure that the subject is correct
    it 'renders the subject' do
      expect(mail.subject).to eq('Your account has been updated')
    end
 
    #ensure that the receiver is correct
    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end
 
    #ensure that the sender is correct
    it 'renders the sender email' do
      expect(mail.from).to eq(['member_support@swap.com'])
    end
  end
end