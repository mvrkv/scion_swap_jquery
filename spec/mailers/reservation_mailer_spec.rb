require 'spec_helper'
 
describe ReservationMailer do
  describe "#new_member_reservation" do
    before(:each) do
      @r1 = FactoryGirl.create(:reservation)
      @mail = ReservationMailer.new_member_reservation(@r1)
    end
 
    #ensure that the subject is correct
    it 'renders the subject' do
      expect(@mail.subject).to eq("Your reservation for a #{@r1.fleet_group.humanize} on #{@r1.start_date}")
    end
 
    #ensure that the receiver is correct
    it 'renders the receiver email' do
      expect(@mail.to).to eq([@r1.user.email])
    end
 
    #ensure that the sender is correct
    it 'renders the sender email' do
      pending
      expect(@mail.from).to eq(['member_support@swap.com'])
    end
  end

  describe "#new_dealer_reservation" do
    before(:each) do
      @r2 = FactoryGirl.create(:reservation)
      @mail = ReservationMailer.new_dealer_reservation(@r2)
    end
 
    #ensure that the subject is correct
    it 'renders the subject' do
      expect(@mail.subject).to eq("A new reservation has been made on #{@r2.fleet_group.humanize}")
    end
 
    #ensure that the receiver is correct
    it 'renders the receiver email' do
      expect(@mail.to).to eq(@r2.fleet_group.dealership.dealer_reps.map(&:email))
    end
 
    #ensure that the sender is correct
    it 'renders the sender email' do
      pending
      expect(@mail.from).to eq(['member_support@swap.com'])
    end
  end
end