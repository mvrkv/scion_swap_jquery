require 'spec_helper'

describe Claim do
  it "should have a valid factory" do
    FactoryGirl.create(:claim)
  end

  describe "Associations" do
  	it { should belong_to(:reservation)}
  	it { should belong_to(:purchase)}
  end

  describe "Validations" do
  end

  describe "Scopes" do
  end
end
