require 'spec_helper'

describe Purchase do
  it "should have a valid factory" do
    FactoryGirl.create(:purchase)
  end

  describe "Associations" do
    it { should belong_to(:user)}
    it { should belong_to(:dealership)}
    it { should have_many(:credits)}
    it { should have_many(:claims)}
  end

  describe "Validations" do
    it { pending ;should validate_uniqueness_of(:vin)}
    it { should validate_presence_of(:dealership)}
  end

  describe "Scopes" do
  end

  describe "Instance Methods" do
    describe "#assign_dealership" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#credits_remaining" do
      it "should have a unit test" do
        pending
      end
    end
  end
end
