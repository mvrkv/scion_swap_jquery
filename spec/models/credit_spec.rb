require 'spec_helper'

describe Credit do
  it "should have a valid factory" do
    FactoryGirl.create(:credit)
  end

  describe "Associations" do
  	it { should belong_to(:purchase)}
  end

  describe "Validations" do
  end

  describe "Scopes" do
  end
end
