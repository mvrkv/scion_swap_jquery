require 'spec_helper'

describe Reservation do
  it "should have a valid factory" do
    FactoryGirl.create(:reservation)
  end

  describe "Associations" do
    it { should belong_to(:user)}
    it { should belong_to(:fleet_group)}

    it { should have_many(:claims)}
  end

  describe "Validations" do
    it { should validate_presence_of(:fleet_group)}
    it { should validate_presence_of(:start_date)}
    it { should validate_presence_of(:end_date)}
    it { should validate_presence_of(:user)}
  end

  describe "Scopes" do
  end

  describe "Instance Methods" do
    describe "#length" do
      before do
        Delorean.time_travel_to '2013-04-15'
        @pending_reservation = FactoryGirl.create(:reservation, start_date: '2013-05-01', end_date: '2013-05-05')
      end

      after do
        Delorean.back_to_the_present
      end
  
      it "should equal the number of days of the reservation" do
        expect(@pending_reservation.length).to be 5
      end
    end

    describe "#cancel" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#end" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#start" do
      it "should have a unit test" do
        pending
      end
    end

    describe '#trac_days' do
      before do
        @member = FactoryGirl.create(:member)
        expect(@member.credits).to be 10
        Delorean.time_travel_to '2013-04-15'
      end

      after do
        Delorean.back_to_the_present
      end

      context 'when the member has only one pending reservation' do
        context 'when the pending reservation is less than 10 days' do
          before do
            @reservation = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-08')
            expect(@reservation.length).to be 8
          end

          it 'should be 0' do
            expect(@reservation.send(:trac_days)).to be 0
          end
        end

        context 'when the pending reservation is 12 days' do
          before do
            @reservation = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-12')
            expect(@reservation.length).to be 12
          end

          it 'should be 2' do
            expect(@reservation.send(:trac_days)).to be 2
          end
        end
      end

      context 'when the member has one pending and one completed reservation', observers: [:reservation_observer] do
        before do
          @completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-08', pickup: '2013-03-01')
          @completed.update_attributes(:dropoff => '2013-03-08')
          expect(@completed.claims.length).to be > 0
          expect(@completed.length).to be 8
        end

        context 'when pending reservation is covered by entitlements' do
          before do
            @pending = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-02')
            expect(@pending.length).to be 2
          end

          it 'should be 0' do
            expect(@pending.send(:trac_days)).to be 0
          end
        end

        context 'when pending reservation is longer than remaining entitlements' do
          before do
            @pending = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-04')
            expect(@pending.length).to be 4
          end

          it 'should be 2' do
            expect(@pending.send(:trac_days)).to be 2
          end
        end
      end

      context 'when the member has two pending and one completed reservation', observers: [:reservation_observer] do
        before do
          @completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', pickup: '2013-03-01')
          @completed.update_attributes(:dropoff => '2013-03-05')
          expect(@completed.claims.length).to be > 0
          expect(@completed.length).to be 5
        end

        context 'when both pending reservations are covered by entitlements' do
          before do
            @pending1 = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-02')
            expect(@pending1.length).to be 2
            @pending2 = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-03', end_date: '2013-05-05')
            expect(@pending2.length).to be 3
          end

          it 'should be 0 for the first pending' do
            expect(@pending1.send(:trac_days)).to be 0
          end

          it 'should be 0 for the second pending' do
            expect(@pending2.send(:trac_days)).to be 0
          end

          context 'when the first pending is updated to use the remaining entitlements' do
            before do
              @pending1.update_attributes(end_date: '2013-05-05')
            end

            it 'should be 0 for the first pending' do
              expect(@pending1.send(:trac_days)).to be 0
            end
            
            it 'should be 3 for the second pending' do
              expect(@pending2.send(:trac_days)).to be 3
            end
          end

          context 'when the second pending is updated to use the remaining entitlements' do
            before do
              @pending2.update_attributes(end_date: '2013-05-08')
            end

            it 'should be 0 for the first pending' do
              expect(@pending1.send(:trac_days)).to be 0
            end

            it 'should be 3 for the second pending' do
              expect(@pending2.send(:trac_days)).to be 3
            end
          end
        end

        context 'when the second pending reservation is partially covered by entitlements' do
          before do
            @pending1 = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-03')
            expect(@pending1.length).to be 3
            @pending2 = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-11', end_date: '2013-05-14')
            expect(@pending2.length).to be 4
          end

          it 'should be 0 for the first pending' do
            expect(@pending1.send(:trac_days)).to be 0
          end
          
          it 'should be 2 for the second pending' do
            expect(@pending2.send(:trac_days)).to be 2
          end
        end
      end

      context 'when the member has one completed, one cancelled and one pending reservation', observers: [:reservation_observer] do
        context 'when the completed reservation used up all the entitlements' do
          before do
            @completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-10', pickup: '2013-03-01')
            @completed.update_attributes(:dropoff => '2013-03-10')
            expect(@completed.claims.length).to be > 0
            expect(@completed.length).to be 10

            @cancelled = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-15', end_date: '2013-03-30', cancelled_at: '2013-03-01')
            expect(@cancelled.length).to be 16

            @pending = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-05')
            expect(@pending.length).to be 5
          end

          it 'should be 0 for the completed reservation' do
            expect(@completed.send(:trac_days)).to be 0
          end

          it 'should be 0 for the cancelled reservation' do
            expect(@cancelled.send(:trac_days)).to be 0
          end

          it 'should be 5 for the pending reservation' do
            expect(@pending.send(:trac_days)).to be 5
          end
        end

        context 'when the pending reservation is fully covered by entitlements' do
          before do
            @completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', pickup: '2013-03-01')
            @completed.update_attributes(:dropoff => '2013-03-05')
            expect(@completed.claims.length).to be > 0
            expect(@completed.length).to be 5

            @cancelled = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-15', end_date: '2013-03-30', cancelled_at: '2013-03-01')
            expect(@cancelled.length).to be 16

            @pending = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-05')
            expect(@pending.length).to be 5
          end

          it 'should be 0 for the completed reservation' do
            expect(@completed.send(:trac_days)).to be 0
          end

          it 'should be 0 for the cancelled reservation' do
            expect(@cancelled.send(:trac_days)).to be 0
          end

          it 'should be 0 for the pending reservation' do
            expect(@pending.send(:trac_days)).to be 0
          end
        end
        
        context 'when the pending reservation is partially covered by entitlments' do
          before do
            @completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', pickup: '2013-03-01')
            @completed.update_attributes(:dropoff => '2013-03-05')
            expect(@completed.claims.length).to be > 0
            expect(@completed.length).to be 5

            @cancelled = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-15', end_date: '2013-03-30', cancelled_at: '2013-03-01')
            expect(@cancelled.length).to be 16

            @pending = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-08')
            expect(@pending.length).to be 8
          end

          it 'should be 0 for the completed reservation' do
            expect(@completed.send(:trac_days)).to be 0
          end

          it 'should be 0 for the cancelled reservation' do
            expect(@cancelled.send(:trac_days)).to be 0
          end

          it 'should be 3 for the pending reservation' do
            expect(@pending.send(:trac_days)).to be 3
          end
        end
      end
    end

    describe '#set_trac_cost' do
      before do
        @member = FactoryGirl.create(:member)
      end

      context 'when creating a reservation' do
        before do
          @reservation = FactoryGirl.build(:reservation, user: @member)
          expect(@reservation.length).to be 6
        end

        context 'when the member has no existing reservations' do
          it 'should set the trac_cost to 0.00' do
            expect(@reservation.trac_cost).to eql 0.00
          end
        end

        context 'when the member has used all their entitlements with completed reservations', observers: [:reservation_observer] do
          before do
            Delorean.time_travel_to '2013-02-15'
            completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-10', pickup: '2013-03-01')
            expect(completed.length).to be 10
            completed.update_attributes(dropoff: '2013-03-10')
            expect(completed.claims.length).to be > 0
            expect(@member.credits_remaining).to be 0
          end

          it 'should cost $50 per day for the entire reservation' do
            expect(@reservation.trac_cost).to eql 300.00
          end
        end

        context 'when the reservation is covered by entitlements and Trac', observers: [:reservation_observer] do
          before do
            Delorean.time_travel_to '2013-02-15'
            completed = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-08', pickup: '2013-03-01')
            expect(completed.length).to be 8
            completed.update_attributes(dropoff: '2013-03-08')
            expect(completed.claims.length).to be > 0
            expect(@member.credits_remaining).to be 2
          end

          it 'should cost $50 per day for 3 days' do
            expect(@reservation.trac_cost).to eql 200.00
          end
        end
      end
    end
  end
end
