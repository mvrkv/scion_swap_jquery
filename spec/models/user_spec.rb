require 'spec_helper'

describe User do
  describe 'Factories' do
    it "default factory should be invalid" do
      expect(FactoryGirl.build(:user)).not_to be_valid
    end

    it 'should create a valid member' do
      FactoryGirl.create(:member)
    end

    it 'should create a valid dealer rep' do
      FactoryGirl.create(:dealer_rep)
    end

    it 'should create a valid member support' do
      FactoryGirl.create(:member_support)
    end
  end

  describe "Associations" do
    it { should have_many(:purchases)}
    it { should have_many(:reservations)}

    it { should belong_to(:dealership)}
  end

  describe "Validations" do
    # either disabled or inactive should probably be taken out
    it { pending ; should ensure_inclusion_of(:roles).in_array(["member", "dealer_rep", "member_support"]) }
    it { pending ; expect(:roles).to allow_value(member: 'pending') }
    it { should validate_uniqueness_of(:customer_ref_id)}
    it { should validate_uniqueness_of(:email)}
  end

  describe "Scopes" do
    describe "#roles" do
      before(:each) do
        @user = FactoryGirl.create(:member)
        @user2 = FactoryGirl.create(:dealer_rep)
        @user3 = FactoryGirl.create(:member_support)
        @user4 = FactoryGirl.create(:member, roles: {member: 'active', dealer_rep: 'active'})
      end

      it "should return all users" do
        expect(User.all).to match_array([@user, @user2, @user3, @user4])
      end
      it "should return users with user role 'member'" do
        expect(User.members).to match_array([@user, @user4])
      end
      it "should return users with user role 'dealer_rep'" do
        expect(User.dealer_reps).to match_array([@user2, @user4])
      end
      it "should return users with user role 'member_support'" do
        expect(User.member_supports).to match_array([@user3])
      end
    end
  end

  describe "Instance Methods" do
    before do
      @member = FactoryGirl.create(:member)
    end

    describe "#self.for_dealerships(dealerships)" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#set_default_role" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#update_role_status" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#member_dealership" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#as_json(options={})" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#confirmation_required_with_member_role?" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#credits" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#credits_remaining" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#credits_pending" do
      context 'when the user has no reservations' do
        before(:each) do
          expect(@member.reservations.size).to be 0
        end

        it "should be 0" do
          expect(@member.credits_pending).to be 0
        end
      end

      context 'when the user has one pending reservation' do
        before do
          Delorean.time_travel_to('2013-04-15')
          @reservation = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-03')
          expect(@reservation.length).to be 3
        end

        after do
          Delorean.back_to_the_present
        end

        it 'should equal length of reservation' do
          expect(@member.credits_pending).to be 3
        end
      end

      context 'when the member has more than one pending reservation' do
        before do
          Delorean.time_travel_to('2013-04-15')
          reservation1 = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-03')
          reservation2 = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-02')
          expect(reservation1.length).to be 3
          expect(reservation2.length).to be 2
        end

        after do
          Delorean.back_to_the_present
        end

        it 'should equal the sum of length of the reservations' do
          expect(@member.credits_pending).to be 5
        end
      end

      context 'when the member has completed and pending reservation' do
        before do
          Delorean.time_travel_to('2013-04-15')
          pending_res = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-03')
          completed_res = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', pickup: '2013-03-01 08:00:00', dropoff: '2013-03-05 17:00:00')

          expect(pending_res.length).to be 3
          expect(completed_res.length).to be 5
        end

        after do
          Delorean.back_to_the_present
        end

        it 'should equal the length of the pending reservation only' do
          expect(@member.credits_pending).to be 3
        end
      end

      context 'when member has a completed and a cancelled reservation' do
        before do
          completed_res = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', pickup: '2013-03-01 08:00:00', dropoff: '2013-03-05 17:00:00')
          cancelled_res = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', cancelled_at: '2013-02-28')
        end

        it 'should be 0' do
          expect(@member.credits_pending).to be 0
        end
      end

      context 'when the member has a cancelled and a pending reservation' do
        before do
          Delorean.time_travel_to('2013-04-15')
          pending_res = FactoryGirl.create(:reservation, user: @member, start_date: '2013-05-01', end_date: '2013-05-03')
          cancelled_res = FactoryGirl.create(:reservation, user: @member, start_date: '2013-03-01', end_date: '2013-03-05', cancelled_at: '2013-02-28')

          expect(pending_res.length).to be 3
          expect(cancelled_res.length).to be 5
        end

        after do
          Delorean.back_to_the_present
        end

        it 'should be the length of the pending res and not the cancelled res' do
          expect(@member.credits_pending).to be 3
        end
      end
    end

    describe "#credit_expirations" do
      it "should have a unit test" do
        pending
      end
    end
  end
end
