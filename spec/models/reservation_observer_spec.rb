require 'spec_helper'

describe ReservationObserver do

  describe "#after_create" do
    let(:mail){ double("mailer", :deliver => true) }

    it "should send a new member reservation email when a reservation is created", observers: [:reservation_observer] do
      ReservationMailer
        .should_receive(:new_member_reservation)
        .and_return(mail)
      FactoryGirl.create(:reservation)
    end

    it "should send a new dealer reservation email when a reservation is created", observers: [:reservation_observer] do
      ReservationMailer
        .should_receive(:new_dealer_reservation)
        .and_return(mail)
      FactoryGirl.create(:reservation)
    end
  end

  describe "#after_update" do
    it "should charge reservation credits if vehicle was dropped off", observers: [:reservation_observer] do
      ReservationObserver.any_instance.should_receive(:charge_reservation_credits)
      @reservation = FactoryGirl.create(:reservation)
      @reservation.update_attribute(:dropoff, Date.today)
    end

    it "should charge reservation credits if reservation was cancelled", observers: [:reservation_observer] do
      ReservationObserver.any_instance.should_receive(:charge_reservation_credits)
      @reservation = FactoryGirl.create(:reservation, :start_date => Date.today, :end_date => Date.tomorrow)
      @reservation.cancel
    end
  end
end