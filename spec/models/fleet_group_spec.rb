require 'spec_helper'

describe FleetGroup do
  it "should have a valid factory" do
    FactoryGirl.create(:fleet_group)
  end

  describe "Associations" do
    it { should have_many(:fleet_group_quantities).dependent(:destroy)}
    it { should have_many(:reservations)}

    it { should belong_to(:dealership)}
  end

  describe "Validations" do
  end

  describe "Scopes" do
  end

  describe "Instance Methods" do
    describe "#quantity" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#is_available_between" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#humanize" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#update_fleet_group_quantity" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#handle_fleet_group_quantity" do
      it "should have a unit test" do
        pending
      end
    end
  end
end
