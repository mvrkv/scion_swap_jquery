require 'spec_helper'

describe FleetGroupQuantity do
  it "should have a valid factory" do
    FactoryGirl.create(:fleet_group_quantity)
  end

  describe "Associations" do
    it { should belong_to(:fleet_group)}
  end

  describe "Validations" do
    it { should validate_presence_of(:start_date)}
    it { pending; should validate_uniqueness_of(:start_date).scoped_to(:fleet_group_id)}
    it { should validate_presence_of(:quantity)}
    it { should validate_presence_of(:fleet_group)}
  end

  describe "Scopes" do
  end
end
