require 'spec_helper'

describe UserObserver do

  describe "#after_update" do
    let(:mail){ double("mailer", :deliver => true) }

    it "should send a welcome email when user has confirmed the account", observers: [:user_observer] do
      UserMailer
        .should_receive(:welcome)
        .and_return(mail)
      @member = FactoryGirl.create(:unconfirmed_member)
      @member.update_attribute(:confirmed_at, Date.today)
    end

    it "should send a account_updated email when user has updated their account information", observers: [:user_observer] do
      UserMailer
        .should_receive(:account_updated)
        .and_return(mail)
      @member = FactoryGirl.create(:unconfirmed_member)
      @member.update_attribute(:first_name, "NewName")
    end
  end
end