require 'spec_helper'

describe Dealership do
  it "should have a valid factory" do
    FactoryGirl.create(:dealership)
  end

  describe "Associations" do
    it { should have_many(:dealer_reps)}
    it { should have_many(:fleet_groups)}
    it { should have_many(:purchases)}
    it { should have_many(:members)}
  end

  describe "Validations" do
    it { should validate_presence_of(:dealer_code)}
  end

  describe "Scopes" do
  end

  describe "Instance Methods" do
    describe "#reservations" do
      it "should have a unit test" do
        pending
      end
    end

    describe "#address" do
      it "should have a unit test" do
        pending
      end
    end
  end
end
