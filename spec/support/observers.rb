RSpec.configure do |config|
  config.before(:each) do
    # disable all observers
    ActiveRecord::Base.observers.disable :all

    # Check if example needs observers
    observers = example.metadata[:observer] || example.metadata[:observers]

    # Enable the requested observers
    if observers
      ActiveRecord::Base.observers.enable *observers
    end
  end
end
