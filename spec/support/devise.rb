module DeviseControllerMacros
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def login_as(user)
      before(:each) do
        login_as user
      end
    end
  end

  def login_as(user)
    @http_auth_was = request.env['HTTP_AUTHORIZATION']
    request.env['HTTP_AUTHORIZATION'] = "Token token='#{user.authentication_token}'"
  end
end

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.include DeviseControllerMacros, :type => :controller
end
