require 'spec_helper'

describe "Account Activation" do
  context "when confirmation token is valid" do
    it "should load the account activation page" do
      pending
      @member = FactoryGirl.create(:unconfirmed_member)
      visit "/user/confirmation?confirmation_token=#{@member.confirmation_token}"
      expect(page).to have_content("Activate Your Account")
    end
    it "should still be unconfirmed for the account" do
      pending
      @member = FactoryGirl.create(:unconfirmed_member)
      visit "/user/confirmation?confirmation_token=#{@member.confirmation_token}"
      @member.reload
      expect(@member.confirmed_at).to eq(nil)
    end
  end

  context "when confirmation token is bad" do
    it "should display an error when confirmation token is blank" do
      visit "/user/confirmation"
      expect(page).to have_content("Sorry, this link is no longer valid")
    end
    it "should display an error when confirmation token is invalid" do
      visit "/user/confirmation?confirmation_token=invalid"
      expect(page).to have_content("Sorry, this link is no longer valid")
   end
 end
end