require 'spec_helper'

describe "Account Activation Page" do
  context "when logged in as member" do
    before(:each) do
      @member = FactoryGirl.create(:unconfirmed_member)
      visit "/user/confirmation?confirmation_token=#{@member.confirmation_token}"
    end

    it "should load the account activation page" do
      pending
      expect(current_path).to eq("/users/confirmation")

      expect(page).to have_content("Activate Your Account")

      expect(page).to have_content("Survey Questions")
      expect(page).to have_content("Help us improve")
      expect(page).to have_content("How did you")
      expect(page).to have_content("Why did you")

      expect(page).to have_content("Account Details")
      expect(page).to have_content("First name")
      expect(page).to have_field("first_name")
      expect(page).to have_content("Last name")
      expect(page).to have_field("last_name")
      expect(page).to have_content("Email")
      expect(page).to have_field("email")
      expect(page).to have_content("You will use this email to sign in")
      expect(page).to have_content("Password")
      expect(page).to have_field("password")
      expect(page).to have_content("Passwords must be")
      expect(page).to have_content("Confirm Password")
      expect(page).to have_field("password_confirmation")
      expect(page).to have_checkbox()
      expect(page).to have_content("I agree to the Terms of Use")
      expect(page).to have_button("Create Account")

      expect(page).to have_content("Why you\'ll love Scion Swap")
    end
  end
end