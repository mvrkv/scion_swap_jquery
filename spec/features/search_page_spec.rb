require 'spec_helper'

describe "Search Page" do
  context "when not logged in" do
    it "should load the sign in page" do
      visit "/search"
      expect(current_path).to eq("/user/sign_in")
    end
  end

  context "when logged in as member" do
    before(:each) do
      @member = FactoryGirl.create(:member)
      @fleet_group = FactoryGirl.create(:fleet_group, dealership: @member.purchases.first.dealership)
      @fleet_group.update_attribute(:image_url, "/assets/camry.png")
      login_with(@member.email, @member.password)
      visit "/search"
    end

    it "should load the navigation bar" do
      expect(current_path).to eq("/search")
      expect(page).to have_link("Home")
      expect(page).to have_link("Account")
      expect(page).to have_link("Logout")
    end

    it "should load the body", :js => true do
      expect(page).to have_field("from_date")
      expect(page).to have_field("to_date")
      expect(page).to have_field("city")
      expect(page).to have_button("Search")

      expect(page).to have_content("days used")
      expect(page).to have_content("days remaining")

      dealership = @member.purchases.first.dealership
      expect(page).to have_content("#{dealership.name}")
      expect(page).to have_content("#{dealership.street}")
      expect(page).to have_content("#{dealership.city}")
      expect(page).to have_content("#{dealership.state}")
      expect(page).to have_content("#{dealership.zip}")

      expect(page).to have_link("Browse all nearby dealers")

      dealership.fleet_groups.each do |fg|
        expect(page).to have_content("#{fg.make}")
        expect(page).to have_content("#{fg.model}")
        expect(page).to have_content("#{fg.description}")
        expect(page).to have_button("Reserve")
        expect(page).to have_xpath("//img[@class='vehicle-image' and @src='#{fg.image_url}']")
      end
    end

    it "should list more dealerships", :js => true do
      pending
      FactoryGirl.create(:dealership, :daly_city)
      FactoryGirl.create(:dealership, :burlingame)
      FactoryGirl.create(:dealership, :san_francisco)
      FactoryGirl.create(:dealership, :san_bruno)
      click_link("Browse all nearby dealers")
      Dealership.all.each do |d|
        expect(page).to have_content("#{d.name}")
        expect(page).to have_content("#{d.street}")
        expect(page).to have_content("#{d.city}")
        expect(page).to have_content("#{d.state}")
        expect(page).to have_content("#{d.zip}")
      end
      expect(page).to have_button("Select")
      expect(page).to have_button("Cancel")
    end

    it "should load the footer" do
      expect(current_path).to eq("/search")
      expect(page).to have_link("Help")
      expect(page).to have_link("Privacy Policy")
      expect(page).to have_link("FAQ\'s")
      expect(page).to have_link("Powered by Wheelz")
    end
  end

  def login_with(email, password)
    visit '/'
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Sign in'
  end
end