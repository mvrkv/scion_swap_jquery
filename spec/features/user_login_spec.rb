require 'spec_helper'

describe "User Login" do
  context 'with active confirmed member account' do
    before(:each) do
      @member = FactoryGirl.create(:member)
    end

    it 'should login with valid email and password' do
      login_with @member.email, @member.password
      # expect(page).to have_content('Sign out')
      expect(current_path).to eq("/users/dashboard")
    end

    it 'should display an error with blank email' do
      pending
      login_with '', @member.password
      expect(page).to have_content('Sign in')
      expect(page).to have_content("ERROR")
    end

    it 'should display an error with invalid email' do
      pending
      login_with 'invalid_email', @member.password
      expect(page).to have_content('Sign in')
      expect(page).to have_content("ERROR")
    end

    it 'should display an error with with blank password' do
      pending
      login_with @member.email, ''
      expect(page).to have_content('Sign in')
      expect(page).to have_content("ERROR")
    end

    it 'should display an error with with invalid password' do
      pending
      login_with @member.email, 'invalid_password'
      expect(page).to have_content('Sign in')
      expect(page).to have_content("ERROR")
    end
  end

  context 'with unconfirmed member account' do
    before(:each) do
      @member = FactoryGirl.create(:unconfirmed_member)
    end

    it 'should display an error with valid email and password' do
      pending
      login_with @member.email, @member.password
      expect(page).to have_content('Sign in')
      expect(page).to have_content("ERROR")
    end
  end

  def login_with(email, password)
    visit '/'
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Sign in'
  end
end