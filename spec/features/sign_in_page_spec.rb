require 'spec_helper'

describe "Sign In Page" do
  before(:each) do
    visit "/"
  end

  context "when not logged in" do
    context "when loading navigation bar" do
      it "should display proper elements" do
        pending "screenshots do not show a nav bar"
        expect(page).to have_link("Home")
        expect(page).to have_link("Account")
        expect(page).to have_link("Logout")
      end
    end

    context "when loading body" do
      it "should display the proper elements" do
        # pending
        expect(current_path).to eq("/")
        expect(page).to have_content("Welcome to Scion Swap")
        expect(page).to have_field("user_email")
        expect(page).to have_field("user_password")
        expect(page).to have_button("Sign in")
        expect(page).to have_content("Forgot your password?")
        expect(page).to have_link("Forgot your password?")
        expect(page).to have_link("Didn't receive confirmation instructions?")
      end
      it "should load the password reset page when clicking the link" do
        expect(current_path).to eq("/")
        click_link("Forgot your password?")
        expect(page).to have_content("Forgot your password?")
        expect(page).to have_content("Email")
        expect(page).to have_button("Send me reset password instructions")
        expect(page).to have_field("user_email")
        expect(current_path).to eq("/user/password/new")
      end
      it "should load the user confirmation page when clicking the link" do
        pending
        expect(current_path).to eq("/")
        click_link("Didn't receive confirmation instructions?")
        expect(current_path).to eq("/user/confirmation/new")
      end
    end

    context "when loading footer" do
      it "should display the proper elements" do
        expect(current_path).to eq("/")
        expect(page).to have_link("Help")
        expect(page).to have_link("Privacy Policy")
        expect(page).to have_link("FAQ\'s")
      end
      it "should load the help page when clicking the link" do
        pending
        expect(current_path).to eq("/")
        click_link("Help")
        expect(page).to have_content("Help")
      end
      it "should load the privacy policy page when clicking the link" do
        pending
        expect(current_path).to eq("/")
        click_link("Privacy Policy")
        expect(page).to have_content("Privacy Policy")
      end
      it "should load the FAQ page when clicking the link" do
        pending
        expect(current_path).to eq("/")
        click_link("FAQ\'s")
        expect(page).to have_content("Frequently Asked Questions")
      end
    end
  end

  context "when logged in as member" do
    before(:each) do
      @member = FactoryGirl.create(:member)
      login_with(@member.email, @member.password)
    end

    it "should load the dashboard" do
      visit "/"
      expect(current_path).to eq("/users/dashboard")
    end
  end

  def login_with(email, password)
    visit '/'
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Sign in'
  end
end