require 'spec_helper'

describe "Account Information Page" do
  context "when logged in as member" do
    before(:each) do
      @member = FactoryGirl.create(:member)
      visit "/users/edit"
    end

    it "should load the account information page" do
      pending
      expect(current_path).to eq("/users/edit")

      expect(page).to have_content("Update Account")

      expect(page).to have_content("First name")
      expect(page).to have_field("first_name")
      expect(page).to have_content("Last name")
      expect(page).to have_field("last_name")
      expect(page).to have_content("Email")
      expect(page).to have_field("email")
      expect(page).to have_content("You will use this email to sign in to Scion Swap")

      expect(page).to have_link("Change Your Password")

      expect(page).to have_button("Save Changes")
      expect(page).to have_button("Cancel")
    end

    it "should load password change page" do
      pending
      expect(current_path).to eq("/users/edit")
      click_link("Change Your Password")

      expect(current_path).to eq("/user/password/edit")
      expect(page).to have_content("Change Password")
      expect(page).to have_content("Current Password")
      expect(page).to have_field("current_password")
      expect(page).to have_content("New Password")
      expect(page).to have_field("new_password")
      expect(page).to have_content("New Password")
      expect(page).to have_content("Password must be")
      expect(page).to have_content("Confirm new password")
      expect(page).to have_field("password_confirmation")

      expect(page).to have_button("Save")
      expect(page).to have_button("Cancel")
    end
  end
end