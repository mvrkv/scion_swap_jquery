require 'spec_helper'

describe "Home Page" do
  context "when not logged in" do
    it "should load the sign in page" do
      visit "/users/dashboard"
      expect(current_path).to eq("/user/sign_in")
    end
  end

  context "when logged in as member" do
    before(:each) do
      @member = FactoryGirl.create(:member)
      login_with(@member.email, @member.password)
    end

    context "when there are no previous reservations" do
      it "should load the dashboard" do
        pending
        visit "/"
        expect(current_path).to eq("/users/dashboard")
        expect(page).to have_content("Start a Scion Swap")
        expect(page).to have_content("Find a car near")
        expect(page).to have_field("dealer or location")
        expect(page).to have_content("Pickup")
        expect(page).to have_field("start_date")
        expect(page).to have_content("Drop off")
        expect(page).to have_field("end_date")
        expect(page).to have_button("Search")
        expect(page).to have_content("Not sure where to look?")
        expect(page).to have_link("Browse participating dealerships")

        expect(page).to have_content("Rewards Summary")
        # expect(page).to_not have_content("View rental history")g

        expect(page).to_not have_content("Upcoming Reservations")
      end
    end

    context "when there are upcoming reservations" do
      before(:each) do
        @reservation = FactoryGirl.create(:reservation)
      end

      it "should load the dashboard" do
        pending
        visit "/"
        expect(current_path).to eq("/users/dashboard")
        expect(page).to have_content("Start a Scion Swap")
        expect(page).to have_content("Find a car near")
        expect(page).to have_field("dealer or location")
        expect(page).to have_content("Pickup")
        expect(page).to have_field("start_date")
        expect(page).to have_content("Drop off")
        expect(page).to have_field("end_date")
        expect(page).to have_button("Search")
        expect(page).to have_content("Not sure where to look?")
        expect(page).to have_link("Browse participating dealerships")

        expect(page).to have_content("Rewards Summary")
        # expect(page).to_not have_content("View rental history")

        expect(page).to have_content("Upcoming Reservations")
      end

      context "when there are more than one upcoming reservation" do
        before(:each) do
          @reservation2 = FactoryGirl.create(:reservation)
        end

        it "should display all upcoming reservations" do
          pending
          expect(page).to have_content("Upcoming Reservations")
          expect(page).to have_content(@reservation.fleet_group.make)
          expect(page).to have_content(@reservation.fleet_group.model)
          expect(page).to have_content(@reservation2.fleet_group.make)
          expect(page).to have_content(@reservation2.fleet_group.model)
        end
      end
    end

    context "when there are past reservations" do
      before(:each) do
        @reservation = FactoryGirl.create(:past_reservation)
      end

      it "should load the dashboard" do
        pending
        visit "/"
        expect(current_path).to eq("/users/dashboard")

        expect(page).to have_content("Start a Scion Swap")
        expect(page).to have_content("Find a car near")
        expect(page).to have_field("dealer or location")
        expect(page).to have_content("Pickup")
        expect(page).to have_field("start_date")
        expect(page).to have_content("Drop off")
        expect(page).to have_field("end_date")
        expect(page).to have_button("Search")
        expect(page).to have_content("Not sure where to look?")
        expect(page).to have_link("Browse participating dealerships")

        expect(page).to have_content("Rewards Summary")
        expect(page).to have_content("View rental history")

        expect(page).to_not have_content("Upcoming Reservations")
      end
    end
  end

  def login_with(email, password)
    visit '/'
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Sign in'
  end
end