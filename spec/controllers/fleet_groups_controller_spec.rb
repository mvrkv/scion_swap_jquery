require 'spec_helper'

describe FleetGroupsController do
  let(:fleet_group_attributes)  {["created_at", "dealership_id", "description", "id", "image_url", "make", "model", "updated_at"]}

  before(:each) do
    @fleet_group = FactoryGirl.create(:fleet_group)
    login_as @fleet_group.dealership.dealer_reps.first
  end

  describe "GET #index" do
    before(:each) do
      @fleet_group2 = FactoryGirl.create(:fleet_group, dealership: @fleet_group.dealership)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        get :index, :dealership_id => @fleet_group.dealership_id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      render_views
      before(:each) do
        @member = FactoryGirl.create(:member)
        login_as @member
      end

      it "should return a listing of fleet groups" do
        get :index, :dealership_id => @fleet_group.dealership_id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@fleet_group.id, @fleet_group2.id])
        expect(obj[0].keys).to eq(fleet_group_attributes)
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        login_as @dealer_rep
      end

      it "should return a listing of fleet groups" do
        get :index, :dealership_id => @fleet_group.dealership_id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@fleet_group.id, @fleet_group2.id])
        expect(obj[0].keys).to eq(fleet_group_attributes)
      end
    end

    context "when logged in as a member support representative" do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should return a listing of fleet groups" do
        pending
        get :index, :dealership_id => @fleet_group.dealership_id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@fleet_group.id, @fleet_group2.id])
        expect(obj[0].keys).to eq(fleet_group_attributes)
      end
    end
  end

  describe "GET #show" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        get :show, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context 'when logged in as a member' do
      render_views
      before(:each) do
        @member = FactoryGirl.create(:member)
        login_as @member
      end

      it "should show information for the specified fleet group" do
        get :show, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@fleet_group.id)
        expect(obj.keys).to eq(fleet_group_attributes)
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        login_as @dealer_rep
      end

      it "should show information for the specified fleet group" do
        get :show, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@fleet_group.id)
        expect(obj.keys).to eq(fleet_group_attributes)
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should show information for the specified fleet group" do
        pending
        get :show, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@fleet_group.id)
        expect(obj.keys).to eq(fleet_group_attributes)
      end
    end
  end

  describe "PUT #update" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj).to have_key("error")
        expect(obj["error"]).to eql "You need to sign in or sign up before continuing."
      end
    end

    context "when logged in as a member" do
      before(:each) do
        @member = FactoryGirl.create(:member)
        login_as @member
      end

      it "should update the specified fleet group" do
        pending
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should update the fleet group quantity of the specified fleet group" do
        pending
        expect(@fleet_group.quantity(Date.today)).to be 5
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :fleet_group => {:fleet_group_quantity => {:start_date => Date.today.iso8601, :quantity => 10}}, :format => :json
        expect(response).to be_success
        @fleet_group.reload
        expect(@fleet_group.quantity(Date.today)).to be 10
        expect(@fleet_group.quantity(Date.tomorrow)).to be 10
        expect(@fleet_group.quantity(Date.yesterday)).to be 5
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        @dealer_rep2 = FactoryGirl.create(:dealer_rep, dealership: @dealer_rep.dealership)
        login_as @dealer_rep
      end

      it "should update the specified fleet group" do
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should update the fleet group quantity of the specified fleet group" do
        expect(@fleet_group.quantity(Date.today)).to be 5
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :fleet_group => {:fleet_group_quantity => {:start_date => Date.today.iso8601, :quantity => 10}}, :format => :json
        expect(response).to be_success
        @fleet_group.reload
        expect(@fleet_group.quantity(Date.today)).to be 10
        expect(@fleet_group.quantity(Date.tomorrow)).to be 10
        expect(@fleet_group.quantity(Date.yesterday)).to be 5
      end
    end

    context "when logged in as a member support representative" do
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should update the specified fleet group" do
        pending
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :format => :json
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should update the fleet group quantity of the specified fleet group" do
        pending
        expect(@fleet_group.quantity(Date.today)).to be 5
        put :update, :dealership_id => @fleet_group.dealership_id, :id => @fleet_group.id, :fleet_group => {:fleet_group_quantity => {:start_date => Date.today.iso8601, :quantity => 10}}, :format => :json
        expect(response).to be_success
        @fleet_group.reload
        expect(@fleet_group.quantity(Date.today)).to be 10
        expect(@fleet_group.quantity(Date.tomorrow)).to be 10
        expect(@fleet_group.quantity(Date.yesterday)).to be 5
      end
    end
  end
end
