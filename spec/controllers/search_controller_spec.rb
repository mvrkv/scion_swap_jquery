require 'spec_helper'

describe SearchController do
  describe 'GET #search-form' do
    render_views

    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        get :index, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        @member = FactoryGirl.create(:member, :confirmed)
        login_as @member
      end

      it "should return a listing of dealerships, with dealership of purchase first in list when in range", :vcr => {:cassette_name => 'SanFrancisco'} do
        pending
        @member2 = FactoryGirl.create(:member, :confirmed)
        login_as @member2
        @dealership = @member.purchases.first.dealership
        FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership)

        @dealership2 = @member2.purchases.first.dealership
        FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership2)

        get :index, :location => 'San Francisco, CA', :format => :json
        obj = JSON.parse(response.body)
        expect(obj.map{|d| d['id']}).to eq([@dealership2.id, @dealership.id])
      end

      it "should return a listing of dealerships, with closest dealership first", :vcr => {:cassette_name => 'SanFrancisco'} do
        pending
        @dealership = @member.purchases.first.dealership
        @fleet_group = FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership)

        @dealership2 = FactoryGirl.create(:dealership, :san_bruno)
        @fleet_group2 = FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership2)

        @dealership3 = FactoryGirl.create(:dealership, :daly_city)
        @fleet_group3 = FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership3)

        get :index, :location => 'San Francisco, CA', :format => :json
        obj = JSON.parse(response.body)
        expect(obj.map{|d| d['id']}).to eq([@dealership.id, @dealership3.id, @dealership2.id])
      end

      it "should return a listing of fleet groups within the dealership, in alphabetical order", :vcr => {:cassette_name => 'SanFrancisco'} do
        pending
        @dealership = @member.purchases.first.dealership
        @fleet_group = FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership, make: "Scion", model: "xA")
        @fleet_group2 = FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership, make: "Scion", model: "tC")

        get :index, :location => 'San Francisco, CA', :format => :json
        dealerships = JSON.parse(response.body)
        expect(dealerships).to eql [{'id' => @fleet_group.dealership_id, 'name' => @fleet_group.dealership.name, 'city' => 'San Francisco', 'state' => 'CA', 'street' => '151 10th St', 'zip' => '94103', 
                                 'latitude' => 37.77493, 'longitude' => -122.419415, 'fleet_groups' => [{'id' => @fleet_group2.id, 'make' => 'Scion', 'model' => 'tC', 'quantity' => 3}, {'id' => @fleet_group.id, 'make' => 'Scion', 'model' => 'xA', 'quantity' => 3}]}]
      end

      it 'should redirect to browse map when location has no fleet groups', :vcr => {:cassette_name => 'SanFrancisco'} do
        pending
        get :index, :location => 'San Francisco, CA', :format => :json
        expect(response).to redirect_to(browse_path)
      end

      it 'should redirect to browse map when there are no dealerships in range', :vcr => {:cassette_name => 'Wyoming'} do
        get :index, :location => 'Cheyenne, WY', :format => :html
        expect(response).to redirect_to(browse_path)
      end

      it 'should not return any dealerships when there are no dealerships in range', :vcr => {:cassette_name => 'Wyoming'} do
        get :index, :location => 'Cheyenne, WY', :format => :json
        expect(response.body).to eql("{}")
      end

      context 'when a fleet group exists with 3 vehicles' do
        before(:each) do
          @dealership = @member.purchases.first.dealership
          @fleet_group = FactoryGirl.create(:fleet_group, quantity: 3, dealership: @dealership)
        end

        context 'when fleet group is returned', :vcr => {:cassette_name => 'SanFrancisco'} do
          it 'should search for vehicles within 50 miles' do
            @dealership.stub(:limit => [@dealership])
            Dealership.should_receive(:near).with('San Francisco, CA').once.and_return(@dealership)
            @dealership.stub(:nearbys => []) # nearbys would otherwise call Dealership.near, skip that to guarantee above expectation
            get :index, :location => 'San Francisco, CA', :format => :json
          end

          it 'should return fleet groups within 50 miles grouped by dealership' do
            get :index, :location => 'San Francisco, CA', :format => :json
            dealerships = JSON.parse(response.body)
            expect(dealerships).to eql({'id' => @fleet_group.dealership_id, 'name' => @fleet_group.dealership.name, 'city' => 'San Francisco', 'state' => 'CA', 'street' => '151 10th St', 'zip' => '94103', 
                                     'latitude' => 37.77493, 'longitude' => -122.419415, 'fleet_groups' => [{'id' => @fleet_group.id, 'make' => 'Scion', 'model' => 'xA', 'description' => nil, 'image_url' => nil, 'quantity' => 3}],
                                     'nearbys' => []})
          end
        end

        context 'when I pass a from date', :vcr => {:cassette_name => 'SanFrancisco'} do
          before(:each) do
            @from_date = 2.days.from_now.to_date
            @miami = FactoryGirl.create(:dealership, :miami)
            @fleet_group.reservations.create(user_id: @member.id, start_date: Date.today, end_date: @from_date + 2.days)
            dealership = [@fleet_group.dealership]
            dealership.stub(:limit => dealership)
            @fleet_group.dealership.stub(:nearbys).and_return([])
          end

          it 'should return the available fleet group' do
            get :index, :location => 'San Francisco, CA', :from_date => @from_date, :format => :json
            dealerships = JSON.parse(response.body)
            expect(dealerships).to eql({'id' => @fleet_group.dealership_id, 'name' => @fleet_group.dealership.name, 'city' => 'San Francisco', 'state' => 'CA', 'street' => '151 10th St', 
                                     'zip' => '94103', 'latitude' => 37.77493, 'longitude' => -122.419415, 'fleet_groups' => [{'id' => @fleet_group.id, 'make' => 'Scion', 
                                     'model' => 'xA', 'description' => nil, 'image_url' => nil, 'quantity' => 2}], 'nearbys' => []})
          end
        end

        it 'should redirect to browse map when location has no fleets', :vcr => {:cassette_name => 'Wyoming'} do
          @miami = FactoryGirl.create(:dealership, :miami)
          FactoryGirl.create(:fleet_group, quantity: 3, dealership: @miami)
          get :index, :location => 'Cheyenne, WY', :format => :html
          expect(response).to redirect_to(browse_path)
        end

        it 'should be blank when location has no fleets', :vcr => {:cassette_name => 'Wyoming'} do
          @miami = FactoryGirl.create(:dealership, :miami)
          FactoryGirl.create(:fleet_group, quantity: 3, dealership: @miami)
          get :index, :location => 'Cheyenne, WY', :format => :json
          expect(response.body).to eql("{}")
        end
      end

    end
  end


    #Scenario: Search With Availability Dates, When Date Is Too Soon
      #Given there is a fleet group with 3 vehicles
      #Given I am logged in as a member
      #When I search for a vehicle 1 days from today
      #Then I should receive a listing of vehicles within 50 miles

    #Scenario: Search With Availability Dates, When Date Is A Holiday
      #Given there is a fleet group with 3 vehicles
      #Given I am logged in as a member
      #When I search for a vehicle on a holiday
      #Then I should receive a listing of vehicles within 50 miles

    #Scenario: Search With Availability Dates, When Date Is When Dealership Is Closed
      #Given there is a fleet group with 3 vehicles
      #Given I am logged in as a member
      #When I search for a vehicle on a day the dealership is closed
      #Then I should receive an error

end
