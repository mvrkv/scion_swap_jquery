require "spec_helper"

describe DealershipsController do

  let(:dealership_attributes)  {["id", "name", "city", "state", "street", "zip", "latitude", "longitude", "fleet_groups"]}

  it "should use DealershipsController" do
    expect(controller).to be_an_instance_of(DealershipsController)
  end

  before(:each) do
    @dealership = FactoryGirl.create(:dealership)
    @dealership2 = FactoryGirl.create(:dealership)
  end

  describe "GET #show" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        get :show, :id => @dealership.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context 'when the member is logged in' do
      before(:each) do
        @member = FactoryGirl.create(:member)
        login_as @member
      end

      it "should show information for the specified dealership" do
        pending
        get :show, :id => @dealership.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@dealership.id)
        expect(obj.keys).to eq(dealership_attributes)
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep, dealership: @dealership)
        login_as @dealer_rep
      end

      it "should show information for the specified dealership" do
        pending
        get :show, :id => @dealership.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@dealership.id)
        expect(obj.keys).to eq(dealership_attributes)
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should show information for the specified dealership" do
        get :show, :id => @dealership.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@dealership.id)
        expect(obj.keys).to eq(dealership_attributes)
      end
    end
  end

  # describe "PUT #update" do
  #   context "when not logged in" do
  #     it "should raise an unauthorized error" do
  #       # pending
  #       put :update, :id => @dealership.id, :format => :json
  #       expect(response).not_to be_success
  #       expect(response.code).to eq("401")
  #       obj = JSON.parse(response.body)
  #       expect(obj).to have_key("error")
  #       expect(obj["error"]).to eql "You need to sign in or sign up before continuing."
  #     end
  #   end

  #   context "when logged in as a member" do
  #     before(:each) do
  #       @member = FactoryGirl.create(:member)
  #       login_as @member
  #     end

  #     it "should update the specified dealership" do
  #       # pending
  #       put :update, :id => @dealership.id, :format => :json
  #       expect(response.code).to eq("204")
  #       expect(response.body).to eq(" ")
  #     end
  #   end

  #   context "when logged in as a dealer representative" do
  #     before(:each) do
  #       @dealer_rep = FactoryGirl.create(:dealer_rep)
  #       @dealer_rep2 = FactoryGirl.create(:dealer_rep, dealership: @dealer_rep.dealership)
  #       login_as @dealer_rep
  #     end

  #     it "should update the specified dealership" do
  #       # pending
  #       put :update, :id => @dealership.id, :format => :json
  #       expect(response.code).to eq("204")
  #       expect(response.body).to eq(" ")
  #     end
  #   end

  #   context "when logged in as a member support representative" do
  #     before(:each) do
  #       @member_support = FactoryGirl.create(:member_support)
  #       login_as @member_support
  #     end

  #     it "should update the specified dealership" do
  #       # pending
  #       put :update, :id => @dealership.id, :format => :json
  #       expect(response.code).to eq("204")
  #       expect(response.body).to eq(" ")
  #     end
  #   end
  # end
end

