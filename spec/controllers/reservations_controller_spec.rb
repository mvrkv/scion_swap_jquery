require "spec_helper"

describe ReservationsController do

  let(:reservation_attributes)  {["id", "start_date", "end_date", "pickup", "dropoff", "rental_agreement_id", "cancelled_at", "trac_cost", "dealership", "user", "fleet_group"]}

  it "should use ReservationsController" do
    expect(controller).to be_an_instance_of(ReservationsController)
  end

  before(:each) do
    @member = FactoryGirl.create(:member)
  end

  describe "GET #index" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @member2 = FactoryGirl.create(:member, email: 'member2_cuke@wheelz.com')
      @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation2 = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation3 = FactoryGirl.create(:reservation, user: @member2, fleet_group: @fleet_group)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        get :index, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      render_views
      before(:each) do
        login_as @member
      end

      it "should return a listing of reservations" do
        get :index, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.length).to eql @member.reservations.count
        expect(obj.map{|r| r['id']}).to eql @member.reservation_ids
      end
      it "should return a listing of reservations for the specified user" do
        pending ("QA - members should not see reservations for a different member")
        FactoryGirl.create(:past_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:cancelled_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:pending_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:active_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:future_reservation, user: @member, fleet_group: @fleet_group)
        get :index, :user_id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|r| r['id']}).to match_array(@member.reservations.map(&:id))
        expect(obj.map{|r| r['user']['id']}).to eql [@member.id] * 7
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep, dealership: @fleet_group.dealership)
        login_as @dealer_rep
      end

      it "should return a listing of reservations" do
        get :index, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@reservation.id, @reservation2.id, @reservation3.id])
        expect(obj[0].keys).to eq(reservation_attributes)
      end
      it 'should list all reservations for user with given email' do
        @member.update_attribute(:email, "member_cuke@wheelz.com")
        get :index, :member_email => 'member_cuke@wheelz.com', :format => :json
        obj = JSON.parse(response.body)
        expect(obj.length).to eql @member.reservations.count
        expect(obj.map{|r| r['id']}).to eql @member.reservation_ids
      end
      it "should return a listing of reservations for the specified user" do
        pending ("QA - members should not see reservations for a different member")
        FactoryGirl.create(:past_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:cancelled_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:pending_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:active_reservation, user: @member, fleet_group: @fleet_group)
        FactoryGirl.create(:future_reservation, user: @member, fleet_group: @fleet_group)
        get :index, :user_id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|r| r['id']}).to match_array(@member.reservations.map(&:id))
        expect(obj.map{|r| r['user']['id']}).to eql [@member.id] * 7
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should return a listing of reservations" do
        pending
        get :index, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@reservation.id, @reservation2.id, @reservation3.id])
        expect(obj[0].keys).to eq(reservation_attributes)
      end
    end
  end

  describe "GET #show" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @member2 = FactoryGirl.create(:member, email: 'cuke2@wheelz.com')
      @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation2 = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation3 = FactoryGirl.create(:reservation, user: @member2, fleet_group: @fleet_group)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        get :show, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      render_views
      before(:each) do
        login_as @member
      end

      it "should return the specified reservation" do
        get :show, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eql "200"
        obj = JSON.parse(response.body)
        expect(obj["id"]).to eql @reservation.id
        expect(obj.keys).to eql reservation_attributes
      end
      it "should raise an error when retrieving another member reservation" do
        expect { get :show, :id => @reservation3.id, :format => :json}.to raise_error ActiveRecord::RecordNotFound
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep, dealership: @fleet_group.dealership)
        login_as @dealer_rep
      end

      it "should return the specified reservation" do
        get :show, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj["id"]).to eq(@reservation.id)
        expect(obj.keys).to eq(reservation_attributes)
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should return the specified reservation" do
        pending
        get :show, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj["id"]).to eq(@reservation.id)
        expect(obj.keys).to eq(reservation_attributes)
      end
    end
  end

  describe 'GET #new' do
    before(:each) do
      login_as @member
      @fleet_group = FactoryGirl.create(:fleet_group)
    end

    context 'when member has no existing reservations' do
      it 'should have null future_cancellations' do
        expect{ get :new, :reservation => {start_date: Date.today, end_date: Date.today + 10.days, fleet_group_id: @fleet_group.id} }.not_to change(Reservation, :count)
        expect(assigns(:future_cancellations)).to be nil
      end
    end

    context 'when member has future reservations and this reservation use up all entitlments available to existing reservation' do
      before do
        @pending = FactoryGirl.create(:reservation, :start_date => Date.today + 30.days, :end_date => Date.today + 31.days, :user => @member, :fleet_group => @fleet_group)
        expect(@pending.reload.cancelled_at).to be nil
      end

      it 'should contain a future_cancellations collection' do
        expect{ get :new, :reservation => {start_date: Date.today, end_date: Date.today + 10.days, fleet_group_id: @fleet_group.id} }.not_to change(Reservation, :count)
        expect(assigns(:future_cancellations)).not_to be nil
        expect(assigns(:future_cancellations).map(&:id)).to eql [@pending.id]
      end
    end
  end

  describe "POST #create" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        post :create, :reservation => @params, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        login_as @member
        @required_params = {:fleet_group_id => @fleet_group.id, :start_date => Date.today.iso8601, :end_date => Date.tomorrow.iso8601}
      end

      context 'when params are complete' do
        render_views

        it "should create a reservation" do
          post :create, :reservation => @required_params, :format => :json
          expect(response).to be_success
          expect(response.code).to eq("200")
          obj = JSON.parse(response.body)
          expect(obj["id"]).to eq(Reservation.last.id)
          expect(obj.keys).to eq(reservation_attributes)
        end
      end
      it "should raise an error when not passing fleet_group" do
        @required_params.delete(:fleet_group_id)
        post :create, :reservation => @required_params, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("422")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["errors"])
        expect(obj["errors"]["fleet_group"]).to match_array(["can't be blank"])
      end
      it "should raise an error when not passing start_date" do
        @required_params.delete(:start_date)
        post :create, :reservation => @required_params, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("422")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["errors"])
        expect(obj["errors"]["start_date"]).to match_array(["can't be blank"])
      end
      it "should raise an error when not passing end_date" do
        @required_params.delete(:end_date)
        post :create, :reservation => @required_params, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("422")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["errors"])
        expect(obj["errors"]["end_date"]).to match_array(["can't be blank"])
      end
      it "should raise an error when not passing any parameters" do
        post :create, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("422")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["errors"])
        expect(obj["errors"]["fleet_group"]).to match_array(["can't be blank"])
        expect(obj["errors"]["start_date"]).to match_array(["can't be blank"])
        expect(obj["errors"]["end_date"]).to match_array(["can't be blank"])
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @required_params = {:fleet_group_id => @fleet_group.id, :start_date => Date.today.iso8601, :end_date => Date.tomorrow.iso8601, :user_id => @member.id}
        @dealer_rep = FactoryGirl.create(:dealer_rep, dealership: @fleet_group.dealership)
        login_as @dealer_rep
      end

      it "should create a reservation" do
        post :create, :reservation => @required_params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj["id"]).to eq(Reservation.last.id)
        expect(obj.keys).to eq(reservation_attributes)
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should create a reservation" do
        pending
        post :create, :reservation => @required_params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj["id"]).to eq(Reservation.last.id)
        expect(obj.keys).to eq(reservation_attributes)
      end
    end
  end

  describe "PUT #update" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @member2 = FactoryGirl.create(:member, email: 'cuke2@wheelz.com')
      @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation2 = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation3 = FactoryGirl.create(:reservation, user: @member2, fleet_group: @fleet_group)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        put :update, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        login_as @member
        @required_params = {:fleet_group => @fleet_group, :start_date => Date.today, :end_time => Date.tomorrow}
      end

      it "should update the specified reservation" do
        put :update, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should update the start_date for the specified reservation" do
        @params = {:start_date => Date.tomorrow}
        put :update, :id => @reservation.id, :reservation => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @reservation.reload
        expect(@reservation.start_date).to eq(Date.tomorrow)
      end
      it "should update the end_date for the specified reservation" do
        pending("does not return JSON response")
        @params = {:end_date => Date.today + 1.week}
        put :update, :id => @reservation.id, :reservation => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @reservation.reload
        expect(@reservation.start_time).to eq(Date.today + 1.week)
      end

      it "should update the reservation and do a page redirect" do
        new_start_date = (@reservation.start_date - 1.day).to_date
        put :update, :dealership_id => @fleet_group.dealership.id, :id => @reservation.id, :reservation => {:start_date => new_start_date}
        expect(response).to be_redirect
        @reservation.reload
        expect(@reservation.start_date).to eql new_start_date
      end
      it "should update fleet_group for the specified reservation and do a page redirect" do
        @new_fleet_group = FactoryGirl.create(:fleet_group, dealership: @reservation.dealership)
        put :update, :dealership_id => @fleet_group.dealership.id, :id => @reservation.id, :reservation => {:fleet_group_id => @new_fleet_group.id}
        expect(response).to be_redirect
        @reservation.reload
        expect(@reservation.fleet_group).to eql @new_fleet_group
      end
      it "should update the end_date for the specified reservation" do
        new_end_date = (Date.today + 1.week).to_date
        expect(new_end_date).not_to eql @reservation.end_date
        parms = {:end_date => new_end_date.iso8601 }
        put :update, :id => @reservation.id, :reservation => parms, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @reservation.reload
        expect(@reservation.end_date).to eq(new_end_date)
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep, dealership: @fleet_group.dealership)
        login_as @dealer_rep
      end

      it "should update the specified reservation" do
        put :update, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should update the start_date for the specified reservation" do
        @params = {:start_date => Date.tomorrow}
        put :update, :id => @reservation.id, :reservation => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @reservation.reload
        expect(@reservation.start_date).to eq(Date.tomorrow)
      end
      it "should update the end_date for the specified reservation" do
        new_end_date = Date.today + 1.week
        @params = {:end_date => new_end_date.iso8601}
        expect(new_end_date).not_to eql @reservation.end_date
        put :update, :id => @reservation.id, :reservation => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @reservation.reload
        expect(@reservation.end_date).to eq(new_end_date)
      end

      it "should update the reservation and do a page redirect" do
        new_start_date = (@reservation.start_date - 1.day).to_date
        put :update, :dealership_id => @fleet_group.dealership.id, :id => @reservation.id, :reservation => {:start_date => new_start_date}
        expect(response).to be_redirect
        @reservation.reload
        expect(@reservation.start_date).to eql new_start_date
      end
      it "should update fleet_group for the specified reservation and do a page redirect" do
        @new_fleet_group = FactoryGirl.create(:fleet_group, dealership: @reservation.dealership)
        put :update, :dealership_id => @fleet_group.dealership.id, :id => @reservation.id, :reservation => {:fleet_group_id => @new_fleet_group.id}
        expect(response).to be_redirect
        @reservation.reload
        expect(@reservation.fleet_group).to eql @new_fleet_group
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should update the specified reservation" do
        put :update, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @member2 = FactoryGirl.create(:member, email: 'cuke2@wheelz.com')
      @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation2 = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
      @reservation3 = FactoryGirl.create(:reservation, user: @member2, fleet_group: @fleet_group)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        delete :destroy, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      render_views
      before(:each) do
        login_as @member
      end

      it "should raise an error when deleting reservations" do
        pending
        expect{
          delete :destroy, :id => @reservation.id, :format => :json
        }.to change(Reservation,:count).by(0)
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("some error")
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        login_as @dealer_rep
      end

      it "should raise an error when deleting reservations" do
        pending
        expect{
          delete :destroy, :id => @reservation.id, :format => :json
        }.to change(Reservation,:count).by(0)
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("some error")
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should raise an error when deleting reservations" do
        pending
        expect{
          delete :destroy, :id => @reservation.id, :format => :json
        }.to change(Reservation,:count).by(0)
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("some error")
      end
    end
  end

  describe "PUT #start" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @dealership = @fleet_group.dealership
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        put :start, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        login_as @member
      end

      it "should raise a forbidden error" do
        pending
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        put :start, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("403")
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = @fleet_group.dealership.dealer_reps.first
        login_as @dealer_rep
      end

      it 'should start a pending reservation' do
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        expect(@reservation.pickup).to be_nil
        pre_request = Time.zone.now
        put :start, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
        @reservation.reload
        expect(@reservation.pickup).to be >= pre_request
      end

      it 'should fail to start an active reservation' do
        @reservation = FactoryGirl.create(:active_reservation, user: @member, fleet_group: @fleet_group)
        pickup_time = @reservation.pickup
        expect(@reservation.pickup).not_to be_nil
        put :start, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).to_not be_success
        expect(response.code).to eq("422")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["errors"])
        expect(obj["errors"].keys).to match_array(["start"])
        expect(obj["errors"]["start"]).to eq(["is invalid for active or complete reservations"])
        @reservation.reload
        expect(@reservation.pickup.iso8601).to eql pickup_time.iso8601
      end

      it 'should fail to start a past reservation' do
        @reservation = FactoryGirl.create(:past_reservation, user: @member, fleet_group: @fleet_group)
        dropoff_time = 30.minutes.ago
        @reservation.update_attributes(:dropoff => dropoff_time)
        expect(@reservation.pickup).not_to be_nil
        expect(@reservation.dropoff).not_to be_nil
        put :start, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).to_not be_success
        expect(response.code).to eq("422")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["errors"])
        expect(obj["errors"].keys).to match_array(["start"])
        expect(obj["errors"]["start"]).to eq(["is invalid for active or complete reservations"])
        @reservation.reload
        expect(@reservation.dropoff.iso8601).to eql dropoff_time.iso8601
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it 'should start a pending reservation' do
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        put :start, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).to_not be_success
        expect(response).to be_forbidden
      end
    end
  end

  describe "PUT #end" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @dealership = @fleet_group.dealership
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        put :end, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        login_as @member
      end

      it "should raise a forbidden error" do
        pending
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        put :end, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("403")
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = @fleet_group.dealership.dealer_reps.first
        login_as @dealer_rep
      end

      it 'should end an active reservation' do
        @reservation = FactoryGirl.create(:active_reservation, user: @member, fleet_group: @fleet_group)
        expect(@reservation.dropoff).to be_nil
        pre_request = Time.zone.now
        put :end, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).to be_success
        @reservation.reload
        expect(@reservation.dropoff).to be >= pre_request
      end

      it 'should fail to end a pending reservation' do
        @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
        expect(@reservation.pickup).to be_nil
        expect(@reservation.dropoff).to be_nil
        put :end, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        @reservation.reload
        expect(@reservation.dropoff).to be_nil
      end

      it 'should fail to end an past reservation' do
        @reservation = FactoryGirl.create(:past_reservation, user: @member, fleet_group: @fleet_group)
        dropoff_time = 30.minutes.ago
        @reservation.update_attributes(:dropoff => dropoff_time)
        expect(@reservation.pickup).not_to be_nil
        expect(@reservation.dropoff).not_to be_nil
        put :end, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).not_to be_redirect
        @reservation.reload
        expect(@reservation.dropoff.iso8601).to eql dropoff_time.iso8601
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it 'should end an active reservation' do
        @reservation = FactoryGirl.create(:active_reservation, user: @member, fleet_group: @fleet_group)
        put :end, :dealership_id => @dealership.id, :id => @reservation.id, :format => :json
        expect(response).to_not be_success
        expect(response).to be_forbidden
      end
    end
  end

  describe "PUT #cancel" do
    before(:each) do
      @fleet_group = FactoryGirl.create(:fleet_group)
      @dealership = @fleet_group.dealership
      @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group)
    end

    context "when not logged in" do
      it "should raise an unauthorized error" do
        put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        login_as @member
        @required_params = {:fleet_group => @fleet_group, :start_time => Date.today, :end_time => Date.tomorrow}
      end

      it "should cancel the specified reservation" do
        put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
        @reservation.reload
        expect(@reservation.cancelled_at).not_to be_nil
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = @fleet_group.dealership.dealer_reps.first
        login_as @dealer_rep
      end

      it 'should cancel a pending reservation' do
        expect(@reservation.cancelled_at).to be_nil
        put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
        @reservation.reload
        expect(@reservation.cancelled_at).not_to be_nil
      end
      it 'should fail to cancel an active reservation' do
        @active_reservation = FactoryGirl.create(:active_reservation, user: @member, fleet_group: @fleet_group)
        expect(@active_reservation.cancelled_at).to be_nil
        @active_reservation.update_attributes(:pickup => @active_reservation.start_date.to_time.advance(:hours => 9))
        put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @active_reservation.id, :format => :json
        expect(response).not_to be_success
        @active_reservation.reload
        expect(@active_reservation.cancelled_at).to be_nil
      end
      it 'should fail to cancel a past reservation' do
        @past_reservation = FactoryGirl.create(:past_reservation, user: @member, fleet_group: @fleet_group)
        expect(@past_reservation.cancelled_at).to be_nil
        @past_reservation.update_attributes(:pickup => @past_reservation.start_date.to_time.advance(:hours => 9), :dropoff => @past_reservation.end_date.to_time.advance(:hours => 17))
        put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @past_reservation.id, :format => :json
        expect(response).not_to be_success
        @past_reservation.reload
        expect(@past_reservation.cancelled_at).to be_nil
      end

      context 'when reservation is within 2 days' do
        it 'should only charge 1 entitlment for a 1-day rental', observers: [:reservation_observer] do
          @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group, start_date: Date.tomorrow, end_date: Date.tomorrow)
          expect(@reservation.length).to be 1
          expect(@reservation.cancelled_at).to be_nil
          put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
          expect(response).to be_success
          @reservation.reload
          expect(@reservation.claims.sum(:credits)).to eql 1
        end

        it 'should charge a penalty to the member for a rental more than 2 days', observers: [:reservation_observer] do
          @reservation = FactoryGirl.create(:reservation, user: @member, fleet_group: @fleet_group, start_date: Date.tomorrow, end_date: Date.tomorrow + 5)
          expect((@reservation.end_date - @reservation.start_date).to_i).to be >= 2
          expect(@reservation.cancelled_at).to be_nil
          put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
          expect(response).to be_success
          @reservation.reload
          expect(@reservation.claims.count).to be > 0
          expect(@reservation.claims.sum(:credits)).to eql 2
        end
      end

      context 'when reservation is at least 2 days away' do
        before(:each) do
          Delorean.time_travel_to @reservation.start_date - 3.days
        end

        after(:each) do
          Delorean.back_to_the_present
        end

        it 'should not charge a penalty to the member', observer: :reservation_observer do
          put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
          expect(response).to be_success
          @reservation.reload
          expect(@reservation.claims.sum(:credits)).to be 0
        end
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it 'should cancel a pending reservation' do
        pending
        expect(@reservation.cancelled_at).to be_nil
        put :cancel, :dealership_id => @fleet_group.dealership_id, :id => @reservation.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
        @reservation.reload
        expect(@reservation.cancelled_at).not_to be_nil
      end
    end
  end
end
