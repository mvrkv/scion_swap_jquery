# require "spec_helper"

# describe PickupLocationsController do

#   let(:pickup_location_attributes)  {[]}

#   it "should use PickupLocationsController" do
#     expect(controller).to be_an_instance_of(PickupLocationsController)
#   end

#   before(:each) do
#     @pickup_location = FactoryGirl.create(:pickup_location)
#     @pickup_location2 = FactoryGirl.create(:pickup_location)
#   end

#   describe "GET #index" do
#     context "when not logged in" do
#       it "should raise an unauthorized error" do
#         get :index, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end

#     context "when logged in as a member" do
#       before(:each) do
#         @member = FactoryGirl.create(:member)
#         login_as @member
#       end

#       it "should return a listing of members" do
#         get :index, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("200")
#         obj = JSON.parse(response.body)
#         expect(obj.map{|v| v['id']}).to match_array([@pickup_location.id, @pickup_location2.id])
#         expect(obj[0].keys).to eq(user_attributes)
#       end
#     end

#     context "when logged in as a dealer representative" do
#       before(:each) do
#         @dealer_rep = FactoryGirl.create(:dealer_rep, pickup_location: @pickup_location)
#         login_as @dealer_rep
#       end

#       it "should return a listing of pickup_locations" do
#         get :index, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("200")
#         obj = JSON.parse(response.body)
#         expect(obj.map{|v| v['id']}).to match_array([@pickup_location.id, @pickup_location2.id])
#         expect(obj[0].keys).to eq(user_attributes)
#       end
#     end
#   end

#   describe "GET #show" do
#     context "when not logged in" do
#       it "should raise an unauthorized error" do
#         get :show, :id => @pickup_location.id, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end

#     context 'when the member is logged in' do
#       before(:each) do
#         login_as @member
#       end

#       it "should show information for the specified pickup_location" do
#         get :show, :id => @pickup_location.id, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("200")
#         obj = JSON.parse(response.body)
#         expect(obj['id']).to eq(@member.id)
#         expect(obj.keys).to eq(user_attributes)
#       end
#     end

#     context "when logged in as a dealer representative" do
#       before(:each) do
#         @dealer_rep = FactoryGirl.create(:dealer_rep)
#         @dealer_rep2 = FactoryGirl.create(:dealer_rep, pickup_location: @dealer_rep.pickup_location)
#         login_as @dealer_rep
#       end

#       it "should show information for the specified pickup_location" do
#         get :show, :id => @pickup_location.id, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("200")
#         obj = JSON.parse(response.body)
#         expect(obj['id']).to eq(@member.id)
#         expect(obj.keys).to eq(user_attributes)
#       end
#     end
#   end

#   describe "POST #create" do
#   end

#   describe "PUT #update" do
#     context "when not logged in" do
#       it "should raise an unauthorized error" do
#         put :update, :id => @pickup_location.id, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end

#     context "when logged in as a member" do
#       before(:each) do
#         login_as @member
#       end

#       it "should raise an unauthorized error" do
#         put :update, :id => @pickup_location.id, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end

#     context "when logged in as a dealer representative" do
#       before(:each) do
#         @dealer_rep = FactoryGirl.create(:dealer_rep)
#         @dealer_rep2 = FactoryGirl.create(:dealer_rep, pickup_location: @dealer_rep.pickup_location)
#         login_as @dealer_rep
#       end

#       it "should update the specified member account" do
#         put :update, :id => @member.id, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("204")
#         expect(response.body).to eq(" ")
#       end
#       it "should update the first_name for the specified member" do
#         @params = {:first_name => "NewFirstName"}
#         put :update, :id => @member.id, :user => @params, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("204")
#         @member.reload
#         expect(@member.first_name).to eq("NewFirstName")
#       end
#       it "should update the last_name for the specified member" do
#         @params = {:last_name => "NewLastName"}
#         put :update, :id => @member.id, :user => @params, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("204")
#         @member.reload
#         expect(@member.last_name).to eq("NewLastName")
#       end
#       it "should update the email for the specified member" do
#         @params = {:email => "new_member_cuke@wheelz.com"}
#         put :update, :id => @member.id, :user => @params, :format => :json
#         expect(response).to be_success
#         expect(response.code).to eq("204")
#         @member.reload
#         expect(@member.email).to eq("new_member_cuke@wheelz.com")
#       end
#     end
#   end

#   describe "DELETE #destroy" do
#     context "when not logged in" do
#       it "should raise an unauthorized error" do
#         delete :destroy, :id => @member.id, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end

#     context "when logged in as a member" do
#       before(:each) do
#         login_as @member
#       end

#       it "should raise an unauthorized error" do
#         delete :destroy, :id => @member.id, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end

#     context "when logged in as a dealer representative" do
#       before(:each) do
#         @dealer_rep = FactoryGirl.create(:dealer_rep)
#         login_as @dealer_rep
#       end

#       it "should raise an unauthorized error" do
#         delete :destroy, :id => @member.id, :format => :json
#         expect(response).not_to be_success
#         expect(response.code).to eq("401")
#         obj = JSON.parse(response.body)
#         expect(obj.keys).to match_array(["error"])
#         expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
#       end
#     end
#   end
# end

