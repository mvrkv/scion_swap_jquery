require "spec_helper"

describe UsersController do

  let(:user_attributes)  {[ 'id', 'email', 'first_name', 'last_name', 'tos_accepted_at', 'customer_ref_id', 'dealership_id', 'roles', 'credits', 'creditsRemaining' ]}

  it "should use UsersController" do
    expect(controller).to be_an_instance_of(UsersController)
  end

  before(:each) do
    @member = FactoryGirl.create(:member)
    @member2 = FactoryGirl.create(:member)
  end

  describe "GET #index" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        pending
        get :index, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      render_views
      before(:each) do
        login_as @member
      end

      it "should return a listing of the current member" do
        get :index, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@member.id])
        expect(obj[0].keys).to eq(user_attributes)
      end
      it "should only return the current member account" do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        @member_support = FactoryGirl.create(:member_support)
        get :index, :format => :json
        expect(response).to be_success
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@member.id])
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        @member3 = FactoryGirl.create(:purchase, dealer_code: @dealer_rep.dealership.dealer_code).user
        login_as @dealer_rep
      end

      it "should return a listing of members and dealer reps" do
        pending
        get :index, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@member.id, @member3.id, @dealer_rep.id])
        expect(obj[0].keys).to eq(user_attributes)
      end
      it "should only return member and dealer accounts" do
        pending
        @member_support = FactoryGirl.create(:member_support)
        get :index, :format => :json
        expect(response).to be_success
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@member3.id, @dealer_rep.id])
      end
    end

    context "when logged in as a member support representative" do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should return a listing of members and member support reps" do
        get :index, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@member.id, @member2.id, @member_support.id])
        expect(obj[0].keys).to eq(user_attributes)
      end
      it "should only return member and member support accounts" do
        pending
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        get :index, :format => :json
        expect(response).to be_success
        obj = JSON.parse(response.body)
        expect(obj.map{|v| v['id']}).to match_array([@member.id, @member2.id, @member_support.id])
      end
    end
  end

  describe "GET #show" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        get :show, :id => @member.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context 'when logged in as a member' do
      render_views
      before(:each) do
        login_as @member
      end

      it "should show account information for own account" do
        get :show, :id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@member.id)
        expect(obj.keys).to eq(user_attributes)
      end
      it "should show account information for the specified member" do
        pending
        get :show, :id => @member2.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@member2.id)
        expect(obj.keys).to eq(user_attributes)
      end
      it "should raise an error when retrieving account information for a dealer rep" do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        get :show, :id => @dealer_rep.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("403")
      end
      it "should raise an error when retrieving account information for a member support rep" do
        @member_support = FactoryGirl.create(:dealer_rep)
        get :show, :id => @member_support.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("403")
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        login_as @dealer_rep
      end

      it "should show account information for the specified member" do
        get :show, :id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@member.id)
        expect(obj.keys).to eq(user_attributes)
      end
      it "should raise an error when retrieving account information for a dealer rep" do
        pending
        get :show, :id => @dealer_rep.id, :format => :json
        expect(response).to be_forbidden
      end
      it "should raise an error when retrieving account information for a member support rep" do
        pending
        @member_support = FactoryGirl.create(:dealer_rep)
        get :show, :id => @member_support.id, :format => :json
        expect(response).to be_forbidden
      end
    end

    context 'when logged in as a member support representative' do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should show account information for the specified member" do
        get :show, :id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@member.id)
        expect(obj.keys).to eq(user_attributes)
      end
      it "should show account information for the specified member support representative" do
        get :show, :id => @member_support.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@member_support.id)
        expect(obj.keys).to eq(user_attributes)
      end
      it "should show account information for the specified member support representative" do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        get :show, :id => @dealer_rep.id, :format => :json
        expect(response).to be_success
        obj = JSON.parse(response.body)
        expect(obj['id']).to eq(@dealer_rep.id)
        expect(obj.keys).to eq(user_attributes)
      end
    end
  end

  describe "POST #create" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        post :create, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj.keys).to match_array(["error"])
        expect(obj["error"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when logged in as a member" do
      before(:each) do
        @required_params = {:email => "rspec@wheelz.com", :password => "rspec_password"}
        login_as @member
      end

      it 'should not allow me to create a new user' do
        expect{ post :create, :user => @required_params, :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end

      it "should not allow me to to create a member" do
        @required_params["roles"] = {member: 'active'}
        expect { post :create, :user => @required_params, :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end

      it 'should not allow me to to create a dealer rep' do
        expect { post :create, :user => @required_params.merge(roles: {dealer_rep: 'active'}, dealership: Dealership.first), :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end

      it "should not allow me to to create a member support rep" do
        @required_params["roles"] = {member_support: 'active'}
        expect {post :create, :user => @required_params, :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end
    end

    context "when logged in as a dealer representative" do
      render_views
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        @required_params = {:email => "rspec@wheelz.com", :password => "rspec_password"}
        login_as @dealer_rep
      end

      it "should raise an error when I try to create a new user" do
        expect{ post :create, :user => @required_params, :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end
    end

    context "when logged in as a member support representative" do
      render_views
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        @required_params = {:email => "rspec@wheelz.com", :password => "rspec_password"}
        login_as @member_support
      end

      it "should not allow me to to create a user" do
        expect { post :create, :user => @required_params, :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end

      it "should not allow me to to create a member" do
        @required_params["roles"] = {member: 'active'}
        expect { post :create, :user => @required_params, :format => :json}.not_to change(User, :count)
        expect(response).to be_forbidden
      end

      it 'should create a dealer rep' do
        expect { post :create, :user => @required_params.merge(roles: {dealer_rep: 'active'}, dealership: Dealership.first), :format => :json}.to change(User, :count).by(1)
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.keys).to eq(user_attributes)
      end

      it "should create a member support rep" do
        @required_params["roles"] = {member_support: 'active'}
        expect {post :create, :user => @required_params, :format => :json}.to change(User,:count).by(1)
        expect(response).to be_success
        expect(response.code).to eq("200")
        obj = JSON.parse(response.body)
        expect(obj.keys).to eq(user_attributes)
      end
    end
  end

  describe "PUT #update" do
    context "when not logged in" do
      it "should raise an unauthorized error" do
        put :update, :id => @member.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("401")
        obj = JSON.parse(response.body)
        expect(obj).to have_key("error")
        expect(obj["error"]).to eql "You need to sign in or sign up before continuing."
      end
    end

    context "when logged in as a member" do
      before(:each) do
        login_as @member
      end

      it "should update own member account" do
        put :update, :id => @member.id, :format => :json
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should update the first_name for own member account" do
        @params = {:first_name => "NewFirstName"}
        put :update, :id => @member.id, :user => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @member.reload
        expect(@member.first_name).to eq("NewFirstName")
      end
      it "should update the last_name for own member account" do
        @params = {:last_name => "NewLastName"}
        put :update, :id => @member.id, :user => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @member.reload
        expect(@member.last_name).to eq("NewLastName")
      end
      it "should update the email for own member account" do
        @params = {:email => "new_member_cuke@wheelz.com"}
        put :update, :id => @member.id, :user => @params, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        @member.reload
        expect(@member.email).to eq("new_member_cuke@wheelz.com")
      end
      it "should raise an error when updating a different member's account" do
        @member2 = FactoryGirl.create(:member)
        put :update, :id => @member2.id, :format => :json
        expect(response).not_to be_success
        expect(response).to be_forbidden
      end
    end

    context "when logged in as a dealer representative" do
      before(:each) do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        @dealer_rep2 = FactoryGirl.create(:dealer_rep, dealership: @dealer_rep.dealership)
        login_as @dealer_rep
      end

      it "should update the specified member account" do
        put :update, :id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should raise an error when updating the specified dealer rep account" do
        pending
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        put :update, :id => @dealer_rep.id, :format => :json
        expect(response).not_to be_success
        expect(response).to be_forbidden
      end
      it "should raise an error when updating the specified member support rep account" do
        @member_support = FactoryGirl.create(:member_support)
        put :update, :id => @member_support.id, :format => :json
        expect(response).not_to be_success
        expect(response.code).to eq("403")
        expect(response.body).to be_blank
      end
    end

    context "when logged in as a member support representative" do
      before(:each) do
        @member_support = FactoryGirl.create(:member_support)
        login_as @member_support
      end

      it "should update the specified member account" do
        put :update, :id => @member.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
      it "should raise an error when updating the specified dealer rep account" do
        @dealer_rep = FactoryGirl.create(:dealer_rep)
        put :update, :id => @dealer_rep.id, :format => :json
        expect(response).to be_success
      end
      it "should update the specified member support rep account" do
        @member_support = FactoryGirl.create(:member_support)
        put :update, :id => @member_support.id, :format => :json
        expect(response).to be_success
        expect(response.code).to eq("204")
        expect(response.body).to eq(" ")
      end
    end
  end

  describe "DELETE #destroy" do
    # no tests needed because route does not exist
  end
end

