Feature: Account Activation
  As a new member
  In order to access the service
  I want to activate my account through the website

  Scenario: Member Activation
    Given I am logged in as a member
    When I goto the account activation page
    And I enter the confirmation token
    Then the account should be activated

  Scenario: Standard Activation
    Given I have a user account
    When I activate my account
    Then the user account status should be "Active"
    Then the user account should have an existing rental balance of 10 days
    Then the user account rentals should expire 365 days from membership start date
    Then a welcome email should be sent to the new member
    When I access the activation URL
    Then the activation URL should no longer be valid
