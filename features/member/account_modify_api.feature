Feature: Account Modify
  As a member
  In order to manage my account
  I want to update my account using a REST API

  Background:
    Given I use the API

  Scenario: Update
    Given I am logged in as a member
    When I update my account
    Then the account should be updated
    Then an account update email should be sent to the user

  Scenario: Update First Name
    Given I am logged in as a member with
      | first_name | Tom |
    When I update my account with
      | first_name | Joe |
    Then the account should be updated with
      | first_name | Joe |

  Scenario: Update Last Name
    Given I am logged in as a member with
      | last_name | Jones |
    When I update my account with
      | last_name | Smith |
    Then the account should be updated with 
      | last_name | Smith |

  Scenario: Update Email
    Given I am logged in as a member with
      | email  | cuke@wheelz.com |
    When I update my account with
      | email | cuke2@wheelz.com |
    Then the account should be updated with
      | email | cuke2@wheelz.com |
