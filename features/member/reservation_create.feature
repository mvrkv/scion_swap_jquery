Feature: Vehicle Reservation
  As a member
  In order to rent a vehicle
  I want to make the reservation through the website

  @vcr
  Scenario: Standard Reservation
    Given I am logged in as a member with
      | entitlements | 1 |
    And A vehicle exists in San Francisco with
      | quantity |
      | 1        |
    When I reserve a vehicle for 1 day
    Then my entitlements are held in pending
    Then the vehicle is no longer available for rent
    Then a reservation confirmation email should be sent to the member
    Then a reservation notification email should be sent to the dealership

  @vcr
  Scenario: Reservation When Reservation Duration Is Longer Than Balance
    Given I am logged in as a member with
      | entitlements | 0 |
    And A vehicle exists in San Francisco with
      | quantity |
      | 1        |
    When I reserve a vehicle for 1 day
    Then the vehicle is no longer available for rent
    Then a reservation confirmation email should be sent to the member
    Then user will be notified that there is an unsettled pending balance
    Then a reservation notification email should be sent to the dealership
    Then dealership will be notified that there is an unsettled pending balance
