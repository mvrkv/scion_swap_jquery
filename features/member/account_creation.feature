Feature: Account Creation
  As a new member
  In order to join the service
  I want to create a new account

  Scenario: Standard Creation
    When I create a new member account
    Then the member account created date should be today
    Then the member account status should be "Pending Activation"
    Then an activation email should be sent to the new member	
