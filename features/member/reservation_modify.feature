Feature: Modify Reservation
  As a member
  In order to change my reservation
  I want to make modifications to my reservation through the website

  Scenario: Navigate to Reservation Edit Page
    Given I am logged in as a member
    Given I have a pending reservation
    When I goto the reservations page
    And I select a reservation
    Then I should be on the reservation edit page

  Scenario: Modify Reservation
    Given I am logged in a member
    Given I have a pending reservation
    When I goto the reservation edit page
    And I change the start date of the reservation
    And I change the end date of the reservation
    Then the start date of the reservation should be changed
    Then the end date of the reservation should be changed

  Scenario: Extend Reservation
    Given I am logged in a member
    Given I have a active reservation
    When I goto the reservation edit page
    And I change the extend the reservation
    Then the end date of the reservation should be changed

