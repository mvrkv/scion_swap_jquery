Feature: Like A Vehicle
  As a member
  In order remember what vehicle I like
  I want to select the vehicle I like through the website

  Scenario: Select A Vehicle And Like It
    Given I am logged in as a member
    And A vehicle exists in San Francisco with
      | quantity |
      | 1        |
    Given I goto the vehicle page
    When I select to like the vehicle
    Then it should keep track of vehicles that I like
