Feature: Account Login
  As a member
  In order to access my account
  I want to login to the service through the website

  Scenario: Normal Login
    Given a member exists
    When I goto the login page
    And I enter email and password
    Then I should be logged in
    And I should be redirected to another page

  Scenario: Login With Zero Balance And No Active Reservations
    Given a member exists with
      | entitlements | 0 |
    When I am logged in as said member
    Then I should receive a Balance Expiration message

  Scenario: Login With Expired Balance
    Given a member exists
    And I have a balance that has expired
    When I am logged in as said member
    Then I should receive a Balance Expiration message

  Scenario: Reset Password
    Given a member exists
    When I goto the login page
    And I select "Password Reset"
    Then an email should be sent to reset the password
