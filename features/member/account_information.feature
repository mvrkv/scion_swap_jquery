Feature: Account Information
  As a member
  In order to view information for my account
  I want to see account information though the website

  Scenario: Show Entitlements Balance
    Given I am logged in as a member
    When I goto the account information page
    Then I should see an entitlements balance
