Feature: Like A Vehicle
  As a member
  In order remember what vehicle I like
  I want to keep track of what I like

  Background:
    Given I use the API

  Scenario: Select A Vehicle And Like It
    Given I am logged in as a member with
      | entitlements | 1 |
    And A vehicle exists in San Francisco with
      | quantity |
      | 1        |
    When I select to like the vehicle
    Then it should keep track of vehicles that I like
