Feature: Account Reversal
  As Toyota Corporate Care
  In order to deactivate a member account
  I want to reverse the account

  Scenario: With One VIN
    Given I have a member account
    And it has a VIN associated with it
    When I reverse the account
    Then the account should be deactivated
    And the account status should be "Disabled"

  Scenario: With Multiple VINs
    Given I have a member account
    And it has a VIN associated with it
    And it has a VIN associated with it
    When I reverse the account
    Then the account should not be deactivated
    And the account status should be "Active"
    And entitlements associated with the reversed VIN should be zero

