Feature: Account Information
  As a support rep
  In order to view information for member account
  I want to see account information though the website

  Scenario: Show Entitlements Balance
    Given I am logged in as a support rep
    Given I goto the member information page
    Then I should see an entitlements balance
