Feature: Modify Reservation
  As a support rep
  In order to change member reservations
  I want to make modifications to member reservation through the website

  Scenario: Navigate to Reservation Edit Page
    Given I am logged in as a support rep
    Given there is a member with a pending reservation
    When I goto the reservations page
    And I select a reservation
    Then I should be on the reservation edit page

  Scenario: Modify Reservation
    Given I am logged in as a support rep
    Given there is a member with a pending reservation
    When I goto the reservation edit page
    And I change the start date of the reservation
    And I change the end date of the reservation
    Then the start date of the reservation should be changed
    Then the end date of the reservation should be changed

  Scenario: Extend Reservation
    Given I am logged in as a support rep
    Given there is a member with a active reservation
    When I goto the reservation edit page
    And I change the extend the reservation
    Then the end date of the reservation should be changed

