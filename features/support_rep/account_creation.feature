Feature: Account Creation
  As a support rep
  In order to allow dealer reps to manage the service
  I want to create a new dealer rep account

  Scenario: Standard Creation
    Given I am logged in as a support rep
    Given I goto the dealer rep account creation page
    When I enter all the proper information
    Then there should be a dealer rep account created
