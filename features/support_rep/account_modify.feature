Feature: Account Modify
  As a support rep
  In order to manage member accounts
  I want to update member accounts through the website

  Scenario: Update Member First Name
    Given I am logged in as a support rep
    Given I goto the member information page
    When I update the the first name of the account
    Then the first name of the account should be updated

  Scenario: Update Member Last Name
    Given I am logged in as a support rep
    Given I goto the member information page
    When I update the the last name of the account
    Then the last name of the account should be updated

  Scenario: Update Member Email
    Given I am logged in as a support rep
    Given I goto the member information page
    When I update the the email of the account
    Then the email of the account should be updated

  Scenario: Reset Password
    Given I am logged in as a support rep
    Given I goto the member information page
    And I select "Password Reset"
    Then an email should be sent to reset the password

  Scenario: Send Activation Email
    Given I am logged in as a support rep
    Given I goto the member information page
    And I select "Send Activation Email"
    Then an email should be sent to activate the account
