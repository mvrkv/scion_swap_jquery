Feature: Account Balance Adjustment
  As Toyota IS
  In order to adjust member account balances
  I want to debit entitlements

  Scenario: Account Balance Adjustment
    Given I have a member account
    When I adjust the member account balance
    Then the entitlements should have changed
