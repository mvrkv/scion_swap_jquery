Feature: Reservation Claims Submission
  As Scion Swap
  In order for Toyota to facilitate the existing claims process with a dealership
  I want submit a transaction

  Scenario: Reservation Claims Submission
    Given I have a member reservation
    When I submit the claim to Toyota
    Then there should be a successful response

  Scenario: Reject Due To Bad Member VIN
    Given I have a member reservation
    Given the member VIN is bad
    When I submit the claim to Toyota
    Then the submission should be rejected

  Scenario: Reject Due Entitlements Exceed Balance
    Given I have a member reservation
    Given the number of entitlements for the member exceeds the balance
    When I submit the claim to Toyota
    Then the submission should be rejected

  Scenario: Reject Due To Entitlements Do Not Equal The Reservation Days
    Given I have a member reservation
    Given the number of entitlements do not equal the reservation days
    When I submit the claim to Toyota
    Then the submission should be rejected
