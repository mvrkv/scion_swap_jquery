Feature: Account Merge
  As Scion Member Engagement
  In order to merge duplicate member accounts
  I want to combine the accounts

  Scenario: Account Merge
    Given I have a member account
    Given I have a member account
    When I merge the two member accounts
    Then there should only be one account left
