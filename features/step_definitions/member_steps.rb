Before do
  step "I use the web"
end

When /^I create a new member account$/ do
  @member = FactoryGirl.create(:unconfirmed_member)
end

When /^I am logged in as a (#{User::ROLES.join('|')})(?:\swith)?$/ do |user_type, *table|
  step "a #{user_type} exists with", *table
  step "I am logged in as said #{user_type}"
end

Given /^a (#{User::ROLES.join('|')}) exists(?:\swith)?$/ do |user_type, *table|
  @password ||= 'secret'
  if table.blank?
    instance_variable_set "@#{user_type}", FactoryGirl.create(user_type, password: @password, roles: {"#{user_type}" => 'active'}, confirmed_at: Time.now)
  else
    instance_variable_set "@#{user_type}", FactoryGirl.create(user_type, table[0].rows_hash.reverse_merge(password: @password, roles: {"#{user_type}" => 'active'}, confirmed_at: Time.now))
  end
end

Given /^I have a balance that has expired$/ do
  @member.purchases.update_all(:purchased_date => Date.today - 1.year)
end

Given /^I am logged in as said (#{User::ROLES.join('|')})$/ do |user_type|
  user = instance_variable_get "@#{user_type}"
  assert user.present?, "#{instance_variable_names}: #{@member.inspect} #{@dealer_rep.inspect}"
  assert @password.present?
  if @api
    step "I send a POST request to \"#{user_auth_token_path}\" with the following:", {member: {email: user.email, password: @password}}.to_json
    obj = JSON.parse(page.driver.response.body)
    obj.should eql({'id' => user.id, 'authentication_token' => user.authentication_token})
    @auth_token = obj['authentication_token']
    step "I send auth token in headers"
  else
    old_page = page.body
    visit new_user_session_path
    fill_in "user_email", :with => user.email
    fill_in "user_password", :with => @password
    click_button "Sign in"
  end
end

Then /^the member account created date should be today$/ do
  @member.should_not be_nil
  @member.created_at.to_date.should eql Date.today
end

Then /^the member account status should be "Pending Activation"$/ do
  @member.member_status.should eql 'pending'
end

Then /^an activation email should be sent to the new member$/ do
  ActionMailer::Base.deliveries.should_not be_empty
  activation_email = ActionMailer::Base.deliveries.first
  ActionMailer::Base.deliveries.delete(activation_email)

  activation_email.to.should eql [@member.email]
  activation_email.subject.should eql 'Please activate your Scion Swap account'
end

Then /^an account update email should be sent to the user$/ do
  ActionMailer::Base.deliveries.should_not be_empty
  activation_email = ActionMailer::Base.deliveries.first
  ActionMailer::Base.deliveries.delete(activation_email)

  activation_email.to.should eql [@member.email]
  activation_email.subject.should eql 'Your account has been updated'
end

Given /^I have a user account$/ do
  @member = FactoryGirl.create(:unconfirmed_member)
end

When /^I activate my account$/ do
  ActionMailer::Base.deliveries.should_not be_empty
  activation_email = ActionMailer::Base.deliveries.first
  activation_match = activation_email.body.match /<a href=".*?(#{user_confirmation_path}.*)">/
  activation_match.should_not be_nil
  ActionMailer::Base.deliveries.delete(activation_email)
  ActionMailer::Base.deliveries.should be_empty
  @confirmation_token = activation_match[1].match(/confirmation_token=(.*)(&|$)/)[1]
  step "I use the web"
  visit activation_match[1]
  @member.reload.should be_confirmed
end

Then /^the user account status should be "Active"$/ do
  @member.member_status.should eql 'active'
end

Then /^the user account should have an existing rental balance of 10 days$/ do
  @member.credits.should be 10
end

Then /^the user account rentals should expire 365 days from membership start date$/ do
  expiration_hash = { 365.days.since(@member.created_at.to_date) => 10 }
  @member.credit_expirations.should eql expiration_hash
end

Then /^a welcome email should be sent to the new member$/ do
  ActionMailer::Base.deliveries.should_not be_empty
  activation_email = ActionMailer::Base.deliveries.last
  ActionMailer::Base.deliveries.delete(activation_email)

  activation_email.to.should eql [@member.email]
  activation_email.subject.should eql "Welcome to Scion Swap!"
end

When /^I access the activation URL$/ do
  member = User.members.last
  member.should be_confirmed
  visit user_confirmation_path(:confirmation_token => @confirmation_token)
end

Then /^the activation URL should no longer be valid$/ do
  page.body.should include "Confirmation token is invalid"
end

When /^I use the API$/ do
  @api = true
  step "I send and accept JSON"
  step "I send auth token in headers"
end

Then /^I send auth token in headers$/ do
  if @auth_token
    page.driver.header 'Authorization', "Token token='#{@auth_token}'"
  end
end

When /^I use the web$/ do
  @api = false
  step "I send and accept HTML"
end

When /^I send and accept HTML$/ do
  page.driver.header 'Accept', 'text/html'
  page.driver.header 'Content-Type', "application/x-www-form-urlencoded"
end

When /^I update my account(?:\swith)?$/ do |*table|
  if @api
    table_hash = table.present? ? table[0].rows_hash.to_json : {first_name: Faker::Name.first_name}.to_json
    step "I send a PUT request to \"#{user_path(:id => @member.id)}\"", table_hash
  else
    pending
    visit edit_users_path
    if table.blank?
      fill_in 'member_first_name', :with => Faker::Name.first_name
    else
      table[0].rows_hash.each do |field, value|
        fill_in "member_#{field}", :with => value
      end
    end
    click_button 'Save'
  end
end

Then /^the account should be updated(?:\swith)?$/ do |*table|
  if @api
    step "the response status should be \"204\""
    unless table.blank?
      step "I send a GET request to \"#{user_path(:id => @member.id)}\""
      table[0].rows_hash.each do |field, value|
        step "the JSON response should have \"#{field}\" with the text \"#{value}\""
      end
    end
  else
    @member.reload
    unless table.blank?
      table[0].rows_hash.each do |field, value|
        @member.send(field).should eql value
      end
    end
  end
end

Then /^I should receive a Balance Expiration message$/ do
  page.body.should include "Your balance has expired"
end
