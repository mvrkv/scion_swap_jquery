When /^A vehicle exists in (.+?)(?:\swith)?$/ do |city_name, *table|
  step "A Dealership exists in #{city_name}"
  step "That dealership has a dealer representative"
  step "that dealership has a fleet group with", *table
end

When /^A Dealership exists(?: in (.+))?$/ do |city_name|
  dealer_options = []
  if city_name.present?
    city, state = city_name.split(',')
    if (city_trait=FactoryGirl.factory_by_name('dealership').defined_traits.detect{|t| t.name.to_s.downcase == city.to_s.downcase})
      dealer_options = [city_trait.name]
    else
      dealer_options << {}
      dealer_options[0][:city]  = city  unless city.blank?
      dealer_options[0][:state] = state unless state.blank?
    end
  end
  unless (@dealership = Dealership.where(:city => city).first)
    @dealership = FactoryGirl.create(:dealership, *dealer_options)
  end
end

When /^there is a fleet group(?: of make "([^"]*)" and model "([^"]*)")?(?: with (\d+) vehicles)?$/ do |make, model, quantity|
  step "A Dealership exists"
  fields, values = [],[]
  fields << 'make'     and values << make     if make.present?
  fields << 'model'    and values << model    if model.present?
  fields << 'quantity' and values << quantity if quantity.present?
  step "That dealership has a fleet group with", table([
    fields,
    values
  ])
end

When /^that dealership has a fleet group(?:\swith)?$/ do |*table|
  if table.blank?
    FactoryGirl.create(:fleet_group, dealership: @dealership)
  else
    table[0].hashes.each do |hash|
      FactoryGirl.create(:fleet_group, {dealership: @dealership}.merge(hash))
    end
  end
end

When /^That dealership has a dealer representative$/ do
  @rep = FactoryGirl.create(:dealer_rep, dealership: @dealership)
end

When /^I reserve a vehicle for (\d+) days?$/ do |num_days|
  pending("Requires JS to render angular templates")
  if FleetGroup.count == 0
    step "A vehicle exists in San Francisco"
  else
    @dealership ||= FleetGroup.last.dealership
  end
  from_date = 5.days.from_now
  step "I use the web"
  step "I search for a vehicle in \"#{@dealership.city}, #{@dealership.state}\" between dates \"#{from_date}\" and \"#{num_days.to_i.days.since(from_date)}\""
  click_button "Reserve"
  page.body.should have_content "Reserve your car"
  click_button 'Complete Reservation'
  page.body.should have_content "Your reservation"
  @reservation = Reservation.order('created_at desc').first
end

Then /^my entitlements are held in pending$/ do
  @reservation.should be_present
  visit dashboard_users_path
  within('#rewards-summary #unavailable') do
    page.should have_content "#{@reservation.length} pending"
  end
end

Then /^the vehicle is no longer available for rent$/ do
  @reservation.fleet_group.availability_between(@reservation.start_date, @reservation.end_date).values.min.should be 0
end

Then /^a reservation confirmation email should be sent to the member$/ do
  @member_reservation_email ||= ActionMailer::Base.deliveries.detect{|em| em.to.include?(@reservation.user.email) && em.subject.start_with?("Your reservation")}
  ActionMailer::Base.deliveries.delete(@member_reservation_email)

  @member_reservation_email.to.should eql [@reservation.user.email]
  @member_reservation_email.subject.should eql "Your reservation for a #{@reservation.fleet_group.humanize} on #{@reservation.start_date}"
end

Then /^a reservation notification email should be sent to the dealership$/ do
  @dealer_reservation_email ||= ActionMailer::Base.deliveries.detect{|em| em.to.include?(@reservation.fleet_group.dealership.dealer_reps.first.email) && em.subject.start_with?("A new reservation")}
  ActionMailer::Base.deliveries.delete(@dealer_reservation_email)

  @dealer_reservation_email.should_not be_nil
  @dealer_reservation_email.to.should eql @dealership.dealer_reps.map(&:email)
  @dealer_reservation_email.subject.should eql "A new reservation has been made on #{@reservation.fleet_group.humanize}"
end

Then /^user will be notified that there is an unsettled pending balance$/ do
  @member_reservation_email ||= ActionMailer::Base.deliveries.detect{|em| em.to.include?(@reservation.user.email) && em.subject.start_with?("Your reservation")}
  ActionMailer::Base.deliveries.delete(@member_reservation_email)

  @member_reservation_email.should_not be_nil
  @member_reservation_email.to.should eql [@reservation.user.email]
  @member_reservation_email.subject.should eql "Your reservation for a #{@reservation.fleet_group.humanize} on #{@reservation.start_date}"
  @member_reservation_email.body.should have_content "additional $15 is due at time of pickup"
end

Then /^dealership will be notified that there is an unsettled pending balance$/ do
  @dealer_reservation_email ||= ActionMailer::Base.deliveries.detect{|em| em.to.include?(@reservation.fleet_group.dealership.dealer_reps.first.email) && em.subject.start_with?("A new reservation")}
  ActionMailer::Base.deliveries.delete(@dealer_reservation_email)

  @dealer_reservation_email.should_not be_nil
  @dealer_reservation_email.to.should eql @dealership.dealer_reps.map(&:email)
  @dealer_reservation_email.subject.should eql "A new reservation has been made on #{@reservation.fleet_group.humanize}"
  @dealer_reservation_email.body.should have_content "additional $15 is due at time of pickup"
end
