When /^I search for a vehicle(?: in "([^"]*)")?(?: between dates "([^"]*)" and "([^"]*)")?$/ do |location, from_date, to_date|
  @last_vehicle_search_location = location
  if @api
    step "I send a GET request to \"#{search_path}\" with the following:", {location: location, from_date: from_date, to_date: to_date}.to_json
  else
    visit search_path(:location => location, :from_date => from_date, :to_date => to_date)
  end
end
