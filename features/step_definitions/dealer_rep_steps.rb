When /^I list all reservations$/ do
  step "I send a GET request to \"/dealerships/#{@dealer_rep.dealership_id}/reservations\""
end

Then /^I should receive a list of (\d+|all) reservations$/ do |res_quant|
  res_quant = res_quant =~ /\d+/ ? res_quant.to_i : @dealer_rep.dealership.reservations.count
  response_reservations = JSON.parse(page.driver.response.body)
  response_reservations.length.should == res_quant
end
