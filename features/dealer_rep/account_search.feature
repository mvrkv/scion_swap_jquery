Feature: Account Search
  As a dealer rep
  In order find members
  I want to search for members through the website

  Scenario: Search For Member
    Given I am logged in as a dealer rep
    Given I goto the member search page
    When I enter a name
    Then I should get a listing of members that are a result of the search
    When I select an account from the list
    Then I should be redirected to the member information page

