Feature: Account Modify
  As a dealer rep
  In order to manage member accounts
  I want to update member accounts through the website

  Scenario: Update Member First Name
    Given I am logged in as a dealer rep
    Given I goto the member information page
    When I update the the first name of the account
    Then the first name of the account should be updated

  Scenario: Update Member Last Name
    Given I am logged in as a dealer rep
    Given I goto the member information page
    When I update the the last name of the account
    Then the last name of the account should be updated

  Scenario: Update Member Email
    Given I am logged in as a dealer rep
    Given I goto the member information page
    When I update the the email of the account
    Then the email of the account should be updated
