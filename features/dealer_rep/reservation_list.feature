Feature: List Reservations
  As a dealer rep
  In order to view reservations
  I want to list reservations through the website

  Scenario: List Past Reservations
    Given I am logged in as a dealer rep
    And there are past reservations
    When I goto the reservations page
    Then I should see a list of past reservations

  Scenario: List Pending Reservations
    Given I am logged in as a dealer rep
    And there are pending reservations
    When I goto the reservations page
    Then I should see a list of pending reservations

  Scenario: List Active Reservations
    Given I am logged in as a dealer rep
    And there are active reservations
    When I goto the reservations page
    Then I should see a list of active reservations