Feature: Account Login
  As a dealer rep
  In order to access the service
  I want to login to the service through the website

  Scenario: Normal Login
    Given a dealer rep exists
    When I goto the login page
    And I enter email and password
    Then I should be logged in
    And I should be redirected to another page