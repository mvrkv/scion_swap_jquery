Feature: Dealership Modify
  As a dealer rep
  In order to change dealership availability
  I want to modify dealership availability through the website

  Scenario: Modify Business Hours
    Given I am logged in as a dealer rep
    Given I goto the dealership information page
    When I enter new business hours
    Then the business hours should be changed

  Scenario: Modify Business Days
    Given I am logged in as a dealer rep
    Given I goto the dealership information page
    When I enter new business days
    Then the business days should be changed

  Scenario: Modify Holiday Days
    Given I am logged in as a dealer rep
    Given I goto the dealership information page
    When I enter new holiday days
    Then the holiday days should be changed