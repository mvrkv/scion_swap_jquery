Feature: Fleet Group Quantity Modify
  As a dealer rep
  In order keep track of the fleet group
  I want to modify the quantity available and unavailable through the website

  Scenario: Standard Modify
    Given I am logged in as a dealer rep
    Given I goto the fleet group information page
    When I enter new available and unavailable quantities
    Then the available and unavailable quantities should change

