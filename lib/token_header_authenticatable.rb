module Devise
  module Strategies
    class TokenHeaderAuthenticatable < Authenticatable
      def valid?
        token_value.present?
      end

      def authenticate!
        resource_scope = mapping.to
        resource = resource_scope.find_for_token_authentication(auth_token: token_value)

        if validate(resource)
          resource.after_token_authentication
          success!(resource)
        else
          fail
        end
      end

      private
      def token_value
        if header && header =~ /^Token token=(?<quote>['"])(?<token>.+)\k<quote>$/
          $~['token']
        end
      end

      def header
        request && request.authorization
      end
    end
  end
end

Warden::Strategies.add(:token_authenticatable, Devise::Strategies::TokenHeaderAuthenticatable)
