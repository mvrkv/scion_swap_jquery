class ImportSwapTransaction < SwapDataTransfer
  TRANSACTION_TYPE_COL = 0
  CUSTOMER_ID_COL = 1
  FIRST_NAME_COL = 2
  LAST_NAME_COL = 3
  ORGANIZATION_COL = 4
  EMAIL_COL = 5
  VIN_COL = 6
  DOFU_COL = 7
  DEALER_CODE_COL = 8

  
def self.import(date=Date.today)
    date_part = if date.is_a? String
      date
    elsif date.respond_to? :strftime
      date.strftime '%Y%m%d'
    else
      raise ArgumentError, "date parameter must be a Date or String"
    end
    transaction_options = Rails.application.config.ftp_data_exchange[:transactions]
    transaction_filename = "scion_swap_trans_#{date_part}.dat"
    transaction_filename << '.Z' if transaction_options[:compression]
    transaction_filename << '.gpg' if transaction_options[:encryption]
    transaction_file = fetch_file(transaction_filename, transaction_options[:url], transaction_options.merge(:remote_path => transaction_options[:path]))
    #transaction_file = Rails.root.join('public', 'scion_swap_trans.dat')


    log_message("Scion Transaction Import (#{Time.zone.now}): No transactions to import on #{date}") and return if transaction_file.blank?
    transactions = read_file(transaction_file, transaction_options).split("\n")

    transaction_errors = []
    transactions.each do |transaction_record|
      fields = transaction_record.split('|')
      if fields[TRANSACTION_TYPE_COL].to_s.downcase == 'purchase'
        user = User.find_or_initialize_by_customer_ref_id(fields[CUSTOMER_ID_COL])
        user.first_name = fields[FIRST_NAME_COL]
        user.last_name = fields[LAST_NAME_COL]
        user.email = fields[EMAIL_COL]
        if user.first_name.blank? && user.last_name.blank?
          user.first_name = fields[ORGANIZATION_COL]
        elsif user.last_name.blank? && user.first_name.present?
          user.last_name = fields[ORGANIZATION_COL]
        end
        user.purchases.build vin: fields[VIN_COL], purchased_date: fields[DOFU_COL], dealer_code: fields[DEALER_CODE_COL], user: user
        if user.save
          log_message "Scion Transaction Import (#{Time.zone.now}): Successfully created user ##{user.id}"
        else
          validation_errors = user.errors
          if user.errors.delete(:purchases)
            user.errors.add :base, user.purchases.map{|p| p.errors && p.errors.full_messages}.compact
          end
          log_message "Scion Transaction Import (#{Time.zone.now}): Failed to save customer #{fields[CUSTOMER_ID_COL]}. Validation errors: #{user.errors.full_messages.uniq.flatten}"
          transaction_errors << [fields[TRANSACTION_TYPE_COL], fields[CUSTOMER_ID_COL], fields[VIN_COL], "Invalid user record: #{user.errors.full_messages.uniq.flatten}"]
        end
      elsif fields[TRANSACTION_TYPE_COL].to_s.downcase == 'reversal'
        user = User.find_by_customer_ref_id fields[CUSTOMER_ID_COL]
        if user.blank?
          log_message "Scion Transaction Import (#{Time.zone.now}): Failed to find Customer Ref for reversal #{fields[CUSTOMER_ID_COL]}"
          transaction_errors << [fields[TRANSACTION_TYPE_COL], fields[CUSTOMER_ID_COL], fields[VIN_COL], "Failed to find Customer Ref Id for reversal #{fields[CUSTOMER_ID_COL]}"]
        else
          user.roles['member'] = 'disabled'
          if user.save
            log_message "Scion Transaction Import (#{Time.zone.now}): Disabled user ##{user.id} due to reversal request"
          else
            log_message "Scion Transaction Import (#{Time.zone.now}): Failed disabling user ##{user.id} due to reversal request. Errors: #{user.errors.full_messages}"
            transaction_errors << [fields[TRANSACTION_TYPE_COL], fields[CUSTOMER_ID_COL], fields[VIN_COL], "Failed to reverse user #{user.id}"]
          end
        end
      else
        log_message "Scion Transaction Import (#{Time.zone.now}): Invalid transaction type '#{fields[TRANSACTION_TYPE_COL]}'"
        transaction_errors << [fields[TRANSACTION_TYPE_COL], fields[CUSTOMER_ID_COL], fields[VIN_COL], "Invalid transaction type. Must be 'purchase' or 'reversal'"]
      end
    end

    if transaction_errors.length > 0
      transaction_errors = transaction_errors.map{|er| er.join('|') }.join("\n")
      send_file("scion_swap_trans_errors_#{date_part}.dat.Z.gpg", transaction_options[:url], transaction_options.merge(:remote_path => transaction_options[:errors_path], local_file: write_data(transaction_errors, transaction_options)))
    end
  end
end
