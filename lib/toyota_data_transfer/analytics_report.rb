class AnalyticsReport
  def self.weekly_reservations_report(report_day=Date.today)
    report = [['Dealer Code', 'Dealer Name', 'Model', 'Actual Model', 'City', 'ReservationID', 'Reservation Number', 'VIN', 
               'Rental Agreement Number', 'Reservation Creation Date', 'Scheduled pick up', 'Actual pick up', 
               'Scheduled Return', 'Actual Return', 'Cancelled', 'Customer Ref Id', 'Member First Name', 
               'Member Last Name', 'Swap Days', 'TRAC days']]
    last_week, today = 1.week.until(report_day).beginning_of_day, report_day.beginning_of_day
    Reservation.where{((dropoff >= last_week) & (dropoff < today)) | ((cancelled_at >= last_week) & (cancelled_at < today))}.each do |reservation|
      report << [reservation.dealership.dealer_code, reservation.dealership.name, reservation.fleet_group.model, 
                 reservation.rental_fleet_group.present? ? reservation.rental_fleet_group.model : nil, reservation.dealership.city, 
                 reservation.id, reservation.number, reservation.claims.map{|cl| cl.purchase && cl.purchase.vin}.compact.join(';'), 
                 reservation.rental_agreement_id, reservation.created_at.to_date.iso8601, reservation.start_date.iso8601,
                 reservation.pickup.blank? ? '' : reservation.pickup.iso8601, reservation.end_date.iso8601, 
                 reservation.dropoff.blank? ? '' : reservation.dropoff.iso8601, 
                 reservation.cancelled_at.blank? ? '' : reservation.cancelled_at.iso8601, reservation.user.customer_ref_id, 
                 reservation.user.first_name, reservation.user.last_name, reservation.claims.sum(:credits), reservation.trac_days]
    end
    recipients = Rails.application.config.analytics_report[:weekly_rental_report_recipients]
    body = "Swap weekly reservation report for week ending #{report_day}"
    AnalyticsMailer.analytics_report(recipients, 
                                     'Swap Weekly Reservation Report', 
                                     report.map{|line| line.join(',')}.join("\n"), 
                                     body).deliver
  end
  
  def self.weekly_customer_list
    report = [['First name', 'Last name', 'Email', 'Customer Ref Id', 'Date Account Created', 'Date Activation email sent',
               'Date activation link clicked', 'Date of activation completion', 'Date first reservation booked', 
               'Date first reservation start', 'Current Rewards Balance']]
    User.members.each do |member|
      first_booked_reservation = member.reservations.order(:created_at).first
      first_scheduled_reservation = member.reservations.order(:start_date).first
      report << [member.first_name, member.last_name, member.email, member.customer_ref_id, member.created_at.iso8601, 
                 member.confirmation_sent_at.blank? ? '' : member.confirmation_sent_at.iso8601, 
                 member.confirmed_at.blank? ? '' : member.confirmed_at.iso8601, 
                 member.tos_accepted_at.blank? ? '' : member.tos_accepted_at.iso8601,
                 first_booked_reservation.blank? ? '' : first_booked_reservation.created_at.to_date.iso8601,
                 first_scheduled_reservation.blank? ? '' : first_scheduled_reservation.start_date.iso8601,
                 member.credits_remaining]
    end
    recipients = Rails.application.config.analytics_report[:weekly_rental_report_recipients]
    body = "Swap weekly customer list for #{Date.today}"
    AnalyticsMailer.analytics_report(recipients,
                                     'Swap Weekly Customer List',
                                     report.map{|line| line.join(',')}.join("\n"),
                                     body).deliver
  end
  
  def self.member_expiry_warnings(date=Date.today)
    expiring_purchases = Purchase.expires_on(30.days.since(date)).select{|p| p.credits_remaining > 0}
    expiring_purchases.group_by(&:user).each do |user, purchases|
      UserMailer.expiration_warning(user, purchases).deliver
    end
  end
end