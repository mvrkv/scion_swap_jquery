class ExportSwapClaims < SwapDataTransfer
  CLAIMS_FILE_NAME = lambda { "scion_swap_claims_#{Date.today.strftime('%Y%m%d')}.dat" }
  ['STATUS_COL','CUSTOMER_ID_COL','RESERVATION_ID_COL','VIN_COL','CLAIM_DAYS_COL',
   'RES_DAYS_COL','START_DATE_COL','RENTAL_AGREEMENT_ID_COL','DEALER_CODE_COL'].each_with_index do |constant, index|
    const_set constant, index
  end

  def self.export(export_date=Date.today)
    if export_date.is_a? String
      export_date = Date.parse export_date
      date_part = export_date.strftime '%Y%m%d'
    elsif export_date.respond_to? :strftime
      date_part = export_date.strftime '%Y%m%d'
    else
      raise ArgumentError, "export_date parameter must be a Date or String"
    end                                  
    create_dated_errors_copy(date_part)
    options = Rails.application.config.ftp_data_exchange[:claims]
    claims = []
    export_claims = Claim.where{cast(created_at.as date) == export_date}
    log_message("ExportSwapClaims (#{Time.zone.now}): No claims to export on #{export_date}") and return if export_claims.blank?
    log_message("ExportSwapClaims (#{Time.zone.now}): Found #{export_claims.count} claims for export on #{export_date}")
    export_claims.each do |claim|
      fields = [claim.reservation.cancelled_at.present? ? 'Cancelled' : 'Completed', claim.reservation.user.customer_ref_id,
                claim.reservation.number, claim.purchase.vin, claim.credits, claim.reservation.total_days, 
                claim.reservation.start_date.to_time(:utc).iso8601, claim.reservation.rental_agreement_id || "#{claim.reservation_id}_cancelled", 
                claim.reservation.dealership.dealer_code]
      claims.push fields.join('|')
    end
    claims_filename = "claims_#{date_part}.dat"
    claims_filename << '.Z' if options[:compression]
    claims_filename << '.gpg' if options[:encryption]
    send_file(claims_filename, options[:url], options.merge(remote_path: options[:path], local_file: write_data(claims.join("\n"), options)))
    log_message("ExportSwapClaims (#{Time.zone.now}): Exported #{export_claims.count} claims for #{export_date}")
  end      

  protected
  def self.create_dated_errors_copy(date)
    options = Rails.application.config.ftp_data_exchange[:claims]
    if options[:errors_latest_copy_path] && options[:errors_latest_copy_filename]
      claims_error_filename = options[:errors_latest_copy_filename].sub(/\.dat(\.Z)?(\.gpg)?/, '').concat '.dat'
      claims_error_filename << '.Z' if options[:compression]
      claims_error_filename << '.gpg' if options[:encryption]
      claims_error_file = fetch_file(claims_error_filename, options[:url], {user: options[:user], password: options[:password], :remote_path => options[:errors_latest_copy_path]})
      log_message("Scion Claims Errors (#{Time.zone.now}): No claims errors to import on #{date}") and return if claims_error_file.blank?
      errors_dated_filename = "claims_errors_#{date}.dat"
      errors_dated_filename << '.Z' if options[:compression]
      errors_dated_filename << '.gpg' if options[:encryption]
      send_file(errors_dated_filename, options[:url], {user: options[:user], password: options[:password], local_file: File.open(claims_error_file), remote_path: options[:errors_path]})
      remove_file(claims_error_filename, options[:url], {user: options[:user], password: options[:password], remote_path: options[:errors_latest_copy_path]})
    end
  end
end
