require 'net/ftp'

class SwapDataTransfer
  def self.read_file(src_stream, options={})
    if src_stream.is_a? String
      src = File.open src_stream
    end
    output_filename = Rails.root.join('tmp', Time.now.strftime("%Y%m%d%H%M_swap#{'.Z' if options[:compression]}"))
    output_file = File.open(output_filename, 'wb')
    if !options[:encryption]
      crypto = GPGME::Crypto.new
      crypto.decrypt src || src_stream, output: output_file
    else
      output_file.write (src || src_stream).read
      output_file.close
    end
    if options[:compression]
      uncompressed = `uncompress -cf #{output_filename}`
      uncompressed
    else
      File.read output_filename
    end
  ensure
    src.close if src
    output_file.close if output_file && !output_file.closed?
  end

  def self.write_data(data, options={})
    dest_filename = options[:dest_filename] || Rails.root.join('tmp', Time.zone.now.strftime("%Y%m%d%H%M%S")).to_s
    begin
      dest = File.open dest_filename, 'wb'
      dest.write data
      dest.write "\n"
    ensure
      dest && dest.close
    end
    
    if options[:compression]
      compressed = `compress -f #{dest_filename}`
      dest_filename << '.Z'
    end
    
    if options[:encryption] && options[:encrypt_recipient]
      encrypted_file = File.open(Rails.root.join('tmp', "#{dest_filename}.gpg"), 'wb')
      crypto = GPGME::Crypto.new
      crypto.encrypt File.open(dest_filename), recipients: options[:encrypt_recipient], output: encrypted_file
      encrypted_file
    else
      compressed_file = File.open(dest_filename, 'rb')
      compressed_file
    end
  end

  def self.fetch_file(filename, remote_url, options={})
  begin
    ftp = initialize_ftp(remote_url, options)
    if options[:local_file] && options[:local_file].kind_of?(File)
      local_file = options[:local_file]
    else
      local_file = Rails.root.join('tmp', filename)
      if File.exists? local_file
        local_file = Rails.root.join('tmp', filename.sub(/([^\.]+)\.(.*)/, '\1_'.concat(Time.zone.now.strftime("%Y%m%d%H%M%S")).concat('.\2')))
      end
    end
    if ftp.nlst(filename).length > 0      
      ftp.getbinaryfile(filename, local_file)
    else
      return nil
    end
  ensure
    ftp && ftp.close
  end
    local_file
  end

  def self.send_file(filename, remote_url, options={})
    ftp = initialize_ftp(remote_url, options)
    if options[:local_file] && options[:local_file].kind_of?(File)
      local_file = options[:local_file]
    else
      local_file = Rails.root.join('tmp', filename)
      if File.exists? local_file
        local_file = Rails.root.join('tmp', filename.sub(/([^\.]+)\.(.*)/, '\1_'.concat(Time.zone.now.strftime("%Y%m%d%H%M%S")).concat('.\2')))
      end
    end
    ftp.putbinaryfile(local_file, filename)
    if options[:latest_copy_path] && options[:latest_copy_filename]
      ftp.chdir options[:latest_copy_path]
      latest_copy_filename = options[:latest_copy_filename].sub(/\.dat(\.Z)?(\.gpg)?/, '').concat '.dat'
      # Ensure the proper file extension
      latest_copy_filename << '.Z' if options[:compression]
      latest_copy_filename << '.gpg' if options[:encryption]
      ftp.putbinaryfile(local_file, latest_copy_filename)
    end
  ensure
    ftp && ftp.close
  end       
  
  def self.remove_file(filename, remote_url, options={})
    ftp = initialize_ftp(remote_url, options)
    ftp.delete(filename)
  ensure
    ftp && ftp.close
  end
  
  protected
  def self.log_message(message=nil)
    return if message.blank?
    puts message
    Rails.logger.info message
  end

  private
  def self.initialize_ftp(url, options={})
    ftp = Net::FTP.new(url)
    ftp_params = [options[:user], options[:password]]
    ftp.login *ftp_params
    ftp.passive = true
    if options[:remote_path]
      ftp.chdir options[:remote_path]
    end
    ftp
  rescue
    ftp.close if ftp && !ftp.closed?
    raise
  end
end
