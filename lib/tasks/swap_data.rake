namespace :swap_data do
  desc "To Import data from FTP"
  task :import_transactions => :environment do
    date = (ENV['DATE'] && Date.parse(ENV['DATE'])) || Date.today
    ImportSwapTransaction.import(date)
  end
  
  desc "To Export data from FTP"
  task :export_claims => :environment do
    date = (ENV['DATE'] && Date.parse(ENV['DATE'])) || Date.today
    ExportSwapClaims.export(date)
  end
end
