namespace :analytics do
  task :customer_list => :environment do
    AnalyticsReport.weekly_customer_list
  end
  
  task :reservations_report => :environment do
    date = (ENV['DATE'] && Date.parse(ENV['DATE'])) || Date.today
    AnalyticsReport.weekly_reservations_report(date)
  end
  
  task :expiry_warning => :environment do
    date = (ENV['DATE'] && Date.parse(ENV['DATE'])) || Date.today
    AnalyticsReport.member_expiry_warnings(date)
  end
  
  task :monthly_activity => :environment do
    User.members.each do |user|
      next unless user.is_active_member?
      UserMailer.monthly_activity(user).deliver
    end
  end
end